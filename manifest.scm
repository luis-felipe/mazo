#| GNU Guix manifest

This file is a GNU Guix manifest file. It can be used with GNU Guix to
create a profile or an environment to work on the project. |#

(use-modules (gnu packages))


(define DEV_PACKAGES
  (list "adwaita-icon-theme"  ; For containerized Mazo.
        "hicolor-icon-theme"  ; Fallback for the above.
        "cambalache"
        "coreutils"
        "dbus"
        "gettext"
        "git"
        "gtk:bin"
        ;; Debugging and profiling.
        "graphviz"
        "python-objgraph"
        "sysprof"
        "xdot"
        ;; Development documentation.
        "python-sphinx"
        "python-sphinx-autodoc-typehints"
        ;; For building the HTML manual.
        "yelp-tools"))

(define PRODUCTION_PACKAGES
  (list "gstreamer"
        "gtk"
        "python"
        "python-django"
        "python-django-cleanup"
        "python-django-svg-image-form-field"
        "python-pillow"
        "python-pycairo"
        "python-pygobject"))

(specifications->manifest
 (append DEV_PACKAGES
         PRODUCTION_PACKAGES))
