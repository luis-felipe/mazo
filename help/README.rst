================
Mazo User Manual
================

The files contained in this directory are part of Mazo's User Manual.

This documentation is written in `Mallard`_, which is the standard format
used by the `GNOME Help Framework`_.


Reading the documentation
=========================

The User Manual can be accessed by end-users from the application's
primary menu. Developers can launch the manual by running the
following command in a development environment::

  yelp $PWD/help/C/mazo


Modifying the documentation
===========================

The source files of the User Manual are XML-based and are located in
the C directory. You can use any text editor to modify them.


Publishing in HTML
==================

If necessary, the manual can be exported to different formats, including
HTML, using ``yelp-build``, one of the tools in `yelp-tools`_. For
example, assumming you are in Mazo's base directory::

  mkdir /tmp/mazo-manual
  yelp-build html -o /tmp/mazo-manual help/C/mazo


.. note:: ``yelp-build`` will complain if the output directory already
          exists.



.. LINKS
.. _GNOME Help Framework: https://projects.gnome.org/yelp/
.. _Mallard: http://projectmallard.org/
.. _yelp-tools: https://wiki.gnome.org/Apps/Yelp/Tools
