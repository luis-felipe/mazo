<page xmlns="http://projectmallard.org/1.0/"
      type="topic"
      id="backing-up-and-restoring-data"
      lang="en">

  <info>
    <title type="sort">3. Backing up your data</title>

    <credit type="author">
      <name>Luis Felipe López Acevedo</name>
      <email>luis.felipe.la@protonmail.com</email>
      <years>2022</years>
    </credit>

    <license href="http://en.wikipedia.org/wiki/Public_domain">
      <p>
        This work has been released into the public domain by its
        author. This applies universewide.
      </p>
    </license>

    <desc>
      Back up and restore Mazo's database and files.
    </desc>
  </info>

  <title>Backing up and restoring your data</title>

  <p>
    The following information describes how to create a backup copy of
    all the data managed by Mazo (see <link xref="your-data" />)
    and also how to restore data using such backup copy. A backup copy
    includes the whole database of decks, associated media files and
    personal memorization information. This is different from
    <link xref="importing-exporting-decks" />.
  </p>



  <section id="backing-up-data">
    <title>Backing up data</title>

    <steps>
      <item>
        <p>
          Select <guiseq><gui>Primary menu</gui>
          <gui>Back up data...</gui></guiseq>.
        </p>
      </item>
      <item>
        <p>Indicate a destination folder to save the copy.</p>
      </item>
      <item>
        <p>
          Click the <gui>Back up data</gui> button.
        </p>
      </item>
    </steps>

    <p>
      The resulting zipped backup can be used later to restore your data
      in case it gets lost for some reason
      (see <link xref="backing-up-and-restoring-data#restoring-data" />).
    </p>
  </section>



  <section id="restoring-data">
    <title>Restoring data</title>

    <steps>
      <item>
        <p>
          Select <guiseq><gui>Primary menu</gui>
          <gui>Restore data...</gui></guiseq>.
        </p>
      </item>
      <item>
        <p>Indicate a backup file to restore from.</p>
      </item>
      <item>
        <p>
          Click the <gui>Restore</gui> button.
        </p>
      </item>
      <item>
        <p>
          Once the data is restored, exit the application and start it
          again to see the changes.
        </p>
      </item>
    </steps>
  </section>

</page>
