<page xmlns="http://projectmallard.org/1.0/"
      type="topic"
      id="your-data"
      lang="en">

  <info>
    <title type="sort">4. Your data</title>

    <credit type="author">
      <name>Luis Felipe López Acevedo</name>
      <email>luis.felipe.la@protonmail.com</email>
      <years>2022</years>
    </credit>

    <license href="http://en.wikipedia.org/wiki/Public_domain">
      <p>
        This work has been released into the public domain by its
        author. This applies universewide.
      </p>
    </license>

    <desc>
      Location of the data you manage using Mazo.
    </desc>
  </info>

  <title>Your data</title>

  <p>
    Mazo stores all your data locally, in your machine or device,
    in the <file>/home/USER_NAME/.local/share/mazo</file> folder.
    The <file>mazo</file> folder contains the following:
  </p>

  <screen>
  mazo
  ├── database.sqlite3
  └── media
  </screen>

  <terms>
    <item>
      <title><file>database.sqlite3</file></title>
      <p>
        <link href="https://www.sqlite.org/">SQLite</link> database.
        It stores all the information of your decks, cards, study
        sessions, memorization progress, etc.
      </p>
    </item>
    <item>
      <title><file>media</file></title>
      <p>
        Folder containing all images and sounds associated with your
        decks and cards.
      </p>
    </item>
  </terms>

  <p>
    The data in the <file>mazo</file> folder is not intended to be
    modified manually, though. Instead, depending on what you plan to
    do with it, you should copy it as indicated in
    <link xref="backing-up-and-restoring-data" /> or
    <link xref="importing-exporting-decks" />.
  </p>

</page>
