from datetime import datetime
import os

from django.core.exceptions import ValidationError
from django.core.files.uploadedfile import SimpleUploadedFile
from django.forms import ModelForm

from django_svg_image_form_field import SvgAndImageFormField

from mazo.models import Card, Deck


# FORMS
# =====

class CardForm(ModelForm):
    """Objects of this class represent Django forms used to validate and
    process creation and modification of Card records in the database."""
    class Meta:
        model = Card
        fields = ["deck", "image", "sound", "text", "text_b"]
        field_classes = {
            "image": SvgAndImageFormField,
        }

    def clean(self):
        """Validate fields that depend on each other."""
        cleaned_data = super().clean()

        if not self.has_at_least_two_values(cleaned_data):
            raise ValidationError(
                "A card requires at least two values for studying.",
                code="NotEnoughValues"
            )

    def has_at_least_two_values(self, data):
        """Validate that the data includes values for at least two of
        the following fields:

        + Card.text
        + Card.text_b
        + Card.image
        + Card.sound

        """
        required_count = 2
        value_count = 0

        if data.get("text", False):
            value_count += 1

        if data.get("text_b", False):
            value_count += 1

        if data.get("image", False):
            value_count += 1

        if data.get("sound", False):
            value_count += 1

        return value_count >= required_count


class DeckForm(ModelForm):
    """Objects of this class represent Django forms used to validate and
    process creation and modification of Deck records in the database."""
    class Meta:
        model = Deck
        fields = ["name", "description", "credits", "icon"]
        field_classes = {
            "icon": SvgAndImageFormField,
        }


# HELPERS
# =======

def create_deck(data, files={}):
    """Create a new Deck using DATA and FILES.

    DATA (dict)
      Like any data dictionary passed to a django.forms.ModelForm.

    FILES (dict)
      Optional. Like any files dictionary passed to a django.forms.ModelForm.

    RETURN VALUE (mazo.Models.Deck)
      The newly created deck or a dictionary of validation errors in the
      form {"Error": "Description.", ...}."""
    form = DeckForm(data, files)
    errors = {}

    # TODO: Validate date follows ISO 8601 format YYYY-MM-DDTHH:MM:SSZ?

    # Validate with Django form and save if valid.
    if form.is_valid():
        deck = form.save()

        # Overwrite automatic creation date with data["creation_date"], if any
        # (e.g. when data comes from a deck import).
        try:
            original_date = data["creation_date"]
        except KeyError:
            pass
        else:
            date_object = datetime.strptime(
                original_date,
                "%Y-%m-%dT%H:%M:%S%z"
            )
            deck.creation_date = date_object
            deck.save()

        return deck
    else:
        errors |= form.errors.as_data()
        return errors


def create_card(data, files={}):
    """Create a new Card using DATA and FILES.

    Note that a card requires at least two of the following fields to
    have values in order to be able to study it:

    + Card.text
    + Card.text_b
    + Card.image
    + Card.sound

    DATA (dict)
      Like any data dictionary passed to a django.forms.ModelForm.

    FILES (dict)
      Optional. Like any files dictionary passed to a django.forms.ModelForm.

    RETURN VALUE (mazo.Models.Card)
      The newly created card or a dictionary of validation errors in the
      form {"Error": "Description.", ...}.

    """
    form = CardForm(data, files)
    errors = {}

    # Validate with Django form and save if valid.
    # TODO: Validate date follows ISO 8601 format YYYY-MM-DDTHH:MM:SSZ?
    # TODO: Validate audio format (might require a custom model field instead).
    if form.is_valid():
        card = form.save()

        # Overwrite automatic creation date with data["creation_date"], if any
        # (e.g. when data comes from a deck import).
        try:
            original_date = data["creation_date"]
        except KeyError:
            pass
        else:
            date_object = datetime.strptime(
                original_date,
                "%Y-%m-%dT%H:%M:%S%z"
            )
            card.creation_date = date_object
            card.save()

        return card
    else:
        errors |= form.errors.as_data()
        return errors

    return form.save()


def create_file_upload(path):
    """Return a file object appropriate for Django forms.

    PATH (string)
      Relative or absolute path to a file in the file system.

    RETURN VALUE (django.core.files.uploadedfile.SimpleUploadedFile)
      A file object appropriate for the files dictionary accepted by
      django.forms.ModelForm instances.

    EXCEPTIONS
      FileNotFoundError if the file in PATH does not exist."""
    file_basename = os.path.basename(path)

    with open(path, "rb") as target_file:
        file_data = target_file.read()
        return SimpleUploadedFile(file_basename, file_data)
