# Generated by Django 4.0.4 on 2022-07-18 15:22

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mazo', '0013_alter_deck_credits_alter_round_progress'),
    ]

    operations = [
        migrations.AddField(
            model_name='answer',
            name='kind',
            field=models.CharField(choices=[('CORRECT', 'Correct'), ('INCORRECT', 'Incorrect'), ('MEMORIZED', 'Memorized')], default='MEMORIZED', max_length=20),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='aspect',
            name='retention_points',
            field=models.PositiveIntegerField(default=0, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(5, message="Max value can't be greater than mazo.constants.ASPECT_POINTS_FOR_MEMORIZATION.")]),
        ),
    ]
