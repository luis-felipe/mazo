# Generated by Django 4.0.4 on 2022-04-20 22:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mazo', '0003_deck_creation_date_alter_aspect_retention_points'),
    ]

    operations = [
        migrations.AddField(
            model_name='studysession',
            name='progress',
            field=models.DecimalField(decimal_places=2, default=0, help_text='Deck memorization progress at the end of the session.', max_digits=6),
        ),
    ]
