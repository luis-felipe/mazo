# Generated by Django 4.0.2 on 2022-03-03 16:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mazo', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='aspect',
            name='retention_points',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
