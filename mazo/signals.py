"""Signal handlers for Mazo's Django models."""

from django.db.models import signals
from django.dispatch import receiver
from django.utils import timezone

from mazo.models import Answer, Aspect, Card, Deck, Round, StudySession


@receiver(signals.post_save, sender=Answer)
def on_answer_created(sender, instance, created, **kwargs):
    """When an answer is created, tell its related aspect to either:

    + Add one point to its retention points accumulator, if the answer
      is correct.
    + Reset retention points to zero, if the answer is incorrect.

    Also, tell current round to update its progress to the memorization
    progress of the deck at this point."""
    if created:
        # Tell related aspect to update its points.
        aspect = instance.review.aspect
        if instance.kind == "CORRECT":
            aspect.add_point()
        elif instance.kind == "MEMORIZED":
            aspect.add_point()
            aspect.set_is_learned(True)
        else:
            aspect.reset_points()

        # Tell current round to update its progress.
        deck = aspect.card.deck
        ongoing_round = deck.rounds.order_by("number").last()
        ongoing_round.update_progress()


@receiver(signals.post_save, sender=Aspect)
def on_aspect_created(sender, instance, created, **kwargs):
    """When an aspect is created, tell it to schedule an initial review
    for itself (if there is an ongoing round)."""
    if (
            created and
            instance.card.deck.rounds.filter(finish_date__isnull=True).exists()
    ):
        instance.schedule_review(timezone.now())


@receiver(signals.post_save, sender=Card)
def on_card_created(sender, instance, created, **kwargs):
    """When a card is created, tell it to create appropriate aspects for
    itself (see mazo.Models.Aspect)."""
    if created:
        instance.create_aspects()


@receiver(signals.post_save, sender=Card)
def on_card_changed(sender, instance, created, **kwargs):
    """When a card is modified, tell it to update its aspects: add
    missing ones, and remove obsolete ones (see mazo.Models.Aspect)."""
    if not created:
        instance.create_aspects()
        instance.remove_aspects()


@receiver(signals.post_save, sender=Deck)
def on_deck_created(sender, instance, created, **kwargs):
    """When a deck is created, create a default Round for itself."""
    if created:
        Round.objects.create(deck=instance)


@receiver(signals.post_save, sender=Round)
def on_round_created(sender, instance, created, **kwargs):
    """When a new study round is created:

    + Tell deck to increase its round count.
    + Tell round to number itself.
    + Tell aspects to schedule initial reviews for themselves.
    + Tell aspects to reset their retention points back to zero."""
    if created:
        # Tell deck to increase its round count.
        deck = instance.deck
        deck.increase_round_count()

        # Tell round to number itself.
        instance.set_number()

        # Tell aspects to schedule reviews for themselves.
        aspects = Aspect.objects.filter(card__deck=deck)
        for aspect in aspects:
            aspect.schedule_review(timezone.now())

        # Tell aspects to reset retention points back to zero.
        for aspect in aspects:
            aspect.reset_points()


@receiver(signals.post_save, sender=StudySession)
def on_study_session_created(sender, instance, created, **kwargs):
    """When a study session is created, tell it to add to itself the
    scheduled cards for review."""
    if created:
        instance.claim_reviews()
