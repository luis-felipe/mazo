"""Mazo importing/exporting mechanisms.

See Mazo's import/export format documentation for more information.

https://luis-felipe.gitlab.io/mazo/doc/import-export-format.html"""

from datetime import datetime
import json
import os
import shutil
import tempfile
import zipfile

from mazo.constants import (
    APP_NAME,
    IMEXPORT_FORMAT_VERSION,
    USER_DATA_DIR
)
from mazo.forms import create_card, create_deck, create_file_upload
from mazo.models import Deck


# FUNCTIONS
# =========

def deck_to_dict(deck):
    """Return DECK represented as a Python dictionary."""
    # Convert cards.
    card_dicts = []

    for card in deck.cards.all():
        card_dicts.append(card_to_dict(card))

    # Convert dictionary.
    deck_dict = {
        "version": IMEXPORT_FORMAT_VERSION,
        "creation_date": deck.creation_date.strftime(
            "%Y-%m-%dT%H:%M:%S%z"
        ),
        "name": deck.name
    }

    if deck.description:
        deck_dict["description"] = deck.description

    if deck.credits:
        deck_dict["credits"] = deck.credits

    if deck.icon.name:
        deck_dict["icon"] = os.path.basename(deck.icon.name)

    if card_dicts:
        deck_dict["cards"] = card_dicts

    return deck_dict


def card_to_dict(card):
    """Return CARD represented as a Python dictionary."""
    card_dict = {
        "creation_date": card.creation_date.strftime(
            "%Y-%m-%dT%H:%M:%S%z"
        )
    }

    if card.text:
        card_dict["text"] = card.text

    if card.text_b:
        card_dict["text_b"] = card.text_b

    if card.image.name:
        card_dict["image"] = card.image.name

    if card.sound.name:
        card_dict["sound"] = card.sound.name

    return card_dict


def export_as_json(deck, destination):
    """Export DECK in JSON format to DESTINATION directory.

    Return an absolute path to the file."""
    json_path = os.path.join(destination, "deck.json")
    deck_dict = deck_to_dict(deck)
    deck_json = json.dumps(deck_dict)

    with open(json_path, "w") as json_file:
        json_file.write(deck_json)

    return json_path


def export_deck(deck, destination):
    """Export the given DECK to the DESTINATION folder.

    The deck will be exported as a zipped archive.

    Return an absolute path to the exported archive."""
    # Define dictionary of errors.
    errors = {}

    # Create working directory in temp directory.
    wd_timestamp = datetime.now().strftime("%Y-%m-%dT%H-%M-%S-%f")
    working_dir_path = os.path.join(
        tempfile.gettempdir(),
        "mazo-export-{}".format(wd_timestamp)
    )
    try:
        os.mkdir(working_dir_path)
    except BaseException as error:
        errors["DeckExportWorkingDirError"] = error
        return errors

    # Create deck folder in working directory.
    zip_timestamp = datetime.now().strftime("%Y-%m-%dT%H-%M-%S")
    folder_name = "{}-{}".format(deck.name, zip_timestamp)
    deck_folder_path = os.path.join(working_dir_path, folder_name)
    try:
        os.mkdir(deck_folder_path)
    except BaseException as error:
        errors["DeckExportDirError"] = error
        return errors

    # Create deck.json.
    try:
        export_as_json(deck, deck_folder_path)
    except BaseException as error:
        errors["DeckExportJSONError"] = error
        return errors

    # Export multimedia files.
    try:
        export_icon(deck, deck_folder_path)
    except BaseException as error:
        errors["DeckExportIconError"] = error
        return errors

    try:
        export_images(deck, deck_folder_path)
    except BaseException as error:
        errors["DeckExportImagesError"] = error
        return errors

    try:
        export_sounds(deck, deck_folder_path)
    except BaseException as error:
        errors["DeckExportSoundsError"] = error
        return errors

    # Zip the deck folder and place it in DESTINATION.
    try:
        deck_archive_path = shutil.make_archive(
            os.path.join(destination, folder_name),  # ZIP name (.zip is added).
            "zip",             # Format.
            working_dir_path,  # Content root directory.
            folder_name        # Folder to zip up.
        )
    except BaseException as error:
        errors["DeckExportCompressionError"] = error
        return errors

    return deck_archive_path


def export_icon(deck, destination):
    """Export the DECK icon to DESTINATION directory.

    Return an absolute path to the icon or None if there's no icon."""
    icon_path = None

    try:
        icon_path = os.path.join(destination, os.path.basename(deck.icon.path))
    except ValueError:
        # TODO: log("Info: The deck has no icon (ignoring).")
        pass
    else:
        shutil.copyfile(deck.icon.path, icon_path)

    return icon_path


def export_images(deck, destination):
    """Export the images of DECK's cards to DESTINATION directory.

    Return an absolute path to the images folder or None if there are
    no images to export."""
    images_folder_path = None

    if deck.has_visual_cards():
        images_folder_path = os.path.join(destination, "images")
        os.mkdir(images_folder_path)
        visual_cards = deck.cards.filter(
            image__isnull=False
        ).exclude(image__exact="")

        for card in visual_cards:
            dst_path = os.path.join(destination, card.image.name)
            shutil.copyfile(card.image.path, dst_path)

    return images_folder_path


def export_sounds(deck, destination):
    """Export sounds of DECK's cards to DESTINATION directory.

    Return an absolute path to the sounds folder or None if there are
    no sounds to export."""
    sounds_folder_path = None

    if deck.has_aural_cards():
        sounds_folder_path = os.path.join(destination, "sounds")
        os.mkdir(sounds_folder_path)
        aural_cards = deck.cards.filter(
            sound__isnull=False
        ).exclude(sound__exact="")

        for card in aural_cards:
            dst_path = os.path.join(destination, card.sound.name)
            shutil.copyfile(card.sound.path, dst_path)

    return sounds_folder_path


def import_deck(path):
    """Import the deck from the archive in PATH.

    Return the newly created deck if the import is successful; otherwise
    return a dictionary of errors in the form {"Error": "Description."}."""
    source_archive_path = path
    archive_name = os.path.basename(path)
    timestamp = datetime.now().strftime("%Y-%m-%dT%H-%M-%S-%f")
    extraction_dir_path = os.path.join(
        tempfile.gettempdir(),
        "mazo-import-{}".format(timestamp)
    )
    archive_copy_path = os.path.join(extraction_dir_path, archive_name)
    deck_dir_path = os.path.join(
        extraction_dir_path,
        archive_name.replace(".zip", "")
    )
    errors = {}

    if zipfile.is_zipfile(source_archive_path):
        # Copy source archive to temp directory for extraction.
        os.mkdir(extraction_dir_path)
        shutil.copyfile(source_archive_path, archive_copy_path)

        # Unzip archive copy.
        shutil.unpack_archive(archive_copy_path, extraction_dir_path)

        # Read deck.json.
        json_path = os.path.join(deck_dir_path, "deck.json")
        if os.path.exists(json_path):
            json_file = open(json_path, encoding="utf-8")
            deck_dict = json.load(json_file)
        else:
            errors["NoJsonFileInDeckArchiveError"] = (
                "The archive doesn't include a deck.json file."
            )
            return errors

        # Validate format version.
        try:
            format_version = deck_dict["version"]
        except KeyError:
            errors["NoFormatVersionError"] = (
                "deck.json is missing the version field."
            )
            return errors

        # Validate deck and create it.
        deck_data = {}
        deck_files = {}

        # Get deck-related data.
        fields = ["creation_date", "name", "description", "credits"]
        for field in fields:
            if field in deck_dict:
                deck_data[field] = deck_dict[field]

        # Get deck icon (if any).
        try:
            icon_subpath = deck_dict["icon"]
        except KeyError:
            # TODO: log("Info: The deck has no icon field (ignoring).")
            pass
        else:
            icon_path = os.path.join(deck_dir_path, icon_subpath)

            try:
                deck_files["icon"] = create_file_upload(icon_path)
            except FileNotFoundError:
                print("Warning: The deck has an icon field, but the associated file does not exist.")
                print(icon_path)

        # Create deck.
        result = create_deck(deck_data, deck_files)

        if isinstance(result, Deck):
            deck = result
        else:
            errors |= result
            return errors

        # Get deck cards (if any).
        try:
            cards = deck_dict["cards"]
        except KeyError:
            # TODO: log("Info: The deck has no cards (ignoring).")
            return deck
        else:
            # Create cards.
            for card_dict in cards:
                import_card(card_dict, deck, deck_dir_path)

            return deck
    else:
        errors["InvalidZipError"] = "The archive is not a valid ZIP file."
        return errors


def import_card(card_dict, deck, media_path):
    """Import the information in CARD DICT.

    CARD_DICT (dict)
      A dictionary representation of a card as specified in Mazo's
      import/export format.

    DECK (mazo.models.Deck)
      The deck the card belongs to.

    MEDIA_PATH (string)
      Path to a directory containing the media files associated with the
      card.

    RETURN VALUE (mazo.models.Card or dict)
      Return the newly created card if the import is successful;
      otherwise return a dictionary of errors in the form
      {"Error": "Description."}."""
    card_data = {"deck": deck}
    card_files = {}

    # Get card-related data.
    fields = ["creation_date", "text", "text_b"]
    for field in fields:
        if field in card_dict:
            card_data[field] = card_dict[field]

    # Get card image (if any).
    try:
        image_subpath = card_dict["image"]
    except KeyError:
        # TODO: log("Info: The card has no image field (ignoring).")
        pass
    else:
        image_path = os.path.join(media_path, image_subpath)

        try:
            card_files["image"] = create_file_upload(image_path)
        except FileNotFoundError:
            print("Warning: The card has an image field, but the associated file does not exist.")
            print(image_path)

    # Get card sound (if any).
    try:
        sound_subpath = card_dict["sound"]
    except KeyError:
        # TODO: log("Info: The card has no sound field (ignoring).")
        pass
    else:
        sound_path = os.path.join(media_path, sound_subpath)

        try:
            card_files["sound"] = create_file_upload(sound_path)
        except FileNotFoundError:
            print("Warning: The card has a sound field, but the associated file does not exist.")
            print(sound_path)

    # Create card.
    return create_card(card_data, card_files)


def back_up_data(destination):
    """Save a zipped copy of user's data into DESTINATION folder.

    User data corresponds to the directory defined in
    ``mazo.constants.USER_DATA_DIR``.

    DESTINATION (string)
      A string representing an absolute path to a folder where to save
      the backup copy.

    RETURN VALUE (string or dict)
      Return an absolute path to the backup copy if the operation is
      successful; otherwise, return a dictionary of errors in the form
      ``{"error": "Description."}``.

    """
    zip_timestamp = datetime.now().strftime("%Y-%m-%dT%H-%M-%S")
    backup_name = "{}-backup-{}".format(APP_NAME, zip_timestamp)

    try:
        backup_path = shutil.make_archive(
            os.path.join(destination, backup_name),  # .zip appended.
            "zip",  # Format.
            os.path.dirname(USER_DATA_DIR),  # Content root directory.
            APP_NAME  # Folder to zip up.
        )
    except BaseException as error:
        return {"DataBackUpError": error}

    return backup_path


def restore_data(path):
    """Restore user data from a zipped backup in PATH.

    This operation overwrites any existing data in the directory
    indicated by ``mazo.constants.USER_DATA_DIR``.

    PATH (string)
      A string representing an absolute path to a backup copy (see
      ``mazo.ie.back_up_data`` for more information on backup copies).

    RETURN VALUE (string or dict)
      Return an absolute path to the restored data if the operation is
      successful; otherwise, return a dictionary of errors in the form
      ``{"error": "Description."}``.

    """
    # Make sure the backup path exists.
    if not os.path.exists(path):
        msg = "{} does not exist.".format(path)
        return {"BackupNotFoundError": msg}

    # Make sure the backup is a ZIP file.
    if not zipfile.is_zipfile(path):
        msg = "{} is not a ZIP file.".format(path)
        return {"BackupFormatError": msg}

    # Make sure the backup includes a database file.
    expected_db_path = os.path.join(APP_NAME, "database.sqlite3")
    with zipfile.ZipFile(path, "r") as backup:
        if not (expected_db_path in backup.namelist()):
            msg = "{} is missing.".format(expected_db_path)
            return {"DatabaseNotFoundError": msg}

    # Delete current user data.
    if os.path.exists(USER_DATA_DIR):
        try:
            shutil.rmtree(USER_DATA_DIR)
        except BaseException as error:
            return {"DataRestoreError": error}

    # Restore USER_DATA_DIR.
    extraction_dir_path = os.path.dirname(USER_DATA_DIR)
    shutil.unpack_archive(path, extraction_dir_path)

    return USER_DATA_DIR
