"""Mazo constants."""

from datetime import timedelta
import os


# Application.
APP_NAME = "mazo"
APP_ID = "lugare.ulkeva.{}".format(APP_NAME)
APP_VERSION = "1.2.1"
IMEXPORT_FORMAT_VERSION = "0.7.0"  # Independent from APP_VERSION.

# The directory to save all user data.
USER_DATA_DIR = os.path.join(os.environ["HOME"], ".local", "share", APP_NAME)

# Database name and location.
DB_DIR = USER_DATA_DIR
DB_NAME = "database.sqlite3"
DB_PATH = os.path.join(DB_DIR, DB_NAME)

# The directory where user uploaded files are placed.
MEDIA_DIR = os.path.join(USER_DATA_DIR, "media")

# Card review interval.
REVIEW_INTERVAL = timedelta(days=2)

# Retention points required to consider that an aspect is learned.
ASPECT_POINTS_FOR_MEMORIZATION = 4
