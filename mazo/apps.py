from django.apps import AppConfig


class MazoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mazo'

    def ready(self):
        from . import signals
