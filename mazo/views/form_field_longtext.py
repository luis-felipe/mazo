"""Long text field for forms."""

import os

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# COMPONENTS
# ==========

@Gtk.Template(filename=os.path.join(UI_PATH, "long-text-field.ui"))
class LongTextField(Gtk.ScrolledWindow):
    """Represents a form field to write multiline text.

    FORM (CardFormView)
      The form the field belongs to.

    FIELD_ID (string)
      An HTML-like id to differentiate this field from other fields of
      the same kind in the form."""
    __gtype_name__ = "LongTextField"

    text_buffer = Gtk.Template.Child("text-buffer")
    text_view = Gtk.Template.Child("text-view")

    def __init__(self, form, field_id):
        super().__init__()
        self.form = form
        self.field_id = field_id

        # XXX: Connect signals to handlers.
        self.text_buffer.connect(
            "changed",
            self.on_text_changed
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.text_buffer.disconnect_by_func(
            self.on_text_changed
        )

    # @Gtk.Template.Callback()
    def on_text_changed(self, text_buffer):
        """Handle the text buffer's changed signal."""
        text = self.get_text()

        if len(text) > 0:
            self.form.emit("field-filled", self.field_id, text)
        else:
            self.form.emit("field-reset", self.field_id)

    def get_text(self):
        """Get the text in the field."""
        start_iter = self.text_buffer.get_start_iter()
        end_iter = self.text_buffer.get_end_iter()

        return self.text_buffer.get_text(start_iter, end_iter, False)

    def set_text(self, text):
        """Set the field to the given text."""
        self.text_buffer.set_text(text)
