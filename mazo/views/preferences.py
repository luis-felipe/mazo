"""Preferences view."""

import os

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# VIEWS
# =====

@Gtk.Template(filename=os.path.join(UI_PATH, "preferences-view.ui"))
class PreferencesView(Gtk.ScrolledWindow):
    """Represents a view where users can set application preferences."""
    __gtype_name__ = "PreferencesView"

    def __init__(self, window):
        super().__init__()
        self.name = "app-preferences"
        self.title = "Preferences"
        self.actions = None
        self.window = window
