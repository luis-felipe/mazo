"""Card browser view."""

import os

from PIL import Image

from mazo.constants import MEDIA_DIR
from mazo.views.gif_paintable import GifPaintable
from mazo.views.media_player import MediaPlayer

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# VIEWS
# =====

@Gtk.Template(filename=os.path.join(UI_PATH, "card-browser-view.ui"))
class CardBrowserView(Gtk.ScrolledWindow):
    """Represents a card browser."""
    __gtype_name__ = "CardBrowserView"

    picture = Gtk.Template.Child("picture")
    player_box = Gtk.Template.Child("player-box")
    text_label = Gtk.Template.Child("text-label")
    text_b_label = Gtk.Template.Child("text-b-label")

    delete_button = Gtk.Template.Child("delete-button")
    edit_button = Gtk.Template.Child("edit-button")
    next_button = Gtk.Template.Child("next-button")
    previous_button = Gtk.Template.Child("previous-button")
    card_number_label = Gtk.Template.Child("card-number-label")

    def __init__(self, window, card):
        super().__init__()
        self.name = "card-detail-view-{}".format(card.id)
        self.title = "Browsing {}".format(card.deck.name)
        self.actions = CardBrowserHeaderActions(window, card)
        self.window = window
        self.card_set = card.deck.cards.all().order_by("creation_date")
        self.card = card
        self.card_index = self.set_card_index()

        # Enable navigation buttons only if necessary.
        if self.card_set.count() > 1:
            self.next_button.set_sensitive(True)
            self.previous_button.set_sensitive(True)

        # Display card.
        self.display(card)

        # XXX: Connect signals to handlers.
        self.delete_button.connect(
            "clicked",
            self.on_delete_button_clicked
        )
        self.edit_button.connect(
            "clicked",
            self.on_edit_button_clicked
        )
        self.next_button.connect(
            "clicked",
            self.on_next_button_clicked
        )
        self.previous_button.connect(
            "clicked",
            self.on_previous_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.delete_button.disconnect_by_func(
            self.on_delete_button_clicked
        )
        self.edit_button.disconnect_by_func(
            self.on_edit_button_clicked
        )
        self.next_button.disconnect_by_func(
            self.on_next_button_clicked
        )
        self.previous_button.disconnect_by_func(
            self.on_previous_button_clicked
        )
        self.actions.disconnect_signal_handlers()

        if hasattr(self, "player"):
            self.player.disconnect_signal_handlers()

    # @Gtk.Template.Callback()
    def on_delete_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        message = (
            "Do you really want to delete this card\nfrom the «{}» deck?".
            format(self.card.deck)
        )
        # FIXME: Gtk.MessageDialog init API does not work in Python (?).
        # That's why I have to add_button and set_property, etc.
        dialog = Gtk.MessageDialog()
        dialog.add_button("Cancel", Gtk.ButtonsType.CANCEL)
        dialog.add_button("OK", Gtk.ButtonsType.OK)
        dialog.connect("response", self.delete_card)
        dialog.set_modal(True)
        dialog.set_transient_for(self.window)
        dialog.set_title("Delete card?")
        dialog.set_property("secondary_text", message)
        dialog.set_property("message_type", Gtk.MessageType.WARNING)
        dialog.show()

    # @Gtk.Template.Callback()
    def on_edit_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.window.show_view(
            "card-edition",
            [self.window],
            {"card": self.card}
        )

    # @Gtk.Template.Callback()
    def on_next_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        next_index = self.card_index + 1

        # Guard against out-of-range indexes.
        if next_index < self.card_set.count():
            card = self.card_set[self.card_index + 1]
        else:
            # Display the first card again when in the last card.
            card = self.card_set.first()

        self.window.show_view(
            "card-browser",
            [self.window, card]
        )

    # @Gtk.Template.Callback()
    def on_previous_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        previous_index = self.card_index - 1

        # Guard against negative indexes.
        if previous_index >= 0:
            card = self.card_set[previous_index]
        else:
            # Display the last card when in the first card.
            card = self.card_set.last()

        self.window.show_view(
            "card-browser",
            [self.window, card],
            transition=Gtk.StackTransitionType.SLIDE_RIGHT
        )

    def delete_card(self, dialog, response):
        """Handle the response of the card deletion dialog."""
        # FIXME: Better use: if response == Gtk.ResponseType.OK:
        #
        # That would be possible if I could pass the "buttons" argument
        # Gtk.ButtonsType.OK_CANCEL normally when initializing the
        # dialog in self.on_delete_button_clicked.
        if response == 1:
            deck = self.card.deck
            # Delete card.
            self.card.delete()
            # Show card's deck detail view.
            self.window.show_view("deck-detail", [self.window, deck])

        dialog.destroy()

    def display(self, card):
        """Display the information of the given CARD."""
        # Display image only if there is one.
        if card.is_visual():
            image_path = os.path.join(MEDIA_DIR, card.image.name)

            if (
                    image_path.lower().endswith(".gif") and
                    Image.open(image_path).is_animated
            ):
                paintable = GifPaintable(image_path)
                self.picture.set_paintable(paintable)
            else:
                self.picture.set_filename(image_path)

            self.picture.set_visible(True)
        # Display media player only if there is sound.
        if card.is_aural():
            sound_path = os.path.join(MEDIA_DIR, self.card.sound.name)
            self.player = MediaPlayer(sound_path)
            self.player_box.append(self.player)
            self.player_box.set_visible(True)
        # Display text only if there is any.
        if card.is_textual():
            markup = "<span size='xx-large'>" + card.text + "</span>"
            self.text_label.set_markup(markup)
            self.text_label.set_visible(True)
        # Display additional text, if any.
        if card.text_b:
            markup = "<span size='xx-large'>" + card.text_b + "</span>"
            self.text_b_label.set_markup(markup)
            self.text_b_label.set_visible(True)
        # Set current card indicator.
        text = "Card {} of {}".format(
            (self.card_index + 1),
            self.card_set.count()
        )
        self.card_number_label.set_text(text)

    def set_card_index(self):
        """Update de browser's card index with the index of the currently
        displayed card in the card set."""
        index = 0

        for card in self.card_set:
            if card.id == self.card.id:
                break
            else:
                index += 1

        return index


# COMPONENTS
# ==========

@Gtk.Template(filename=os.path.join(UI_PATH, "card-browser-header-actions.ui"))
class CardBrowserHeaderActions(Gtk.Box):
    """GUI component with actions for card browser views (CardBrowserView)."""
    __gtype_name__ = "CardBrowserHeaderActions"

    leave_button = Gtk.Template.Child("leave-button")

    def __init__(self, window, card):
        super().__init__()
        self.window = window
        self.card = card

        # XXX: Connect signals to handlers.
        self.leave_button.connect(
            "clicked",
            self.on_leave_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.leave_button.disconnect_by_func(
            self.on_leave_button_clicked
        )

    # @Gtk.Template.Callback()
    def on_leave_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.window.show_view(
            "deck-detail",
            [self.window, self.card.deck]
        )
