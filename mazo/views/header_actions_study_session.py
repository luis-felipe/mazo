"""Study session actions for the header component."""

import os

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# COMPONENTS
# ==========

@Gtk.Template(
    filename=os.path.join(UI_PATH, "study-session-header-actions.ui")
)
class StudySessionHeaderActions(Gtk.Box):
    """GUI component with actions for a given study session."""
    __gtype_name__ = "StudySessionHeaderActions"

    finish_button = Gtk.Template.Child("finish-button")
    progress_label = Gtk.Template.Child("progress-label")

    def __init__(self, window, study_session):
        super().__init__()
        self.window = window
        self.study_session = study_session

        # Show session progress.
        total_cards = study_session.reviews.count()
        cards_reviewed = study_session.reviews.filter(
            answer__isnull=False
        ).count()
        progress = "{}/{}".format(
            cards_reviewed + 1,  # Start at 1.
            total_cards
        )
        self.progress_label.set_text(progress)

        # XXX: Connect signals to handlers.
        self.finish_button.connect(
            "clicked",
            self.on_finish_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.finish_button.disconnect_by_func(
            self.on_finish_button_clicked
        )

    # @Gtk.Template.Callback()
    def on_finish_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.window.end_study_session()
