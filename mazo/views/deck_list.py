"""Deck list view."""

import os

from django.utils import timezone

from mazo.constants import MEDIA_DIR
from mazo.models import Deck
from mazo.views.progress_gauge import ProgressGauge

import gi

gi.require_version("GdkPixbuf", "2.0")
gi.require_version("Gtk", "4.0")

from gi.repository import GdkPixbuf, Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")
GENERIC_IMAGE = os.path.join(UI_PATH, "generic-image.svg")


# VIEWS
# =====

@Gtk.Template(filename=os.path.join(UI_PATH, "deck-list-view.ui"))
class DeckListView(Gtk.Box):
    """Represents a view that lists decks in progress or finished."""
    __gtype_name__ = "DeckListView"

    list_box = Gtk.Template.Child("list-box")
    ongoing_button = Gtk.Template.Child("ongoing-button")
    finished_button = Gtk.Template.Child("finished-button")
    store_button = Gtk.Template.Child("store-button")

    def __init__(self, window, category="ongoing"):
        super().__init__()
        self.window = window
        self.id = timezone.now().strftime("%H%S")
        self.name = "deck-list-view-{}".format(self.id)
        self.title = "List of Decks"
        self.actions = DeckListHeaderActions(window)
        self.category = category

        # Set empty list placeholder.
        placeholder = NoDecksFoundView(window)
        self.list_box.set_placeholder(placeholder)

        # Enable finished decks switch.
        finished_decks = Deck.objects.exclude(rounds__finish_date__isnull=True)

        if finished_decks.exists():
            self.finished_button.set_sensitive(True)

        # List decks according to selected category.
        decks = Deck.objects.all()

        if category == "ongoing":
            self.name = "deck-list-ongoing-{}".format(self.id)
            self.title = "Decks in progress"
            self.ongoing_button.set_has_frame(True)
            self.finished_button.set_has_frame(False)

            decks = Deck.objects.filter(rounds__finish_date__isnull=True)
        elif category == "finished":
            self.name = "deck-list-finished-{}".format(self.id)
            self.title = "Decks finished"
            self.ongoing_button.set_has_frame(False)
            self.finished_button.set_has_frame(True)

            decks = finished_decks

        self.list(decks)

        # XXX: Connect signals to handlers.
        self.list_box.connect(
            "row-activated",
            self.on_row_activated
        )
        self.ongoing_button.connect(
            "clicked",
            self.on_ongoing_button_clicked
        )
        self.finished_button.connect(
            "clicked",
            self.on_finished_button_clicked
        )
        self.store_button.connect(
            "clicked",
            self.on_store_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.finished_button.disconnect_by_func(
            self.on_finished_button_clicked
        )
        self.ongoing_button.disconnect_by_func(
            self.on_ongoing_button_clicked
        )
        self.list_box.disconnect_by_func(
            self.on_row_activated
        )
        self.store_button.disconnect_by_func(
            self.on_store_button_clicked
        )
        self.actions.disconnect_signal_handlers()

    # @Gtk.Template.Callback()
    def on_finished_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.window.show_view(
            "deck-list",
            [self.window],
            {"category": "finished"}
        )

    # @Gtk.Template.Callback()
    def on_ongoing_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.window.show_view("deck-list", [self.window])

    # @Gtk.Template.Callback()
    def on_row_activated(self, listbox, row):
        """Handle the LISTBOX's row-activated signal."""
        self.window.show_view(
            "deck-detail",
            [self.window, row.deck]
        )

    # @Gtk.Template.Callback()
    def on_store_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.window.show_view("deck-store", [self.window])

    def list(self, decks):
        """List the given decks.

        DECKS (Django QuerySet)
          A query set of mazo.models.Deck objects."""
        # Clear list.
        self.list_box.set_selection_mode(Gtk.SelectionMode.MULTIPLE)
        self.list_box.select_all()
        rows = self.list_box.get_selected_rows()

        for row in rows:
            self.list_box.remove(row)

        self.list_box.set_selection_mode(Gtk.SelectionMode.SINGLE)

        # Repopulate the list.
        for deck in decks.order_by("name"):
            self.list_box.append(DeckListItem(deck))


@Gtk.Template(filename=os.path.join(UI_PATH, "no-decks-found-view.ui"))
class NoDecksFoundView(Gtk.ScrolledWindow):
    """Represents a view that is shown when there are no decks yet."""
    __gtype_name__ = "NoDecksFoundView"

    add_button = Gtk.Template.Child("add-button")

    def __init__(self, window):
        super().__init__()
        self.window = window

        # XXX: Connect signals to handlers.
        self.add_button.connect("clicked", self.on_add_button_clicked)

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.add_button.disconnect_by_func(self.on_add_button_clicked)

    # @Gtk.Template.Callback()
    def on_add_button_clicked(self, button):
        """Handle the BUTTON's clicked signal."""
        self.window.show_view("deck-form", [self.window])


# COMPONENTS
# ==========

@Gtk.Template(filename=os.path.join(UI_PATH, "deck-list-header-actions.ui"))
class DeckListHeaderActions(Gtk.Box):
    """Objects of this class represent the actions available for the
    Deck List View."""
    __gtype_name__ = "DeckListHeaderActions"

    add_button = Gtk.Template.Child("add-button")
    import_button = Gtk.Template.Child("import-button")

    def __init__(self, window):
        super().__init__()
        self.window = window

        # XXX: Connect signals to handlers.
        self.add_button.connect("clicked", self.on_add_button_clicked)
        self.import_button.connect("clicked", self.on_import_button_clicked)

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.add_button.disconnect_by_func(self.on_add_button_clicked)
        self.import_button.disconnect_by_func(self.on_import_button_clicked)

    # @Gtk.Template.Callback()
    def on_add_button_clicked(self, button):
        """Handle the BUTTON's clicked signal."""
        self.window.show_view("deck-form", [self.window])

    # @Gtk.Template.Callback()
    def on_import_button_clicked(self, button):
        """Handle the BUTTON's clicked signal."""
        self.window.show_view("deck-import", [self.window])


@Gtk.Template(filename=os.path.join(UI_PATH, "deck-list-item.ui"))
class DeckListItem(Gtk.ListBoxRow):
    """Objects of this class represent items of a list of decks."""
    __gtype_name__ = "DeckListItem"

    icon_image = Gtk.Template.Child("icon-image")
    name_label = Gtk.Template.Child("name-label")
    progress_box = Gtk.Template.Child("progress-box")

    def __init__(self, deck):
        super().__init__()

        self.deck = deck

        # Set icon.
        if not deck.icon:
            self.icon_image.set_from_file(GENERIC_IMAGE)
        else:
            icon_path = os.path.join(MEDIA_DIR, deck.icon.name)
            pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(
                icon_path,
                50,
                50
            )
            self.icon_image.set_from_pixbuf(pixbuf)
        # Set name and description.
        self.name_label.set_markup(
            "<big><b>{}</b></big>\n{}".format(
                deck.name,
                deck.description)
        )
        # Set progress.
        progress = deck.rounds.last().progress
        gauge = ProgressGauge(outer_fraction=progress, scale=0.5)
        self.progress_box.append(gauge)
