"""Package defining GUI components.

This packages contains modules that define components for the
Graphical User Interface (GUI) of the application.

"""
