"""Shortcuts view."""

import os

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# VIEWS
# =====

# NOTE: Glade 3.38 does not support ShortcutsWindow.
# This XML UI file was copy/pasted from GTK documentation.
@Gtk.Template(filename=os.path.join(UI_PATH, "shortcuts-window.ui"))
class ShortcutsWindow(Gtk.ShortcutsWindow):
    """Represents the singleton Shortcuts window of the application."""
    __gtype_name__ = "ShortcutsWindow"

    def __init__(self, window):
        super().__init__()
        self.set_application(window)
        self.present()
