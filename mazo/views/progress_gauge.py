"""Progress gauge component."""

import os

import gi

gi.require_version("GdkPixbuf", "2.0")
gi.require_version("Gtk", "4.0")

from gi.repository import GdkPixbuf, Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# COMPONENTS
# ==========

class ProgressGauge(Gtk.Image):
    """A progress gauge is a circular indicator of progress."""
    def __init__(self, inner_fraction=0, outer_fraction=0, scale=1.0):
        """Fractions are numbers between 0.0 and 1.0, inclusive,
        indicating the part of the gauge to fill in."""
        super().__init__()
        self.template = os.path.join(UI_PATH, "deck-progress-dark.svg")
        self.svg = ""  # SVG document as a string.
        self.width = 100  # pixels.
        self.height = 100  # pixels.
        self.set_pixel_size(self.width*scale)

        # Use an appropriate SVG template for the screen theme (light/dark).
        # See GTK_THEME in https://docs.gtk.org/gtk4/running.html.
        gtk_settings = Gtk.Settings.get_default()
        theme_name = gtk_settings.get_property("gtk-theme-name").lower()
        gtk_theme = os.getenv("GTK_THEME", default="").lower()

        dark_theme_set = (
            ("dark" in gtk_theme) or ("dark" in theme_name)
        )
        if dark_theme_set:
            self.template = os.path.join(
                UI_PATH,
                "deck-progress-light.svg"
            )

        # Read SVG gauge template.
        with open(self.template) as template:
            self.svg = template.read()

        # Prepare tooltip text.
        percentage_learned = "{:.1f}%".format(outer_fraction*100)
        tooltip_text = "{} memorized".format(percentage_learned)

        # Update gauge values in SVG and show the gauge.
        self.fill_inner_gauge(inner_fraction)
        self.fill_outer_gauge(outer_fraction)
        self.set_text(outer_fraction)
        self.set_tooltip_text(tooltip_text)
        self.display(scale)

    def display(self, scale):
        """Display the gauge."""
        loader = GdkPixbuf.PixbufLoader()
        loader.set_size(self.width*scale, self.height*scale)
        loader.write(self.svg.encode())
        loader.close()
        pixbuf = loader.get_pixbuf()
        self.set_from_pixbuf(pixbuf)
        self.show()

    def fill_inner_gauge(self, fraction):
        """Fill the segment of the gauge corresponding to fraction."""
        # Fill and circumference correspond to a single dash length and
        # dash gap in the stroke-dasharray attribute of the SVG circle
        # that represents the progress.
        circumference = 236  # A constant value in the SVG template.
        fill = circumference * fraction
        self.svg = self.svg.replace("~INNER_FILL~", str(fill))

    def fill_outer_gauge(self, fraction):
        """Fill the segment of the gauge corresponding to fraction."""
        # Fill and circumference correspond to a single dash length and
        # dash gap in the stroke-dasharray attribute of the SVG circle
        # that represents the progress.
        circumference = 283  # A constant value in the SVG template.
        fill = circumference * fraction
        self.svg = self.svg.replace("~OUTER_FILL~", str(fill))

    def set_text(self, fraction):
        """Set the text in the center of the gauge as a percentage%."""
        percentage = "{:.1f}%".format(fraction*100)
        self.svg = self.svg.replace("~%~", percentage)
