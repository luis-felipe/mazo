"""Card front view."""

import os

from PIL import Image

from mazo.constants import MEDIA_DIR
from mazo.views.gif_paintable import GifPaintable
from mazo.views.header_actions_study_session import StudySessionHeaderActions
from mazo.views.media_player import MediaPlayer

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# VIEWS
# =====

@Gtk.Template(filename=os.path.join(UI_PATH, "card-front-view.ui"))
class CardFrontView(Gtk.ScrolledWindow):
    """Represents the frontal part of a card in a study session."""
    __gtype_name__ = "CardFrontView"

    picture = Gtk.Template.Child("picture")
    player_box = Gtk.Template.Child("player-box")
    text_label = Gtk.Template.Child("text-label")
    flip_over_button = Gtk.Template.Child("flip-over-button")

    def __init__(self, window, card):
        """window is an instance of mazo.views.AppWindow.
        card is an instance of mazo.models.Card."""
        super().__init__()
        self.name = "card-front-view"
        self.title = "Studying {}".format(card.deck.name)
        self.window = window
        self.study_session = window.study_session
        self.card = card
        self.actions = StudySessionHeaderActions(
            window,
            self.study_session
        )

        # Display card.
        self.display(card)

        # XXX: Connect signals to handlers.
        self.flip_over_button.connect(
            "clicked",
            self.on_flip_over_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.flip_over_button.disconnect_by_func(
            self.on_flip_over_button_clicked
        )
        self.actions.disconnect_signal_handlers()

        if hasattr(self, "player"):
            self.player.disconnect_signal_handlers()

    # @Gtk.Template.Callback()
    def on_flip_over_button_clicked(self, button):
        """Handle the BUTTON's clicked signal."""
        self.window.show_view(
            "card-back",
            [self.window, self.card],
            transition=Gtk.StackTransitionType.SLIDE_UP
        )

    def display(self, card):
        """Display the information of the given CARD."""
        if self.study_session.kind == "VISUAL":
            self.set_image(card)
            self.picture.set_visible(True)
        elif self.study_session.kind == "AURAL":
            sound_path = os.path.join(MEDIA_DIR, card.sound.name)
            self.player = MediaPlayer(sound_path)
            self.player_box.append(self.player)
            self.player_box.set_visible(True)
        elif self.study_session.kind == "TEXTUAL":
            markup = "<span size='xx-large'>" + card.text + "</span>"
            self.text_label.set_markup(markup)
            self.text_label.set_visible(True)

    def set_image(self, card):
        """Set the view's image to card.image."""
        image_path = os.path.join(MEDIA_DIR, card.image.name)
        if (
                image_path.lower().endswith(".gif") and
                Image.open(image_path).is_animated
        ):
            paintable = GifPaintable(image_path)
            self.picture.set_paintable(paintable)
        else:
            self.picture.set_filename(image_path)
