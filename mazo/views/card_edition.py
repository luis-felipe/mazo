"""Card modification view."""

import os

from mazo.constants import MEDIA_DIR
from mazo.forms import CardForm
from mazo.views.card_form import CardFormView


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# VIEWS
# =====

class EditCardFormView(CardFormView):
    """Represents a graphical form to edit a card."""
    def __init__(self, window, card):
        super().__init__(window, card.deck, card)
        self.title = "Editing card"

        # Prepare form for editing.
        self.prefill(card)
        self.save_and_add_another_button.set_visible(False)
        self.save_button.set_label("Update")

    def on_cancel_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        # Show card's back view again.
        self.window.show_view(
            "card-browser",
            [self.window, self.card]
        )

    def on_save_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        # Update card and show card's back view again.
        self.update(self.card)
        self.window.show_view(
            "card-browser",
            [self.window, self.card]
        )

    def prefill(self, card):
        """Prefill the form with the data of the given card."""
        self.text_field_a.set_text(card.text)
        self.text_field_b.set_text(card.text_b)

        if card.image:
            image_path = os.path.join(MEDIA_DIR, card.image.name)
            self.image_field.set_image(image_path)

        if card.sound:
            sound_path = os.path.join(MEDIA_DIR, card.sound.name)
            self.sound_field.set_file_path(sound_path)

    def update(self, card):
        """Update the card with the data filled in the form."""
        data = self.get_data()
        files = self.get_files()
        metaform = CardForm(data, files, instance=card)

        if metaform.is_valid():
            # XXX: Save modifications by simply calling metaform.save()
            #
            # Simply calling metaform.save() is not enough when card
            # modifications include deleting image or sound files
            # (i.e. when setting files["image"] and files["sound"] to
            # None. See CardFormView.get_files method).
            #
            # The only way I found to remove image or sound on update,
            # was to not save changes using the CardForm directly but
            # to do it using the model's save method instead, as
            # follows.
            card = metaform.save(commit=False)

            if files["image"] is None:
                card.image = None
            if files["sound"] is None:
                card.sound = None

            card.save()
        else:
            for field, error in metaform.errors.as_data().items():
                print(f"{field}: {error}")
