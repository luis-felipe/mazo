"""Deck detail view."""

import os

from django.db import transaction
from django.utils import timezone

from mazo.constants import MEDIA_DIR
from mazo.models import Round
from mazo.views.progress_gauge import ProgressGauge
from mazo.views.workers import AsyncWorker

import gi

gi.require_version("Gdk", "4.0")
gi.require_version("GdkPixbuf", "2.0")
gi.require_version("Gtk", "4.0")

from gi.repository import GdkPixbuf, Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")
GENERIC_IMAGE = os.path.join(UI_PATH, "generic-image.svg")


# VIEWS
# =====

@Gtk.Template(filename=os.path.join(UI_PATH, "deck-detail-view.ui"))
class DeckDetailView(Gtk.ScrolledWindow):
    """Objects of this class represent the details view of decks."""
    __gtype_name__ = "DeckDetailView"

    deck_name_label = Gtk.Template.Child("deck-name")
    deck_description_label = Gtk.Template.Child("deck-description")
    card_count_label = Gtk.Template.Child("card-count-label")
    round_count_label = Gtk.Template.Child("round-count-label")
    deck_icon_image = Gtk.Template.Child("deck-icon")
    deck_progress_box = Gtk.Template.Child("deck-progress-box")
    visual_study_button = Gtk.Template.Child("visual-study-button")
    aural_study_button = Gtk.Template.Child("aural-study-button")
    textual_study_button = Gtk.Template.Child("textual-study-button")
    study_again_box = Gtk.Template.Child("study-again-box")
    study_again_button = Gtk.Template.Child("study-again-button")
    no_cards_box = Gtk.Template.Child("no-cards-box")
    add_cards_button = Gtk.Template.Child("add-cards-button")

    progress_box = Gtk.Template.Child("progress-box")
    progress_spinner = Gtk.Template.Child("progress-spinner")

    error_box = Gtk.Template.Child("error-box")
    error_buffer = Gtk.Template.Child("error-buffer")

    def __init__(self, window, deck):
        super().__init__()
        self.id = timezone.now().strftime("%H%S")
        self.name = "deck-detail-view-{}".format(self.id)
        self.title = deck.name
        self.actions = DeckHeaderActions(window, deck)
        self.window = window
        self.deck = deck

        # Display DECK details.
        self.deck_name_label.set_markup("<b>{}</b>".format(deck.name))
        self.deck_description_label.set_text(deck.description)
        self.card_count_label.set_text(str(deck.cards.count()))
        self.round_count_label.set_text(str(deck.rounds.count()))

        if deck.icon:
            icon_path = os.path.join(MEDIA_DIR, deck.icon.name)
            self.set_icon(icon_path)
        else:
            self.set_icon(GENERIC_IMAGE)

        progress = deck.rounds.last().progress
        gauge = ProgressGauge(outer_fraction=progress)
        self.deck_progress_box.append(gauge)

        # Show study options or "study again" message.
        ongoing_rounds = deck.rounds.filter(finish_date__isnull=True)

        if ongoing_rounds.exists():
            self.show_study_options(deck)
        else:
            self.study_again_box.set_visible(True)

        # Show "no cards" message.
        if deck.cards.count() == 0:
            self.no_cards_box.set_visible(True)

        # XXX: Connect signals to handlers.
        self.add_cards_button.connect(
            "clicked",
            self.on_add_cards_button_clicked
        )
        self.aural_study_button.connect(
            "clicked",
            self.on_aural_study_button_clicked
        )
        self.study_again_button.connect(
            "clicked",
            self.on_study_again_button_clicked
        )
        self.textual_study_button.connect(
            "clicked",
            self.on_textual_study_button_clicked
        )
        self.visual_study_button.connect(
            "clicked",
            self.on_visual_study_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.add_cards_button.disconnect_by_func(
            self.on_add_cards_button_clicked
        )
        self.aural_study_button.disconnect_by_func(
            self.on_aural_study_button_clicked
        )
        self.study_again_button.disconnect_by_func(
            self.on_study_again_button_clicked
        )
        self.textual_study_button.disconnect_by_func(
            self.on_textual_study_button_clicked
        )
        self.visual_study_button.disconnect_by_func(
            self.on_visual_study_button_clicked
        )
        self.actions.disconnect_signal_handlers()

    # @Gtk.Template.Callback()
    def on_add_cards_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.window.show_view(
            "card-creation",
            [self.window, self.deck]
        )

    # @Gtk.Template.Callback()
    def on_aural_study_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.start_session("AURAL")

    def on_round_creation_finished(self, worker, result, handler_data):
        """Handle the RESULT of the asynchronous operation performed by
        WORKER.

        WORKER (mazo.views.RoundCreationWorker)
          The worker responsible for starting a new round
          asynchronously.

        RESULT (Gio.AsyncResult)
          The asynchronous result of the asynchronous operation.

        HANDLER_DATA (None)
          Additional data passed to this handler by the worker when the
          job is done. It should be None in this case.

        """
        # Check result's value.
        outcome = worker.return_value(result)

        if isinstance(outcome, Round):
            # Redisplay the deck detail view.
            self.window.show_view(
                "deck-detail",
                [self.window, self.deck]
            )
        else:
            # Hide progress box.
            self.progress_spinner.stop()
            self.progress_box.set_visible(False)

            # Display error box.
            errors = outcome
            error_report = ""

            for error, description in errors.items():
                error_report += f"{error}: {description}\n"

            self.error_buffer.set_text(error_report)
            self.error_box.set_visible(True)

    # @Gtk.Template.Callback()
    def on_study_again_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        # Display progress box.
        self.study_again_box.set_visible(False)
        self.progress_spinner.start()
        self.progress_box.set_visible(True)

        # Start a new study round.
        async_worker = RoundCreationWorker(
            operation_inputs=(self.deck,),
            operation_callback=self.on_round_creation_finished
        )
        async_worker.start()

    # @Gtk.Template.Callback()
    def on_textual_study_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.start_session("TEXTUAL")

    # @Gtk.Template.Callback()
    def on_visual_study_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.start_session("VISUAL")

    def set_icon(self, path):
        """Set icon in the view to the image in PATH."""
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(
            path,
            100,
            100
        )
        self.deck_icon_image.set_from_pixbuf(pixbuf)

    def show_study_options(self, deck):
        """Show study options available for the deck."""
        # Handle visibility of study buttons.
        if deck.has_visual_cards():
            self.visual_study_button.set_visible(True)
        if deck.has_aural_cards():
            self.aural_study_button.set_visible(True)
        if deck.has_textual_cards():
            self.textual_study_button.set_visible(True)

        # Handle sensitivity of study buttons according to learning
        # progress.
        if deck.is_aspect_learned("VISUAL"):
            self.visual_study_button.set_sensitive(False)
            self.visual_study_button.set_label(
                "Visual aspect learned!"
            )

        if deck.is_aspect_learned("AURAL"):
            self.aural_study_button.set_sensitive(False)
            self.aural_study_button.set_label(
                "Aural aspect learned!"
            )

        if deck.is_aspect_learned("TEXTUAL"):
            self.textual_study_button.set_sensitive(False)
            self.textual_study_button.set_label(
                "Textual aspect learned!"
            )

    def start_session(self, method):
        """Start a study session for the given method."""
        # Start a study session (or continue an ongoing one).
        session = self.deck.start_session(method)
        self.window.study_session = session

        # Display first card to study.
        review = session.next_review()
        self.window.show_view(
            "card-front",
            [self.window, review.aspect.card]
        )


# COMPONENTS
# ==========

@Gtk.Template(filename=os.path.join(UI_PATH, "deck-header-actions.ui"))
class DeckHeaderActions(Gtk.Box):
    """Objects of this class represent the actions available for the
    Deck Detail View."""
    __gtype_name__ = "DeckHeaderActions"

    add_button = Gtk.Template.Child("add-card-button")
    browse_deck_button = Gtk.Template.Child("browse-deck-button")
    credits_button = Gtk.Template.Child("credits-button")
    delete_button = Gtk.Template.Child("delete-deck-button")
    edit_button = Gtk.Template.Child("edit-deck-button")
    export_button = Gtk.Template.Child("export-button")
    more_options_popover = Gtk.Template.Child("more-options-popover")

    def __init__(self, window, deck):
        super().__init__()
        self.window = window
        self.deck = deck

        # Enable deck browsing.
        if deck.cards.count() > 0:
            self.browse_deck_button.set_sensitive(True)

        # XXX: Connect signals to handlers.
        self.add_button.connect(
            "clicked",
            self.on_add_button_clicked
        )
        self.browse_deck_button.connect(
            "clicked",
            self.on_browse_deck_button_clicked
        )
        self.credits_button.connect(
            "clicked",
            self.on_credits_button_clicked
        )
        self.delete_button.connect(
            "clicked",
            self.on_delete_button_clicked
        )
        self.edit_button.connect(
            "clicked",
            self.on_edit_button_clicked
        )
        self.export_button.connect(
            "clicked",
            self.on_export_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.add_button.disconnect_by_func(
            self.on_add_button_clicked
        )
        self.browse_deck_button.disconnect_by_func(
            self.on_browse_deck_button_clicked
        )
        self.credits_button.disconnect_by_func(
            self.on_credits_button_clicked
        )
        self.delete_button.disconnect_by_func(
            self.on_delete_button_clicked
        )
        self.edit_button.disconnect_by_func(
            self.on_edit_button_clicked
        )
        self.export_button.disconnect_by_func(
            self.on_export_button_clicked
        )

    # @Gtk.Template.Callback()
    def on_add_button_clicked(self, button):
        """Handle the BUTTON's clicked signal."""
        # XXX: Gtk.Popover is not popping down itself anymore.
        #
        # Clicking on any of the buttons in DeckHeaderActions used to
        # pop the popover down too, but now it doesn't. Instead, users
        # are left with a residual modal layer that prevents them from
        # interacting with the application. They can't even close it.
        #
        # For this reason, I had to force the GtkPopover.popdown() in
        # all the children buttons of popover when they are clicked.
        self.more_options_popover.popdown()

        self.window.show_view(
            "card-creation",
            [self.window, self.deck]
        )

    # @Gtk.Template.Callback()
    def on_browse_deck_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.more_options_popover.popdown()  # XXX: Popdown workaround.

        card = self.deck.cards.first()
        self.window.show_view("card-browser", [self.window, card])

    # @Gtk.Template.Callback()
    def on_credits_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.more_options_popover.popdown()  # XXX: Popdown workaround.

        self.window.show_view("deck-credits", [self.window, self.deck])

    # @Gtk.Template.Callback()
    def on_delete_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.more_options_popover.popdown()  # XXX: Popdown workaround.

        # Confirm deck removal.
        message = (
            "Do you really want to delete the deck\n«{}»?".
            format(self.deck.name)
        )
        # FIXME: Gtk.MessageDialog init API does not work in Python (?).
        # That's why I have to add_button and set_property, etc.
        dialog = Gtk.MessageDialog()
        dialog.add_button("Cancel", Gtk.ButtonsType.CANCEL)
        dialog.add_button("OK", Gtk.ButtonsType.OK)
        dialog.connect("response", self.delete_deck)
        dialog.set_modal(True)
        dialog.set_transient_for(self.window)
        dialog.set_title("Delete deck?")
        dialog.set_property("secondary_text", message)
        dialog.set_property("message_type", Gtk.MessageType.WARNING)
        dialog.show()

    # @Gtk.Template.Callback()
    def on_edit_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.more_options_popover.popdown()  # XXX: Popdown workaround.

        self.window.show_view(
            "deck-form",
            [self.window],
            {"deck": self.deck}
        )

    # @Gtk.Template.Callback()
    def on_export_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.more_options_popover.popdown()  # XXX: Popdown workaround.

        self.window.show_view("deck-export", [self.window, self.deck])

    def delete_deck(self, dialog, response):
        """Handle the response of the deck deletion dialog."""
        # FIXME: Better use: if response == Gtk.ResponseType.OK:
        #
        # That would be possible if I could pass the "buttons" argument
        # Gtk.ButtonsType.OK_CANCEL normally when initializing the
        # dialog in self.on_delete_button_clicked.
        if response == 1:
            # Delete deck.
            self.deck.delete()
            # Show deck list view.
            self.window.show_view("deck-list", [self.window])

        dialog.destroy()


# ASYNCHRONOUS WORKERS
# ====================

class RoundCreationWorker(AsyncWorker):
    """Represents a worker object that creates a new study round
    asynchronously."""
    def __init__(self, operation_inputs=(), operation_callback=None):
        super().__init__(
            operation_inputs=operation_inputs,
            operation_callback=operation_callback
        )

    def work(self, deck):
        """Start a new study round for the given deck.

        DECK (mazo.models.Deck)
          The deck which will start the new round.

        RETURN VALUE (mazo.models.Round or error dict)
          Return the newly created round if all goes well. Otherwise,
          return a dictionary of errors in the form

          {"Error": "Description."}

        """
        outcome = None

        try:
            with transaction.atomic():
                # Finish the current study round.
                deck.rounds.last().finish()

                # Reset all card aspects to unlearned state.
                deck.unlearn_cards()

                # Start a new study round.
                outcome = deck.start_round()
        except BaseException as error:
            outcome = {"RoundCreationError": error}

        return outcome
