"""Deck form view."""

import os

from django.core.files.uploadedfile import SimpleUploadedFile

from mazo.constants import MEDIA_DIR
from mazo.forms import DeckForm
from mazo.models import Deck
from mazo.views.file_chooser import FileChooserDialog

import gi

gi.require_version("GdkPixbuf", "2.0")
gi.require_version("Gtk", "4.0")

from gi.repository import GdkPixbuf, Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")
GENERIC_IMAGE = os.path.join(UI_PATH, "generic-image.svg")


# VIEWS
# =====

@Gtk.Template(filename=os.path.join(UI_PATH, "deck-form.ui"))
class DeckFormView(Gtk.ScrolledWindow):
    """Objects of this class represent forms to add or edit decks."""
    __gtype_name__ = "DeckFormView"

    deck_name_entry = Gtk.Template.Child("deck-name")
    deck_description_buffer = Gtk.Template.Child(
        "deck-description-buffer"
    )
    deck_credits_buffer = Gtk.Template.Child("deck-credits-buffer")
    deck_icon_image = Gtk.Template.Child("icon-image")
    icon_path_label = Gtk.Template.Child("deck-icon-path")
    icon_button = Gtk.Template.Child("icon-button")
    save_button = Gtk.Template.Child("save-button")
    cancel_button = Gtk.Template.Child("cancel-button")

    def __init__(self, window, deck=None):
        super().__init__()
        self.name = "deck-form-view"
        self.title = "Deck Form View"
        self.actions = None
        self.window = window
        self.deck = deck

        # Set form according to mode.
        if self.is_editing():
            self.title = "Editing Deck {}".format(deck.name)
            self.prefill(deck)
            self.save_button.set_label("Update")
        else:
            self.title = "New Deck"
            self.set_icon(GENERIC_IMAGE)
            self.save_button.set_label("Create")
            self.save_button.set_sensitive(False)

        # XXX: Connect signals to handlers.
        self.cancel_button.connect(
            "clicked",
            self.on_cancel_button_clicked
        )
        self.icon_button.connect(
            "clicked",
            self.on_icon_button_clicked
        )
        self.deck_name_entry.connect(
            "changed",
            self.on_name_changed
        )
        self.save_button.connect(
            "clicked",
            self.on_save_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.cancel_button.disconnect_by_func(
            self.on_cancel_button_clicked
        )
        self.icon_button.disconnect_by_func(
            self.on_icon_button_clicked
        )
        self.deck_name_entry.disconnect_by_func(
            self.on_name_changed
        )
        self.save_button.disconnect_by_func(
            self.on_save_button_clicked
        )

    # @Gtk.Template.Callback()
    def on_cancel_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        if self.is_editing():
            self.window.show_view(
                "deck-detail",
                [self.window, self.deck]
            )
        else:
            self.window.show_view("deck-list", [self.window])

    # @Gtk.Template.Callback()
    def on_icon_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        # Show image chooser dialog.
        last_dir = self.window.last_dir_visited
        dialog = FileChooserDialog(
            self.window,
            self.on_icon_selected,
            directory=last_dir,
            filter_kind="images"
        )
        dialog.show()

    def on_icon_selected(self, path):
        """Handle deck icon selection in icon dialog."""
        if path:
            self.set_icon(path)
            self.window.last_dir_visited = os.path.dirname(path)

    # @Gtk.Template.Callback()
    def on_name_changed(self, entry):
        """Handle ENTRY's changed signal."""
        # Enable self.save_button only if there is a name.
        if self.deck_name_entry.get_text_length() == 0:
            self.save_button.set_sensitive(False)
            self.deck_name_entry.set_icon_from_icon_name(
                Gtk.EntryIconPosition.SECONDARY,
                "dialog-warning-symbolic"
            )
        else:
            self.save_button.set_sensitive(True)
            self.deck_name_entry.set_icon_from_icon_name(
                Gtk.EntryIconPosition.SECONDARY,
                ""
            )

    # @Gtk.Template.Callback()
    def on_save_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        if self.is_editing():
            # Update deck.
            self.update(self.deck)
            # Show deck detail view.
            self.window.show_view(
                "deck-detail",
                [self.window, self.deck]
            )
        else:
            # Create new deck.
            self.create()
            # Show deck list view.
            self.window.show_view("deck-list", [self.window])

    def create(self):
        """Create a new deck using the data in the form."""
        data = self.get_data()
        files = self.get_files()
        metaform = DeckForm(data, files)

        if metaform.is_valid():
            metaform.save()
        else:
            for field, error in metaform.errors.as_data().items():
                print(f"{field}: {error}")

    def get_data(self):
        """Get the data from the form's non-file fields."""
        data = {
            "name": self.deck_name_entry.get_text(),
            "description": self.deck_description_buffer.get_text(
                self.deck_description_buffer.get_start_iter(),
                self.deck_description_buffer.get_end_iter(),
                False  # Don't include invisible text.
            ),
            "credits": self.deck_credits_buffer.get_text(
                self.deck_credits_buffer.get_start_iter(),
                self.deck_credits_buffer.get_end_iter(),
                False  # Don't include invisible text.
            )
        }

        return data

    def get_files(self):
        """Return a dictionary of SimpleUploadedFiles or an empty
        dictionary, if there are no files to upload.
        (see django.core.files.uploadedfile)."""
        icon_path = self.icon_path_label.get_text()
        icon_basename = os.path.basename(icon_path)
        files = {}

        if os.path.exists(icon_path):
            with open(icon_path, "rb") as icon_file:
                icon_data = icon_file.read()
                files = {
                    "icon": SimpleUploadedFile(icon_basename, icon_data)
                }

        return files

    def is_editing(self):
        """Return True if the form is in EDITION mode."""
        return type(self.deck) is Deck

    def prefill(self, deck):
        """Prefill form with DECK data for edition."""
        self.deck_name_entry.set_text(deck.name)
        self.deck_description_buffer.set_text(deck.description)
        self.deck_credits_buffer.set_text(deck.credits)

        if deck.icon:
            icon_path = os.path.join(MEDIA_DIR, deck.icon.name)
            self.set_icon(icon_path)
            self.icon_path_label.set_text(deck.icon.name)
        else:
            self.icon_path_label.set_text("There is no icon selected.")

    def set_icon(self, path):
        """Set icon in the form to the image in PATH."""
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(
            path,
            100,
            100
        )
        self.deck_icon_image.set_from_pixbuf(pixbuf)
        self.icon_path_label.set_text(path)

    def update(self, deck):
        """Update DECK with the data filled in the form."""
        data = self.get_data()
        files = self.get_files()
        metaform = DeckForm(data, files, instance=deck)

        if metaform.is_valid():
            if metaform.has_changed():
                metaform.save()
        else:
            for field, error in metaform.errors.as_data().items():
                print(f"{field}: {error}")
