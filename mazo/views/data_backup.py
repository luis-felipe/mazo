"""Data back up component."""

import os

from django.utils import timezone

from mazo.ie import back_up_data
from mazo.views.file_chooser import FileChooserDialog
from mazo.views.header_actions_leave import LeaveHeaderAction
from mazo.views.workers import AsyncWorker

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# VIEWS
# =====

@Gtk.Template(filename=os.path.join(UI_PATH, "data-back-up-view.ui"))
class DataBackUpView(Gtk.ScrolledWindow):
    __gtype_name__ = "DataBackUpView"

    error_box = Gtk.Template.Child("error-box")
    error_buffer = Gtk.Template.Child("error-buffer")
    folder_path_label = Gtk.Template.Child("folder-path-label")
    back_up_button = Gtk.Template.Child("back-up-button")
    leave_button = Gtk.Template.Child("leave-button")
    progress_box = Gtk.Template.Child("progress-box")
    progress_spinner = Gtk.Template.Child("progress-spinner")
    select_folder_button = Gtk.Template.Child("select-folder-button")
    success_box = Gtk.Template.Child("success-box")

    def __init__(self, window):
        super().__init__()
        self.id = timezone.now().strftime("%H%S")
        self.name = "data-back-up-view-{}".format(self.id)
        self.title = "Backing up data"
        self.window = window
        self.actions = LeaveHeaderAction(
            window,
            "deck-list",
            [window]
        )
        self.destination_path = ""

        # XXX: Connect signals to handlers.
        self.select_folder_button.connect(
            "clicked",
            self.on_select_folder_button_clicked
        )
        self.back_up_button.connect(
            "clicked",
            self.on_back_up_button_clicked
        )
        self.leave_button.connect(
            "clicked",
            self.on_leave_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.select_folder_button.disconnect_by_func(
            self.on_select_folder_button_clicked
        )
        self.back_up_button.disconnect_by_func(
            self.on_back_up_button_clicked
        )
        self.leave_button.disconnect_by_func(
            self.on_leave_button_clicked
        )
        self.actions.disconnect_signal_handlers()

    # @Gtk.Template.Callback()
    def on_select_folder_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        last_dir = self.window.last_dir_visited
        dialog = FileChooserDialog(
            self.window,
            self.on_folder_selected,
            directory=last_dir
        )
        dialog.show()

    def on_folder_selected(self, path):
        """Handle folder selection in file chooser dialog."""
        if path:
            self.destination_path = path
            self.folder_path_label.set_text(path)
            self.back_up_button.set_sensitive(True)
            self.window.last_dir_visited = os.path.dirname(path)

    # @Gtk.Template.Callback()
    def on_back_up_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        button.set_visible(False)
        self.select_folder_button.set_sensitive(False)
        self.progress_spinner.start()
        self.progress_box.set_visible(True)

        # Back up data.
        async_worker = AsyncWorker(
            operation=back_up_data,
            operation_inputs=(self.destination_path,),
            operation_callback=self.on_back_up_finished
        )
        async_worker.start()

    def on_back_up_finished(self, worker, result, handler_data):
        """Handle the RESULT of the asynchronous operation performed by
        WORKER.

        WORKER (mazo.views.AsyncWorker)
          The worker responsible for backing up data asynchronously.

        RESULT (Gio.AsyncResult)
          The asynchronous result of the asynchronous operation.

        HANDLER_DATA (None)
          Additional data passed to this handler by the worker when the
          job is done. It should be None in this case.

        """
        # Check result's value.
        outcome = worker.return_value(result)

        # Update user interface.
        self.progress_spinner.stop()
        self.progress_box.set_visible(False)
        self.leave_button.set_visible(True)

        if isinstance(outcome, str) and os.path.exists(outcome):
            self.success_box.set_visible(True)
        else:
            errors = outcome  # Since export failed.
            error_report = ""

            for error, description in errors.items():
                error_report += f"{error}: {description}\n"

            self.error_buffer.set_text(error_report)
            self.error_box.set_visible(True)

    # @Gtk.Template.Callback()
    def on_leave_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.window.show_view("deck-list", [self.window])
