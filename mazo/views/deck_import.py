"""Deck import view."""

import os

from mazo.ie import import_deck
from mazo.models import Deck
from mazo.views.file_chooser import FileChooserDialog
from mazo.views.header_actions_leave import LeaveHeaderAction
from mazo.views.workers import AsyncWorker

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# VIEWS
# =====

@Gtk.Template(filename=os.path.join(UI_PATH, "deck-import-view.ui"))
class DeckImportView(Gtk.ScrolledWindow):
    __gtype_name__ = "DeckImportView"

    error_box = Gtk.Template.Child("error-box")
    error_buffer = Gtk.Template.Child("error-buffer")
    file_path_label = Gtk.Template.Child("file-path-label")
    import_button = Gtk.Template.Child("import-button")
    leave_button = Gtk.Template.Child("leave-button")
    progress_box = Gtk.Template.Child("progress-box")
    progress_spinner = Gtk.Template.Child("progress-spinner")
    select_file_button = Gtk.Template.Child("select-file-button")
    success_box = Gtk.Template.Child("success-box")

    def __init__(self, window):
        super().__init__()
        self.name = "deck-import-view"
        self.title = "Importing a deck"
        self.window = window
        self.actions = LeaveHeaderAction(
            window,
            "deck-list",
            [window]
        )
        self.archive_path = ""

        # XXX: Connect signals to handlers.
        self.select_file_button.connect(
            "clicked",
            self.on_select_file_button_clicked
        )
        self.import_button.connect(
            "clicked",
            self.on_import_button_clicked
        )
        self.leave_button.connect(
            "clicked",
            self.on_leave_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.select_file_button.disconnect_by_func(
            self.on_select_file_button_clicked
        )
        self.import_button.disconnect_by_func(
            self.on_import_button_clicked
        )
        self.leave_button.disconnect_by_func(
            self.on_leave_button_clicked
        )
        self.actions.disconnect_signal_handlers()

    # @Gtk.Template.Callback()
    def on_select_file_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        last_dir = self.window.last_dir_visited
        dialog = FileChooserDialog(
            self.window,
            self.on_file_selected,
            directory=last_dir,
            filter_kind="archives"
        )
        dialog.show()

    def on_file_selected(self, path):
        """Handle archive selection in file chooser dialog."""
        if path:
            self.archive_path = path
            self.file_path_label.set_text(path)
            self.import_button.set_sensitive(True)
            self.window.last_dir_visited = os.path.dirname(path)

    # @Gtk.Template.Callback()
    def on_import_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        button.set_visible(False)
        self.select_file_button.set_sensitive(False)
        self.progress_spinner.start()
        self.progress_box.set_visible(True)

        # Import deck.
        async_worker = AsyncWorker(
            operation=import_deck,
            operation_inputs=(self.archive_path,),
            operation_callback=self.on_import_finished
        )
        async_worker.start()

    def on_import_finished(self, worker, result, handler_data):
        """Handle the RESULT of the asynchronous operation performed by
        WORKER.

        WORKER (mazo.views.AsyncWorker)
          The worker responsible for importing the deck
          asynchronously.

        RESULT (Gio.AsyncResult)
          The asynchronous result of the asynchronous operation.

        HANDLER_DATA (None)
          Additional data passed to this handler by the worker when the
          job is done. It should be None in this case.

        """
        # Check result's value.
        outcome = worker.return_value(result)

        # Update user interface.
        self.progress_spinner.stop()
        self.progress_box.set_visible(False)
        self.leave_button.set_visible(True)

        if isinstance(outcome, Deck):
            self.success_box.set_visible(True)
        else:
            error_report = ""

            for error, description in outcome.items():
                error_report += f"{error}: {description}\n"

            self.error_buffer.set_text(error_report)
            self.error_box.set_visible(True)

    # @Gtk.Template.Callback()
    def on_leave_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.window.show_view("deck-list", [self.window])
