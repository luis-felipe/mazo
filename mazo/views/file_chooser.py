"""File chooser view."""

import os
from urllib.parse import unquote, urlparse

from mazo.views.media_player import MediaPlayer

import gi

gi.require_version("Gdk", "4.0")
gi.require_version("Gtk", "4.0")

from gi.repository import Gio, Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# VIEWS
# =====

class FileChooserDialog(Gtk.FileChooserDialog):
    """Represents a generic file chooser for the kinds of files used in
    Mazo.

    PARENT (Gtk.ApplicationWindow)
      The application window this dialog is spawned from.

    HANDLER (callable)
      A function or method that takes one argument that can be one of
      the following:

      + The path to the selected file, as a string.
      + False. Meaning that the user didn't select any file.

    FILTER_KIND (string)
      Optional. The kind of files to display. It can be one of the
      following strings: "archives", "images", "sounds".

      If not provided, the dialog will display folders only for
      selection.

    DIRECTORY (string)
      Optional. Path to a directory to display by default.

    """
    FILE_TYPES = {
        "archives": [
            "application/zip",
        ],
        "images": [
            "image/gif",
            "image/jpeg",
            "image/png",
            "image/svg+xml"
        ],
        "sounds": [
            "audio/mp4",
            "audio/mpeg",
            "audio/x-oppus+ogg",
            "audio/x-vorbis+ogg",
            "audio/x-wav"
        ]
    }

    def __init__(self, parent, handler, filter_kind="", directory=""):
        super().__init__()
        self.set_transient_for(parent)
        self.set_modal(True)
        self.set_action(Gtk.FileChooserAction.SELECT_FOLDER)
        self.filter_kind = filter_kind
        self.response_handler = handler

        # Filter file types if necessary.
        if filter_kind:
            self.set_action(Gtk.FileChooserAction.OPEN)
            file_filter = Gtk.FileFilter()
            file_filter.set_name(filter_kind)

            for mime_type in FileChooserDialog.FILE_TYPES[filter_kind]:
                file_filter.add_mime_type(mime_type)

            self.add_filter(file_filter)

        # Set dialog title.
        if filter_kind == "archives":
            self.set_title("Choose an archive")
        elif filter_kind == "images":
            self.set_title("Choose an image")
        elif filter_kind == "sounds":
            self.set_title("Choose a sound")
        else:
            self.set_title("Choose a folder")

        # Add buttons.
        self.add_buttons(
            "Cancel",
            Gtk.ResponseType.CANCEL,
            "Select",
            Gtk.ResponseType.ACCEPT
        )

        # Show directory if any.
        if os.path.exists(directory):
            gfile = Gio.File.new_for_path(directory)
            self.set_current_folder(gfile)

        # Connect signals.
        self.connect("response", self.on_response)

        # TODO: Find a way to get the file preview back.
        #
        # GTK 4 removed the preview widget from GtkFileChooser, so
        # this signal is gone.
        #
        # self.connect("update-preview", self.on_update_preview)

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.disconnect_by_func(self.on_response)

    def on_response(self, dialog, response_id):
        """Handle this dialog's response signal."""
        if response_id == Gtk.ResponseType.ACCEPT:
            gfile = self.get_file()
            self.response_handler(gfile.get_path())
        else:
            self.response_handler(False)

        self.disconnect_signal_handlers()  # XXX: See method definition.
        self.destroy()

    def on_update_preview(self, chooser):  # TODO: Get this feature back.
        """Handle this file chooser's update-preview signal."""
        # Clean previewer.
        dir_image = Gtk.Image.new_from_icon_name(
            "inode-directory-symbolic",
            Gtk.IconSize.DIALOG
        )
        self.set_preview_widget(dir_image)

        # Update preview.
        uri = self.get_uri()

        if uri:
            path = unquote(urlparse(uri).path)

            if os.path.exists(path) and os.path.isfile(path):
                if self.filter_kind == "images":
                    previewer = Gtk.Picture.new_for_filename(path)
                else:
                    previewer = MediaPlayer(path)

                self.set_preview_widget(previewer)
