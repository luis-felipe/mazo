"""Deck store view."""

import os

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# VIEWS
# =====

@Gtk.Template(filename=os.path.join(UI_PATH, "deck-store-view.ui"))
class DeckStoreView(Gtk.ScrolledWindow):
    """Represents an informative view pointing to Mazo Store, where
    people can get ready-made decks."""
    __gtype_name__ = "DeckStoreView"

    back_button = Gtk.Template.Child("back-button")

    def __init__(self, window):
        super().__init__()
        self.name = "deck-store-view"
        self.title = "Store"
        self.actions = None
        self.window = window

        # XXX: Connect signals to handlers.
        self.back_button.connect(
            "clicked",
            self.on_back_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.back_button.disconnect_by_func(
            self.on_back_button_clicked
        )

    # @Gtk.Template.Callback()
    def on_back_button_clicked(self, button):
        """Handle the BUTTON's clicked signal."""
        self.window.show_view("deck-list", [self.window])
