"""Media player component."""

import os

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# COMPONENTS
# ==========

@Gtk.Template(filename=os.path.join(UI_PATH, "media-player.ui"))
class MediaPlayer(Gtk.Video):
    """Represents a media player for the file in ``path``.

    Initialization parameters:

    path (string)
      An absolute path to an audio or video file.

    """
    __gtype_name__ = "MediaPlayer"

    def __init__(self, path):
        super().__init__()
        self.set_filename(path)

        # XXX: Connect signals to handlers.
        self.connect(
            "unrealize",
            self.on_unrealize
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.disconnect_by_func(
            self.on_unrealize
        )

    # @Gtk.Template.Callback()
    def on_unrealize(self, *args):
        """Handle self's unrealize signal."""
        # Make sure to stop any ongoing playback.
        stream = self.get_media_stream()
        stream.pause()
