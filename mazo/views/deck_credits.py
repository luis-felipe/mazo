"""Deck credits view."""

import os

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# VIEWS
# =====

@Gtk.Template(filename=os.path.join(UI_PATH, "deck-credits-view.ui"))
class DeckCreditsView(Gtk.ScrolledWindow):
    """Objects of this class represent a view listing people who have
    contributed to the development of a deck."""
    __gtype_name__ = "DeckCreditsView"

    credits_buffer = Gtk.Template.Child("credits-buffer")
    back_button = Gtk.Template.Child("back-button")

    def __init__(self, window, deck):
        super().__init__()
        self.name = "deck-credits-view"
        self.title = "Credits of " + deck.name
        self.actions = None
        self.window = window
        self.deck = deck

        # Show credits (if any).
        self.credits_buffer.set_text(deck.credits)

        # XXX: Connect signals to handlers.
        self.back_button.connect(
            "clicked",
            self.on_back_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.back_button.disconnect_by_func(
            self.on_back_button_clicked
        )

    # @Gtk.Template.Callback()
    def on_back_button_clicked(self, button):
        """Handle the BUTTON's clicked signal."""
        self.window.show_view(
            "deck-detail",
            [self.window, self.deck]
        )
