"""Card back view."""

import os

from django.utils import timezone

from PIL import Image

from mazo.constants import MEDIA_DIR, REVIEW_INTERVAL
from mazo.views.gif_paintable import GifPaintable
from mazo.views.header_actions_study_session import StudySessionHeaderActions
from mazo.views.media_player import MediaPlayer

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# VIEWS
# =====

@Gtk.Template(filename=os.path.join(UI_PATH, "card-back-view.ui"))
class CardBackView(Gtk.ScrolledWindow):
    """Represents the back of a card in a study session."""
    __gtype_name__ = "CardBackView"

    picture = Gtk.Template.Child("picture")
    player_box = Gtk.Template.Child("player-box")
    text_label = Gtk.Template.Child("text-label")
    text_b_label = Gtk.Template.Child("text-b-label")
    correct_button = Gtk.Template.Child("correct-button")
    incorrect_button = Gtk.Template.Child("incorrect-button")
    learned_button = Gtk.Template.Child("learned-button")

    def __init__(self, window, card):
        super().__init__()
        self.name = "card-back-view"
        self.title = "Studying {}".format(card.deck.name)
        self.window = window
        self.study_session = window.study_session
        self.study_method = self.study_session.kind
        self.card = card
        self.aspect = card.aspects.filter(kind=self.study_method).first()
        self.review = self.aspect.get_scheduled_review()
        self.actions = StudySessionHeaderActions(
            window,
            self.study_session
        )

        # Show Learned! button if appropriate.
        if self.aspect.seems_memorized():
            self.learned_button.set_visible(True)

        # Display card.
        self.display(card)

        # XXX: Connect signals to handlers.
        self.correct_button.connect(
            "clicked",
            self.on_correct_button_clicked
        )
        self.incorrect_button.connect(
            "clicked",
            self.on_incorrect_button_clicked
        )
        self.learned_button.connect(
            "clicked",
            self.on_learned_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.correct_button.disconnect_by_func(
            self.on_correct_button_clicked
        )
        self.incorrect_button.disconnect_by_func(
            self.on_incorrect_button_clicked
        )
        self.learned_button.disconnect_by_func(
            self.on_learned_button_clicked
        )
        self.actions.disconnect_signal_handlers()

        if hasattr(self, "player"):
            self.player.disconnect_signal_handlers()

    # @Gtk.Template.Callback()
    def on_correct_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        # Record correct answer.
        self.review.respond_correctly()

        # Set next review.
        date = timezone.now() + REVIEW_INTERVAL
        self.aspect.schedule_review(date)

        # Show next card.
        self.show_next_card()

    # @Gtk.Template.Callback()
    def on_incorrect_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        # Record correct answer.
        self.review.respond_incorrectly()

        # Set next review.
        date = timezone.now() + REVIEW_INTERVAL/2
        self.aspect.schedule_review(date)

        # Show next card.
        self.show_next_card()

    # @Gtk.Template.Callback()
    def on_learned_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        # Record correct answer.
        self.review.respond_memorized()

        # Show next card.
        self.show_next_card()

    def display(self, card):
        """Display the information of the given CARD."""
        # Display image only if there is one.
        if card.is_visual():
            image_path = os.path.join(MEDIA_DIR, card.image.name)

            if (
                    image_path.lower().endswith(".gif") and
                    Image.open(image_path).is_animated
            ):
                paintable = GifPaintable(image_path)
                self.picture.set_paintable(paintable)
            else:
                self.picture.set_filename(image_path)

            self.picture.set_visible(True)
        # Display media player only if there is sound.
        if card.is_aural():
            sound_path = os.path.join(MEDIA_DIR, self.card.sound.name)
            self.player = MediaPlayer(sound_path)
            self.player_box.append(self.player)
            self.player_box.set_visible(True)
        # Display text only if there is any.
        if card.is_textual():
            markup = "<span size='xx-large'>" + card.text + "</span>"
            self.text_label.set_markup(markup)
            self.text_label.set_visible(True)
        # Display additional text, if any.
        if card.text_b:
            markup = "<span size='xx-large'>" + card.text_b + "</span>"
            self.text_b_label.set_markup(markup)
            self.text_b_label.set_visible(True)

    def show_next_card(self):
        """Show the next card to study, if any."""
        pending_reviews = self.study_session.reviews.filter(
            answer__isnull=True
        )

        if pending_reviews.exists():
            next_review = self.study_session.next_review()
            next_card = next_review.aspect.card
            self.window.show_view(
                "card-front",
                [self.window, next_card]
            )
        else:
            self.window.end_study_session()
