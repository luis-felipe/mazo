"""GIF display component."""

import gi

gi.require_version("Gdk", "4.0")
gi.require_version("GdkPixbuf", "2.0")

from gi.repository import Gdk, GdkPixbuf, GLib, GObject


# COMPONENTS
# ==========

class GifPaintable(GObject.Object, Gdk.Paintable):
    """Represents a GIF or animated GIF object.

    You can use it with any widget that accepts paintables, like
    Gtk.Picture.

    Initialization parameters:

    PATH (string)
      Path to the GIF file.

    This class is a translation of GTK 4's pixbufpaintable.c, which came
    with the Images demo application (see gtk4-demo program)."""
    def __init__(self, path):
        super().__init__()
        self.animation = GdkPixbuf.PixbufAnimation.new_from_file(path)
        self.iterator = self.animation.get_iter()
        self.delay = self.iterator.get_delay_time()
        self.timeout = GLib.timeout_add(self.delay, self.do_delay)

        self.invalidate_contents()

    def do_delay(self):
        delay = self.iterator.get_delay_time()
        self.timeout = GLib.timeout_add(delay, self.do_delay)
        self.invalidate_contents()

        return GLib.SOURCE_REMOVE

    def do_get_intrinsic_height(self):
        return self.animation.get_height()

    def do_get_intrinsic_width(self):
        return self.animation.get_width()

    def do_snapshot(self, snapshot, width, height):
        time = GLib.TimeVal()
        time.tv_usec = GLib.get_real_time()
        self.iterator.advance(time)
        pixbuf = self.iterator.get_pixbuf()
        texture = Gdk.Texture.new_for_pixbuf(pixbuf)
        texture.snapshot(snapshot, width, height)

    def invalidate_contents(self):
        self.emit("invalidate-contents")
