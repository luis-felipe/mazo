"""Card creation view."""

import os

from mazo.forms import CardForm
from mazo.views.card_form import CardFormView


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# VIEWS
# =====

class NewCardFormView(CardFormView):
    """Represents a graphical form to create a new card."""
    def __init__(self, window, deck):
        super().__init__(window, deck)
        self.title = "Adding card to {}".format(deck.name)

        # Connect signals to handlers.
        self.save_and_add_another_button.connect(
            "clicked",
            self.on_save_and_add_another_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        super().disconnect_signal_handlers()
        self.save_and_add_another_button.disconnect_by_func(
            self.on_save_and_add_another_button_clicked
        )

    def on_cancel_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.window.show_view(
            "deck-detail",
            [self.window, self.deck]
        )

    def on_save_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        # Create new card and show deck detail view.
        self.create()
        self.window.show_view(
            "deck-detail",
            [self.window, self.deck]
        )

    def on_save_and_add_another_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        # Create new card and display the form to add another one.
        self.create()
        self.window.show_view(
            "card-creation",
            [self.window, self.deck]
        )

    def create(self):
        """Create a new card using the data in the form."""
        data = self.get_data()
        files = self.get_files()
        metaform = CardForm(data, files)

        if metaform.is_valid():
            metaform.save()
        else:
            for field, error in metaform.errors.as_data().items():
                print(f"{field}: {error}")
