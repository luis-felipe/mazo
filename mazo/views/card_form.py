"""Base form for cards."""

import os

from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils import timezone

from mazo.views.form_field_image import ImageField
from mazo.views.form_field_longtext import LongTextField
from mazo.views.form_field_sound import SoundField

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# VIEWS
# =====

class CardFormView(Gtk.ScrolledWindow):
    """Base class for forms to create or edit cards."""
    def __init__(self, window, deck, card=None):
        super().__init__()

        # Set properties.
        self.id = timezone.now().strftime("%H%S")
        self.name = "card-form-view-{}".format(self.id)
        self.actions = None
        self.window = window
        self.deck = deck
        self.card = card
        self.filled_fields = {
            "image-field": False,
            "sound-field": False,
            "text-field-a": False,
            "text-field-b": False
        }
        self.min_fields_required = 2  # to enable save buttons.

        # Set viewport and box of widgets.
        margin = 24
        self.viewport = Gtk.Viewport()
        self.viewport.set_margin_top(margin)
        self.viewport.set_margin_bottom(margin)
        self.viewport.set_margin_start(margin)
        self.viewport.set_margin_end(margin)
        self.box = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=12
        )
        self.viewport.set_child(self.box)

        # Set image and sound fields.
        self.image_field = ImageField(self)
        self.sound_field = SoundField(self)

        self.box.append(self.image_field)
        self.box.append(self.sound_field)

        # Set text fields.
        self.text_field_a = LongTextField(self, "text-field-a")
        self.text_field_a.set_vexpand(True)
        self.text_field_b = LongTextField(self, "text-field-b")

        self.box.append(self.text_field_a)
        self.box.append(self.text_field_b)

        # Set buttons.
        self.save_button = Gtk.Button()
        self.save_and_add_another_button = Gtk.Button()
        self.cancel_button = Gtk.Button()

        self.save_button.set_label("Save")
        self.save_and_add_another_button.set_label("Save and add another")
        self.cancel_button.set_label("Cancel")

        self.save_button.set_sensitive(False)
        self.save_and_add_another_button.set_sensitive(False)

        self.box.append(self.save_and_add_another_button)
        self.box.append(self.save_button)
        self.box.append(self.cancel_button)

        # Connect signals to handlers.
        self.save_button.connect("clicked", self.on_save_button_clicked)
        self.cancel_button.connect("clicked", self.on_cancel_button_clicked)

        # Connect custom signals to handlers.
        self.connect("field-filled", self.on_field_filled)
        self.connect("field-reset", self.on_field_reset)

        # Show all widgets.
        self.set_child(self.viewport)

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.save_button.disconnect_by_func(
            self.on_save_button_clicked
        )
        self.cancel_button.disconnect_by_func(
            self.on_cancel_button_clicked
        )
        self.disconnect_by_func(
            self.on_field_filled
        )
        self.disconnect_by_func(
            self.on_field_reset
        )
        self.image_field.disconnect_signal_handlers()
        self.sound_field.disconnect_signal_handlers()
        self.text_field_a.disconnect_signal_handlers()
        self.text_field_b.disconnect_signal_handlers()

    def on_field_filled(self, form, field_id, value):
        """Handle the FORM's field-filled signal."""
        self.filled_fields[field_id] = True
        self.enable_save_buttons()

        if (
                (field_id == "image-field" or field_id == "sound-field") and
                value is not None
        ):
            path = value
            self.window.last_dir_visited = os.path.dirname(path)

    def on_field_reset(self, form, field_id, *args):
        """Handle the FORM's field-reset signal."""
        self.filled_fields[field_id] = False
        self.enable_save_buttons()

    def get_data(self):
        """Get the data from the form's non-file fields."""
        data = {
            "deck": self.deck,
            "text": self.text_field_a.get_text(),
            "text_b": self.text_field_b.get_text()
        }

        return data

    def get_files(self):
        """Get files from the form's file fields."""
        image_path = self.image_field.get_file_path()
        image_basename = os.path.basename(image_path)
        sound_path = self.sound_field.get_file_path()
        sound_basename = os.path.basename(sound_path)
        files = {}

        # Add image file, if any.
        if os.path.exists(image_path):
            with open(image_path, "rb") as image_file:
                image_data = image_file.read()
                files["image"] = SimpleUploadedFile(image_basename, image_data)
        else:
            files["image"] = None
        # Add sound file, if any.
        if os.path.exists(sound_path):
            with open(sound_path, "rb") as sound_file:
                sound_data = sound_file.read()
                files["sound"] = SimpleUploadedFile(sound_basename, sound_data)
        else:
            files["sound"] = None

        return files

    def enable_save_buttons(self):
        """Enable save buttons if the form's enough_fields method
        returns True. Otherwise, disable the buttons."""
        if self.enough_fields():
            self.save_button.set_sensitive(True)
            self.save_and_add_another_button.set_sensitive(True)
        else:
            self.save_button.set_sensitive(False)
            self.save_and_add_another_button.set_sensitive(False)

    def enough_fields(self):
        """Return True if self.filled_fields contains at least
        self.min_fields_required."""
        filled_fields_count = 0

        for field, is_filled in self.filled_fields.items():
            if is_filled:
                filled_fields_count += 1

        return filled_fields_count >= self.min_fields_required
