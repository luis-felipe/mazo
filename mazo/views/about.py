"""About (the app) view."""

import os

from mazo.constants import APP_VERSION

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# VIEWS
# =====

@Gtk.Template(filename=os.path.join(UI_PATH, "about-dialog.ui"))
class AboutDialog(Gtk.AboutDialog):
    """Represents the About dialog of the application."""
    __gtype_name__ = "AboutDialog"

    def __init__(self):
        super().__init__()
        self.set_version(APP_VERSION)
