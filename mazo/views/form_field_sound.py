"""Sound field for forms."""

import os

from mazo.views.file_chooser import FileChooserDialog

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# COMPONENTS
# ==========

@Gtk.Template(filename=os.path.join(UI_PATH, "sound-field.ui"))
class SoundField(Gtk.Box):
    """Represents a form field to select an audio file.

    FORM (CardFormView)
      The form the field belongs to."""
    __gtype_name__ = "SoundField"

    sound_icon = Gtk.Template.Child("sound-icon")
    file_path = Gtk.Template.Child("file-path")
    change_button = Gtk.Template.Child("change-button")
    delete_button = Gtk.Template.Child("delete-button")

    def __init__(self, form):
        super().__init__()
        self.form = form

        # XXX: Connect signals to handlers.
        self.change_button.connect(
            "clicked",
            self.on_change_button_clicked
        )
        self.delete_button.connect(
            "clicked",
            self.on_delete_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.change_button.disconnect_by_func(
            self.on_change_button_clicked
        )
        self.delete_button.disconnect_by_func(
            self.on_delete_button_clicked
        )

    # @Gtk.Template.Callback()
    def on_delete_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.remove_sound()

    # @Gtk.Template.Callback()
    def on_change_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        # Show sound chooser dialog.
        last_dir = self.form.window.last_dir_visited
        dialog = FileChooserDialog(
            self.form.window,
            self.on_sound_selected,
            directory=last_dir,
            filter_kind="sounds"
        )
        dialog.show()

    def on_sound_selected(self, path):
        """Handle card sound selection in file chooser dialog."""
        if path:
            self.set_file_path(path)
            self.form.window.last_dir_visited = os.path.dirname(path)

    def get_file_path(self):
        """Return a string representing the path to the selected file,
        if any. Otherwise, return the placeholder path."""
        return self.file_path.get_text()

    def remove_sound(self):
        """Remove image, if any."""
        # Reset file path to default.
        self.set_file_path("No audio file selected")

        # Update sound icon.
        self.set_icon("audio-volume-muted-symbolic")

        # Hide sound deletion button.
        self.delete_button.set_visible(False)

        # Signal field change.
        self.form.emit("field-reset", "sound-field")

    def set_file_path(self, path):
        """Set the field's path to the given path.

        PATH (string)
          Absolute path to a sound file."""
        self.file_path.set_text(path)

        # Update sound icon.
        self.set_icon("audio-speakers-symbolic")

        # Display sound deletion button.
        self.delete_button.set_visible(True)

        # Signal field change.
        self.form.emit("field-filled", "sound-field", path)

    def set_icon(self, name):
        """Set the icon by NAME."""
        self.sound_icon.set_from_icon_name(name)
