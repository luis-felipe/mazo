"""End of study view."""

import os

from mazo.views.progress_gauge import ProgressGauge

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# VIEWS
# =====

@Gtk.Template(filename=os.path.join(UI_PATH, "end-of-study-view.ui"))
class EndOfStudyView(Gtk.ScrolledWindow):
    """Represents a view that is shown when a study session is over."""
    __gtype_name__ = "EndOfStudyView"

    left_gauge_box = Gtk.Template.Child("left-gauge-box")
    right_gauge_box = Gtk.Template.Child("right-gauge-box")
    message = Gtk.Template.Child("message")
    ok_button = Gtk.Template.Child("ok-button")

    def __init__(self, window, deck):
        super().__init__()
        self.name = "end-of-study-view"
        self.actions = None
        self.window = window
        self.deck = deck
        self.previous_progress = deck.get_previous_progress()
        self.current_progress = deck.get_progress()
        self.set_gauges()
        self.set_message()

        # XXX: Connect signals to handlers.
        self.ok_button.connect(
            "clicked",
            self.on_ok_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.ok_button.disconnect_by_func(
            self.on_ok_button_clicked
        )

    # @Gtk.Template.Callback()
    def on_ok_button_clicked(self, button):
        """Handle the BUTTON's clicked signal."""
        self.window.show_view("deck-detail", [self.window, self.deck])

    def set_gauges(self):
        """Set previous and current progress indicators."""
        left_gauge = ProgressGauge(outer_fraction=self.current_progress)
        right_gauge = ProgressGauge(outer_fraction=self.previous_progress)

        self.left_gauge_box.append(left_gauge)
        self.right_gauge_box.append(right_gauge)

    def set_message(self):
        """Set a message appropriate for the progress achieved."""
        if self.current_progress < self.previous_progress:
            self.message.set_text(
                "That's fine, you'll do better next time."
            )
        if self.current_progress == self.previous_progress:
            self.message.set_text(
                "Well, it happens some times..."
            )
        if self.current_progress > self.previous_progress:
            self.message.set_text(
                "Well done! You're making progress!"
            )
        if self.current_progress == 1.0:
            self.message.set_text(
                "Congratulations! You've mastered this deck!"
            )
