"""Data restore component."""

import os

from django.utils import timezone

from mazo.ie import restore_data
from mazo.views.file_chooser import FileChooserDialog
from mazo.views.header_actions_leave import LeaveHeaderAction
from mazo.views.workers import AsyncWorker

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# VIEWS
# =====

@Gtk.Template(filename=os.path.join(UI_PATH, "data-restore-view.ui"))
class DataRestoreView(Gtk.ScrolledWindow):
    __gtype_name__ = "DataRestoreView"

    error_box = Gtk.Template.Child("error-box")
    error_buffer = Gtk.Template.Child("error-buffer")
    file_path_label = Gtk.Template.Child("file-path-label")
    restore_button = Gtk.Template.Child("restore-button")
    leave_button = Gtk.Template.Child("leave-button")
    progress_box = Gtk.Template.Child("progress-box")
    progress_spinner = Gtk.Template.Child("progress-spinner")
    quit_button = Gtk.Template.Child("quit-button")
    select_file_button = Gtk.Template.Child("select-file-button")
    success_box = Gtk.Template.Child("success-box")

    def __init__(self, window):
        super().__init__()
        self.id = timezone.now().strftime("%H%S")
        self.name = "data-restore-view-{}".format(self.id)
        self.title = "Restoring data"
        self.window = window
        self.actions = LeaveHeaderAction(
            window,
            "deck-list",
            [window]
        )
        self.archive_path = ""

        # XXX: Connect signals to handlers.
        self.select_file_button.connect(
            "clicked",
            self.on_select_file_button_clicked
        )
        self.restore_button.connect(
            "clicked",
            self.on_restore_button_clicked
        )
        self.quit_button.connect(
            "clicked",
            self.on_quit_button_clicked
        )
        self.leave_button.connect(
            "clicked",
            self.on_leave_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.select_file_button.disconnect_by_func(
            self.on_select_file_button_clicked
        )
        self.restore_button.disconnect_by_func(
            self.on_restore_button_clicked
        )
        self.quit_button.disconnect_by_func(
            self.on_quit_button_clicked
        )
        self.leave_button.disconnect_by_func(
            self.on_leave_button_clicked
        )
        self.actions.disconnect_signal_handlers()

    # @Gtk.Template.Callback()
    def on_select_file_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        last_dir = self.window.last_dir_visited
        dialog = FileChooserDialog(
            self.window,
            self.on_file_selected,
            directory=last_dir,
            filter_kind="archives"
        )
        dialog.show()

    def on_file_selected(self, path):
        """Handle archive selection in file chooser dialog."""
        if path:
            self.archive_path = path
            self.file_path_label.set_text(path)
            self.restore_button.set_sensitive(True)
            self.window.last_dir_visited = os.path.dirname(path)

    # @Gtk.Template.Callback()
    def on_restore_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        button.set_visible(False)
        self.select_file_button.set_sensitive(False)
        self.progress_spinner.start()
        self.progress_box.set_visible(True)

        # Restore data.
        async_worker = AsyncWorker(
            operation=restore_data,
            operation_inputs=(self.archive_path,),
            operation_callback=self.on_restore_finished
        )
        async_worker.start()

    def on_restore_finished(self, worker, result, handler_data):
        """Handle the RESULT of the asynchronous operation performed by
        WORKER.

        WORKER (mazo.views.AsyncWorker)
          The worker responsible for restoring user data
          asynchronously.

        RESULT (Gio.AsyncResult)
          The asynchronous result of the asynchronous operation.

        HANDLER_DATA (None)
          Additional data passed to this handler by the worker when the
          job is done. It should be None in this case.

        """
        # Check result's value.
        outcome = worker.return_value(result)

        # Update user interface.
        self.progress_spinner.stop()
        self.progress_box.set_visible(False)
        self.leave_button.set_visible(True)

        if isinstance(outcome, str) and os.path.exists(outcome):
            self.success_box.set_visible(True)
        else:
            error_report = ""

            for error, description in outcome.items():
                error_report += f"{error}: {description}\n"

            self.error_buffer.set_text(error_report)
            self.error_box.set_visible(True)

    # @Gtk.Template.Callback()
    def on_quit_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.window.get_application().quit()

    # @Gtk.Template.Callback()
    def on_leave_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.window.show_view("deck-list", [self.window])
