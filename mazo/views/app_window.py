"""Application window."""

import os

from mazo.views.card_back import CardBackView
from mazo.views.card_browser import CardBrowserView
from mazo.views.card_creation import NewCardFormView
from mazo.views.card_edition import EditCardFormView
from mazo.views.card_front import CardFrontView
from mazo.views.data_backup import DataBackUpView
from mazo.views.data_restore import DataRestoreView
from mazo.views.deck_credits import DeckCreditsView
from mazo.views.deck_detail import DeckDetailView
from mazo.views.deck_export import DeckExportView
from mazo.views.deck_form import DeckFormView
from mazo.views.deck_import import DeckImportView
from mazo.views.deck_list import DeckListView
from mazo.views.deck_store import DeckStoreView
from mazo.views.end_of_study import EndOfStudyView
from mazo.views.preferences import PreferencesView

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")
VIEWER_TRANSITION = Gtk.StackTransitionType.SLIDE_LEFT  # Default.
VIEWS = {
    "card-back": CardBackView,
    "card-browser": CardBrowserView,
    "card-creation": NewCardFormView,
    "card-edition": EditCardFormView,
    "card-front": CardFrontView,
    "data-backup": DataBackUpView,
    "data-restore": DataRestoreView,
    "deck-credits": DeckCreditsView,
    "deck-detail": DeckDetailView,
    "deck-export": DeckExportView,
    "deck-form": DeckFormView,
    "deck-import": DeckImportView,
    "deck-list": DeckListView,
    "deck-store": DeckStoreView,
    "end-of-study": EndOfStudyView,
    "preferences": PreferencesView
}


# VIEWS
# =====

@Gtk.Template(filename=os.path.join(UI_PATH, "app-window.ui"))
class AppWindow(Gtk.ApplicationWindow):
    """Objects of this class represent application windows."""
    __gtype_name__ = "AppWindow"

    actions_box = Gtk.Template.Child("actions-box")
    header_bar = Gtk.Template.Child("header-bar")
    menu_button = Gtk.Template.Child("primary-menu-button")
    title_label = Gtk.Template.Child("title-label")
    view_stack = Gtk.Template.Child("view-stack")

    def __init__(self, app):
        """Initialize the window and set APP, a Gtk.Application, as its
        manager."""
        super().__init__()
        self.menus = Gtk.Builder.new_from_file(
            os.path.join(UI_PATH, "menus.ui")
        )
        self.study_session = None  # See mazo.models.DeckStudySession
        self.last_dir_visited = ""  # When selecting media files

        # Set primary menu
        popover = Gtk.PopoverMenu.new_from_model(
            self.menus.get_object("primary-menu-model")
        )
        self.menu_button.set_popover(popover)

        # Show deck list view.
        self.show_view("deck-list", [self])

    @Gtk.Template.Callback()
    def on_deck_list_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        self.show_view("deck-list", [self])

    def end_study_session(self):
        """End any active study session."""
        deck = self.study_session.round.deck

        # Finish the study session.
        self.study_session.finish()

        # Clean current session field.
        self.study_session = None

        # Display results of the finished session.
        self.show_view("end-of-study", [self, deck])

    def set_view_actions(self, actions):
        """Display ACTIONS (a Gtk.Widget or None) in the actions box of
        the header bar."""
        # Remove previous actions (if any).
        old_actions = self.actions_box.get_first_child()

        if old_actions:
            self.actions_box.remove(old_actions)

        # Add new actions.
        if actions:
            self.actions_box.append(actions)

    def show_view(
            self,
            view_id,
            inputs=[],
            kinputs={},
            transition=VIEWER_TRANSITION
    ):
        """Display the view corresponding to the given ID and
        initialize it with the given inputs."""
        # Get old view.
        old_view = self.view_stack.get_visible_child()

        # Add new view. TODO: Print warning if the ID does not exist.
        view = VIEWS[view_id](*inputs, **kinputs)
        self.view_stack.add_named(view, view.name)
        self.set_view_actions(view.actions)

        if hasattr(view, "title"):
            self.title_label.set_text(view.title)

        # Show new view.
        self.view_stack.set_transition_type(transition)
        self.view_stack.set_visible_child_name(view.name)

        # Remove old view (if any).
        if old_view:
            # FIXME: Memory leak related to changing views.
            #
            # In GTK 3, removing the old view from the self.view_stack
            # (Gtk.Stack) using its superclass method
            # Gtk.Container.remove didn't destroy the old view,
            # so RAM usage grew and grew as you changed views, and RAM
            # was soon exhausted when reviewing decks with hundreds of
            # cards. RAM was only freed when the application was closed.
            #
            # To avoid this, the old view was destroyed directly using
            # Gtk.Widget.destroy instead of Gtk.Container.remove.
            # Destroying the old view manually helped reduce memory
            # usage 5 times, but didn't seem to fix the issue
            # completely: RAM still grew gradually when switching
            # views, and that RAM was not freed while the app was
            # running.
            #
            # I wondered that maybe Gtk.Stack was keeping copies of
            # previous objects around to use in page transitions,
            # but setting transitions to
            # Gtk.StackTransitionType.NONE didn't make much of a
            # difference.
            #
            # In GTK 4, however, GtkWidget.destroy was removed, so
            # we are now using the new GtkStack.remove method and
            # have reported an issue to track whether the RAM issue is
            # the same, worst or gone for good.
            #
            # XXX: The following is a work around memory leaks that
            # result from circular references to the old view which
            # prevent it from being garbage collected.
            # (https://discourse.gnome.org/t/22651)
            old_view.disconnect_signal_handlers()
            self.view_stack.remove(old_view)
