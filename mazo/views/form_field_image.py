"""Image field for forms."""

import os

from mazo.views.file_chooser import FileChooserDialog

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# COMPONENTS
# ==========

@Gtk.Template(filename=os.path.join(UI_PATH, "image-field.ui"))
class ImageField(Gtk.Box):
    """Represents a form field to select an image file.

    FORM (CardFormView)
      The form the field belongs to."""
    __gtype_name__ = "ImageField"

    picture = Gtk.Template.Child("picture")
    file_path = Gtk.Template.Child("file-path")
    change_button = Gtk.Template.Child("change-button")
    delete_button = Gtk.Template.Child("delete-button")

    def __init__(self, form):
        super().__init__()
        self.form = form

        # XXX: Connect signals to handlers.
        self.change_button.connect(
            "clicked",
            self.on_change_button_clicked
        )
        self.delete_button.connect(
            "clicked",
            self.on_change_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.change_button.disconnect_by_func(
            self.on_change_button_clicked
        )
        self.delete_button.disconnect_by_func(
            self.on_change_button_clicked
        )

    # @Gtk.Template.Callback()
    def on_delete_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        # Empty image box.
        self.remove_image()

        # Signal field change.
        self.form.emit("field-reset", "image-field")

    # @Gtk.Template.Callback()
    def on_change_button_clicked(self, button):
        """Handle this BUTTON's clicked signal."""
        # Show image chooser dialog.
        last_dir = self.form.window.last_dir_visited
        dialog = FileChooserDialog(
            self.form.window,
            self.on_image_selected,
            directory=last_dir,
            filter_kind="images"
        )
        dialog.show()

    def on_image_selected(self, path):
        """Handle card image selection in file chooser dialog."""
        if path:
            self.set_image(path)
            self.form.window.last_dir_visited = os.path.dirname(path)

    def get_file_path(self):
        """Return a string representing the path to the selected file,
        if any. Otherwise, return the placeholder path defined in the
        XML template."""
        return self.file_path.get_text()

    def remove_image(self):
        """Remove image, if any."""
        self.picture.set_filename("")
        self.set_file_path("No image file selected")

        # Hide image deletion button.
        self.delete_button.set_visible(False)

    def set_file_path(self, path):
        """Set the field's path to the given path.

        PATH (string)
          Absolute path to an image file."""
        self.file_path.set_text(path)

    def set_image(self, path):
        """Set the image to the image in the given path."""
        # Set image.
        self.picture.set_filename(path)
        self.set_file_path(path)

        # Display image deletion button.
        self.delete_button.set_visible(True)

        # Signal field change.
        self.form.emit("field-filled", "image-field", path)
