"""Leave action for the header component."""

import os

import gi

gi.require_version("Gtk", "4.0")

from gi.repository import Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# COMPONENTS
# ==========

@Gtk.Template(filename=os.path.join(UI_PATH, "leave-header-action.ui"))
class LeaveHeaderAction(Gtk.Box):
    """Represents a single action button to leave the current view.

    Initialization parameters:

    WINDOW (AppWindow)
      The window that will display the given VIEW.

    VIEW (Gtk.Widget)
      For example, a DeckDetailView, DeckListView, etc.

    """
    __gtype_name__ = "LeaveHeaderAction"

    leave_button = Gtk.Template.Child("leave-button")

    def __init__(self, window, view, inputs=[], kinputs={}):
        super().__init__()
        self.window = window
        self.view = view
        self.inputs = inputs
        self.kinputs = kinputs

        # XXX: Connect signals to handlers.
        self.leave_button.connect(
            "clicked",
            self.on_leave_button_clicked
        )

    # XXX: Disconnect signal handlers to ensure garbage collection of self.
    def disconnect_signal_handlers(self):
        self.leave_button.disconnect_by_func(
            self.on_leave_button_clicked
        )

    # @Gtk.Template.Callback()
    def on_leave_button_clicked(self, button):
        """Handle the BUTTON's clicked signal."""
        self.window.show_view(
            self.view,
            self.inputs,
            self.kinputs
        )
