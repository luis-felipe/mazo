"""Application management mechanisms."""

import os

from mazo.constants import APP_ID
from mazo.views.about import AboutDialog
from mazo.views.app_window import AppWindow
from mazo.views.card_form import CardFormView
from mazo.views.shortcuts import ShortcutsWindow

import gi

gi.require_version("Gdk", "4.0")
gi.require_version("GdkPixbuf", "2.0")
gi.require_version("Gtk", "4.0")

from gi.repository import Gdk, Gio, GObject, Gtk


# CONSTANTS
# =========

UI_PATH = os.path.join(os.path.dirname(__file__), "ui")


# VIEWS
# =====

class App(Gtk.Application):
    """Represents the singleton application object."""
    def __init__(self):
        Gtk.Application.__init__(
            self,
            application_id=APP_ID,
            flags=Gio.ApplicationFlags.FLAGS_NONE
        )

        # Define custom signals.
        #
        # XXX: Find or design GTK forms with automatic validation.
        # Most of these signals are currently used to validate forms
        # (see CardFormView), but all forms functionality should
        # be abstracted to clean up current forms.
        #
        # Also, is there a way to define all signals in the classes of
        # objects that emit them? Last time I tried, signals where not
        # found.
        GObject.signal_new(
            "field-filled",
            CardFormView,
            GObject.SignalFlags.RUN_LAST,
            GObject.TYPE_PYOBJECT,
            [
                GObject.TYPE_PYOBJECT,  # Field ID (string)
                GObject.TYPE_PYOBJECT   # Field value
            ]
        )
        GObject.signal_new(
            "field-reset",
            CardFormView,
            GObject.SignalFlags.RUN_LAST,
            GObject.TYPE_PYOBJECT,
            [GObject.TYPE_PYOBJECT]  # Field ID (string)
        )

    def do_startup(self):
        Gtk.Application.do_startup(self)

        # Set application actions
        action = Gio.SimpleAction.new("about", None)
        action.connect("activate", self.on_about)
        self.add_action(action)

        action = Gio.SimpleAction.new("back-up", None)
        action.connect("activate", self.on_back_up)
        self.add_action(action)

        action = Gio.SimpleAction.new("restore-data", None)
        action.connect("activate", self.on_restore_data)
        self.add_action(action)

        action = Gio.SimpleAction.new("help", None)
        action.connect("activate", self.on_help)
        self.add_action(action)
        self.set_accels_for_action("app.help", ["F1"])

        # TODO: Activate when actual preferences get added.
        # action = Gio.SimpleAction.new("preferences", None)
        # action.connect("activate", self.on_preferences)
        # self.add_action(action)

        action = Gio.SimpleAction.new("shortcuts", None)
        action.connect("activate", self.on_shortcuts)
        self.add_action(action)

        action = Gio.SimpleAction.new("quit", None)
        action.connect("activate", self.on_quit)
        self.add_action(action)
        self.set_accels_for_action("app.quit", ["<Ctrl>Q"])

    def do_activate(self):
        # Show default application window.
        window = AppWindow(self)
        self.add_window(window)
        window.present()

    def on_about(self, action, param):
        """Show application About dialog."""
        dialog = AboutDialog()
        dialog.show()

    def on_back_up(self, action, param):
        """Show data back up view."""
        window = self.get_active_window()
        window.show_view("data-backup", [window])

    def on_restore_data(self, action, param):
        """Show data restoration view."""
        window = self.get_active_window()
        window.show_view("data-restore", [window])

    def on_help(self, action, param):
        """Show application manual in Yelp."""
        window = self.get_active_window()
        Gtk.show_uri(window, "help:mazo", Gdk.CURRENT_TIME)

    def on_preferences(self, action, param):
        """Show application preferences dialog."""
        window = self.get_active_window()
        window.show_view("preferences", [window])

    def on_quit(self, action, param):
        """Show Application preferences dialog."""
        self.quit()

    def on_shortcuts(self, action, param):
        """Show gestures and shortcuts window."""
        ShortcutsWindow(self)
