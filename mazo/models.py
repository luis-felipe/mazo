import os
import random

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils import timezone

from mazo.constants import ASPECT_POINTS_FOR_MEMORIZATION


# CONSTANTS
# =========

ASPECT_KINDS = [
    ("VISUAL", "Visual"),
    ("AURAL", "Aural"),
    ("TEXTUAL", "Textual"),
    ("ORAL", "Oral"),
    ("WRITTEN", "Written")
]

ANSWER_KINDS = [
    ("CORRECT", "Correct"),
    ("INCORRECT", "Incorrect"),
    ("MEMORIZED", "Memorized")
]


# HELPER FUNCTIONS
# ================

def icon_path(instance, filename):
    """Return upload path for the file represented by FILENAME."""
    return upload_path("icons", filename)


def image_path(instance, filename):
    """Return upload path for the file represented by FILENAME."""
    return upload_path("images", filename)


def sound_path(instance, filename):
    """Return upload path for the file represented by FILENAME."""
    return upload_path("sounds", filename)


def upload_path(directory, filename):
    """Return a path in the form MEDIA_DIR/DIRECTORY/FILENAME_BASENAME."""
    base_name = os.path.basename(filename)
    return "{}/{}".format(directory, base_name)


# MODELS: Subjects of study
# =========================

class Deck(models.Model):
    """Represents a collection of cards."""
    name = models.CharField(max_length=100)
    description = models.TextField(
        max_length=250,
        blank=True
    )
    credits = models.TextField(
        blank=True
    )
    icon = models.ImageField(
        upload_to=icon_path,
        blank=True,
        null=True,
        help_text="A 200x200 pixels image (PNG, JPEG)."
    )
    round_count = models.PositiveIntegerField(default=0)
    creation_date = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name

    def count_learned_cards(self):
        """Return the number of cards that have been learned so far."""
        count = 0

        for card in self.cards.all():
            if card.is_learned():
                count += 1

        return count

    def get_partial_progress(self):
        """Return a fraction (0.0 to 1.0) indicating the percentage of
        aspects in the deck that are considered memorized."""
        aspects = Aspect.objects.filter(card__deck=self)
        progress = 0.0
        total_points = aspects.count() * ASPECT_POINTS_FOR_MEMORIZATION

        if aspects.count() > 0:
            current_points = aspects.aggregate(
                models.Sum("retention_points")
            )["retention_points__sum"]
            progress = current_points/total_points

        return progress

    def get_previous_progress(self):
        """Return a fraction (0.0 to 1.0) indicating the memorization
        progress achieved in the penultimate of the already finished
        study sessions (if any)."""
        progress = 0.0
        current_round = self.rounds.last()  # Assume round 1 to N sorting.
        finished_study_sessions = current_round.sessions.filter(
            finish_date__isnull=False
        ).order_by("-finish_date")  # Reverse chronologically.

        if finished_study_sessions.count() >= 2:
            penultimate_session = finished_study_sessions[1]
            progress = penultimate_session.progress

        return progress

    def get_progress(self):
        """Return a fraction (0.0 to 1.0) indicating the percentage of
        aspects that are considered memorized and marked as learned by
        the user."""
        aspects = Aspect.objects.filter(card__deck=self)
        total_aspects = aspects.count()
        progress = 0

        if total_aspects > 0:
            # Get retention points of all aspects.
            total_points = total_aspects * (ASPECT_POINTS_FOR_MEMORIZATION + 1)
            sum_of_aspects_points = aspects.aggregate(
                models.Sum("retention_points")
            )["retention_points__sum"]
            num_aspects_learned = aspects.filter(is_learned=True).count()
            current_points = sum_of_aspects_points + num_aspects_learned

            progress = current_points / total_points

        return progress

    def has_aural_cards(self):
        """Return True if the deck has cards with sound."""
        cards = self.cards.all()
        aural_cards = list(filter(Card.is_aural, cards))

        return len(aural_cards) > 0

    def has_textual_cards(self):
        """Return True if the deck has cards with text."""
        cards = self.cards.all()
        textual_cards = list(filter(Card.is_textual, cards))

        return len(textual_cards) > 0

    def has_visual_cards(self):
        """Return True if the deck has cards with image."""
        cards = self.cards.all()
        visual_cards = list(filter(Card.is_visual, cards))

        return len(visual_cards) > 0

    def increase_round_count(self):
        """Increase deck's round count by one.

        This method is called automatically when a new Round is
        created (see mazo.signals.on_round_created). There's is no
        need to call this method manually."""
        self.round_count += 1
        self.save()

    def is_aspect_learned(self, aspect_kind):
        """Return true if the given aspect kind has been learned in all
        cards in the deck."""
        # XXX: Use a bool_and aggregate function instead if/when Django
        # supports it.
        learned_list = []  # List of booleans
        aspects = Aspect.objects.filter(card__deck=self, kind=aspect_kind)

        for aspect in aspects:
            learned_list.append(aspect.is_learned)

        return aspects.count() > 0 and all(learned_list)

    def start_round(self):
        """Start a new study round if there isn't an ongoing round
        already. Otherwise, return False."""
        # Return False if there is any previous round unfinished.
        if self.rounds.filter(finish_date__isnull=True).count() > 0:
            return False

        # Otherwise, start a new round and return it.
        return Round.objects.create(deck=self)

    def start_session(self, kind):
        """Start a study session of the given KIND and return it.

        If there is any unfinished session, it will be closed before
        starting the new one."""
        current_round = self.rounds.order_by("number").last()
        ongoing_sessions = current_round.sessions.filter(
            kind=kind,
            finish_date__isnull=True
        )

        # Finish any unfinished session.
        if ongoing_sessions.exists():
            for study_session in ongoing_sessions:
                study_session.finish()

        # Start new session.
        session = StudySession.objects.create(
            round=current_round,
            kind=kind
        )

        return session

    def unlearn_cards(self):
        """Mark all aspects of all cards in the deck as unlearned so
        that the cards can be studied again in a new round."""
        for card in self.cards.all():
            for aspect in card.aspects.all():
                aspect.set_is_learned(False)


class Card(models.Model):
    """Represents a concept that can be learned. Cards are organized in
    decks."""
    deck = models.ForeignKey(
        Deck,
        related_name="cards",
        on_delete=models.CASCADE
    )
    image = models.ImageField(
        upload_to=image_path,
        blank=True,
        null=True
    )
    sound = models.FileField(
        upload_to=sound_path,
        blank=True,
        null=True
    )
    text = models.TextField(
        max_length=250,
        blank=True
    )
    text_b = models.TextField(
        max_length=250,
        blank=True
    )
    creation_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "image:{} | sound:{} | text:{}".format(
            self.image.name, self.sound.name, self.text
        )

    def create_aspects(self):
        """Create aspects according to the card's properties.

        So, if the card has an image, a visual aspect is added; if it
        has sound, an aural aspect is added; if it has text, a textual
        aspect is added.

        This method is called automatically when a card is created or
        modified (see signals.on_card_created and signals.on_card_changed).
        There is no need to call it directly."""
        if (
                self.is_aural() and not
                self.aspects.filter(kind="AURAL").exists()
        ):
            Aspect.objects.create(card=self, kind="AURAL")
        if (
                self.is_textual() and not
                self.aspects.filter(kind="TEXTUAL").exists()
        ):
            Aspect.objects.create(card=self, kind="TEXTUAL")
        if (
                self.is_visual() and not
                self.aspects.filter(kind="VISUAL").exists()
        ):
            Aspect.objects.create(card=self, kind="VISUAL")

    def is_aural(self):
        """Return True if the card has a non-empty sound."""
        if self.sound and self.sound.name != "":
            return True
        else:
            return False

    def is_textual(self):
        """Return True if the card has text."""
        return self.text != ""

    def is_visual(self):
        """Return True if the card has a non-empty image."""
        if self.image and self.image.name != "":
            return True
        else:
            return False

    def is_learned(self):
        """Return true if all aspects of the card have been learned."""
        answer = True

        for aspect in self.aspects.all():
            if not aspect.is_learned:
                answer = False
                break

        return answer

    def remove_aspects(self):
        """Remove aspects that are not present anymore.

        So, if the card has no image anymore, its visual aspect is
        removed; if it has no sound anymore, its aural aspect is
        removed; if it has no text anymore, its textual aspect is
        removed.

        This method is called automatically when a card is modified
        (see signals.on_card_changed). There is no need to call it
        directly."""
        visual_aspect_set = self.aspects.filter(kind="VISUAL")
        aural_aspect_set = self.aspects.filter(kind="AURAL")
        textual_aspect_set = self.aspects.filter(kind="TEXTUAL")

        if not self.is_visual() and visual_aspect_set.exists():
            visual_aspect = visual_aspect_set.first()
            visual_aspect.delete()
        if not self.is_aural() and aural_aspect_set.exists():
            aural_aspect = aural_aspect_set.first()
            aural_aspect.delete()
        if not self.is_textual() and textual_aspect_set.exists():
            textual_aspect = textual_aspect_set.first()
            textual_aspect.delete()


class Aspect(models.Model):
    """Represents a learnable part of a concept. It can be one of the
    following kinds:

    + AURAL
    + ORAL
    + TEXTUAL
    + VISUAL
    + WRITTEN

    For instance, in a deck of Spanish words, each word can be seen as a
    concept that can be heard (aural aspect), that can be read (textual
    aspect), that can be written (written aspect), that can be pronounced
    (oral aspect), and whose object it represents can be visualized
    (visual aspect)."""
    card = models.ForeignKey(
        Card,
        related_name="aspects",
        on_delete=models.CASCADE
    )
    kind = models.CharField(max_length=20, choices=ASPECT_KINDS)
    is_learned = models.BooleanField(default=False)
    retention_points = models.PositiveIntegerField(
        default=0,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(
                ASPECT_POINTS_FOR_MEMORIZATION,
                message="Max value can't be greater than mazo.constants.ASPECT_POINTS_FOR_MEMORIZATION."
            )
        ]
    )

    class Meta:
        ordering = ["kind"]

    def add_point(self):
        """Add one point to Aspect.retention_points if retention points
        are not at the maximum value (ASPECT_POINTS_FOR_MEMORIZATION)."""
        if self.retention_points < ASPECT_POINTS_FOR_MEMORIZATION:
            self.retention_points += 1
            self.save()

    def get_scheduled_review(self):
        """Return the latest review with no answer, if any; otherwise,
        return None."""
        unanswered_reviews = self.reviews.filter(answer__isnull=True)
        # Assume reviews are sorted in reverse chronological order.
        scheduled_review = unanswered_reviews.first()

        return scheduled_review

    def reset_points(self):
        """Set aspect retention points back to zero."""
        self.retention_points = 0
        self.save()

    def schedule_review(self, datetime):
        """Plan a new review on the given date and time and return the
        newly created review."""
        # Create the review.
        return Review.objects.create(
            aspect=self,
            date=datetime
        )

    def seems_memorized(self):
        """Return true if the aspect's retention points are greater or
        equal to mazo.constants.ASPECT_POINTS_FOR_MEMORIZATION."""
        return self.retention_points >= ASPECT_POINTS_FOR_MEMORIZATION

    def set_is_learned(self, state):
        """Set the aspect's is_learned to the given boolean state.

        If the deck memorization progress reaches 100% at this point,
        the aspect tells the current round to finish itself."""
        # Save aspect state.
        self.is_learned = state
        self.save()

        # Tell current round to finish itself if necessary.
        deck = self.card.deck
        rounds = deck.rounds.filter(deck=deck).order_by("number")
        current_round = rounds.last()

        if deck.get_progress() == 1.0:
            current_round.finish()


# MODELS: Study mechanisms
# ========================

class Round(models.Model):
    """Represents a period of time taken to memorize a whole deck.

    A Deck can have many rounds, a round can have as many study sessions
    as necessary to memorize the whole deck.

    Every Deck is assigned a first round automatically when it is
    created.

    The round number is automatically set at round creation time.
    See the signal mazo.signals.on_round_created."""
    deck = models.ForeignKey(
        Deck,
        related_name="rounds",
        on_delete=models.CASCADE
    )
    number = models.PositiveIntegerField(
        default=0,
        editable=False
    )
    progress = models.FloatField(
        default=0,
        help_text="Deck memorization progress (0.0 to 1.0)."
    )
    start_date = models.DateTimeField(auto_now_add=True)
    finish_date = models.DateTimeField(
        blank=True,
        null=True
    )

    class Meta:
        ordering = ["number"]

    def finish(self):
        """Set self.finish_date to the current datetime and return this
        datetime. Also, update round progress to 1.0 (100% memorized)."""
        if self.finish_date is None:
            self.finish_date = timezone.now()
            self.progress = 1.0
            self.save()

        return self.finish_date

    def get_scheduled_reviews(self, kind):
        """Return a set of reviews of the given KIND that have not been
        answered yet.

        KIND (string)
          One of the mazo.models.ASPECT_KINDS.

        RETURN VALUE (Django QuerySet)
          A query set of mazo.models.Review objects.

        """
        reviews = Review.objects.filter(
            aspect__card__deck=self.deck,
            aspect__kind=kind
        )
        scheduled_reviews = reviews.filter(answer__isnull=True)

        return scheduled_reviews

    def set_number(self):
        """Number itself according to Deck.round_count.

        This method is called automatically when a new round is created
        (see mazo.signals.on_round_created). There is no need to call it
        manually."""
        self.number = self.deck.round_count
        self.save()

    def update_progress(self):
        """Update self.progress to the value of Deck.get_progress.

        This method is called automatically when a review is answered
        (see mazo.signals.on_answer_created)."""
        self.progress = self.deck.get_progress()
        self.save()


class StudySession(models.Model):
    """Represents a session where the learner reviews a particular
    aspect of the cards in a deck.

    Study sessions are associated to a Round, not directly to a
    Deck."""
    round = models.ForeignKey(
        Round,
        related_name="sessions",
        on_delete=models.CASCADE
    )
    kind = models.CharField(
        max_length=20,
        choices=ASPECT_KINDS
    )
    progress = models.FloatField(
        default=0,
        help_text="Deck memorization progress at the end of the session."
    )
    start_date = models.DateTimeField(auto_now_add=True)
    finish_date = models.DateTimeField(
        blank=True,
        null=True
    )

    class Meta:
        ordering = ["start_date"]

    def claim_reviews(self):
        """Assign pending reviews to itself.

        This method is called automatically on session creation. See
        on_study_session_created in mazo.signals.

        """
        pending_reviews = self.round.get_scheduled_reviews(self.kind)
        self.reviews.add(*pending_reviews)

    def finish(self):
        """Set study session progress to the value of its related deck
        memorization progress, and finish_date to the current datetime.
        Return the latter."""
        if self.finish_date is None:
            # Set progress.
            deck_progress = self.round.deck.get_progress()
            self.progress = deck_progress
            # Set finish date.
            self.finish_date = timezone.now()
            self.save()

        return self.finish_date

    def next_review(self):
        """Return a random unanswered review from self.reviews.

        Raises IndexError if self.reviews is empty or has no unanswered
        reviews."""
        pending_reviews = self.reviews.filter(
            answer__isnull=True
        )

        if pending_reviews.exists():
            random_index = random.randint(0, pending_reviews.count() - 1)
            random_review = pending_reviews[random_index]
        else:
            raise IndexError("The study session has no pending reviews.")

        return random_review


class Review(models.Model):
    """Represents a scheduled revision of a card Aspect for a
    particular Round."""
    aspect = models.ForeignKey(
        Aspect,
        related_name="reviews",
        on_delete=models.CASCADE
    )
    study_session = models.ForeignKey(
        StudySession,
        related_name="reviews",
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    date = models.DateTimeField()

    class Meta:
        ordering = ["date"]

    def __str__(self):
        state = "PENDING"

        if self.is_complete():
            state = "DONE"

        return "{} {} {}".format(
            self.aspect.kind, self.date, state
        )

    def is_complete(self):
        """Return True if the review has an answer (self.answer)."""
        return hasattr(self, "answer")

    def is_today(self):
        """Return True if self.date is today."""
        today = timezone.now()

        return (
            self.date.year == today.year and
            self.date.month == today.month and
            self.date.day == today.day
        )

    def respond_correctly(self):
        """Record a correct answer for the review and return it."""
        return Answer.objects.create(
            review=self,
            kind="CORRECT"
        )

    def respond_incorrectly(self):
        """Record an incorrect answer for the review and return it."""
        return Answer.objects.create(
            review=self,
            kind="INCORRECT"
        )

    def respond_memorized(self):
        """Record an answer of the MEMORIZED kind and return it."""
        return Answer.objects.create(
            review=self,
            kind="MEMORIZED"
        )


class Answer(models.Model):
    """Represents an answers given to a particular Review of a card
    Aspect. It can be one of the following kinds:

    + CORRECT
    + INCORRECT
    + MEMORIZED"""
    review = models.OneToOneField(
        Review,
        related_name="answer",
        on_delete=models.CASCADE
    )
    date = models.DateTimeField(auto_now_add=True)
    kind = models.CharField(max_length=20, choices=ANSWER_KINDS)

    class Meta:
        ordering = ["date"]
