from datetime import datetime
import os

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase

from mazo.forms import create_card, create_deck, create_file_upload
from mazo.models import Card, Deck


# FORM HELPERS TESTS
# ==================

class CreateDeckTests(TestCase):
    """Tests for the procedure forms.create_deck."""
    def test_create_deck(self):
        # Define deck data and files.
        data = {
            "creation_date": "2022-05-21T13:07:00+0000",
            "name": "Spanish Interjections",
            "description": "Common and not so common interjections."
        }
        icon_path = os.path.join(
            "mazo", "tests", "data", "icon.svg"
        )
        files = {
            "icon": create_file_upload(icon_path)
        }

        # Test results.
        self.assertIsInstance(
            create_deck(data, files),
            Deck
        )

    def test_record_creation_date_from_data_when_provided(self):
        """Newly created decks are assigned a creation date automatically, but this must be overwritten if provided in the data dictionary (e.g. when importing a deck)."""
        # Define deck data.
        creation_date = "2020-04-19T13:30:00+0000"
        data = {
            "creation_date": creation_date,
            "name": "Natural Sites",
            "description": "UNESCO's World Heritage Natural Sites."
        }

        # Expected date.
        source_date = datetime.strptime(
            creation_date,
            "%Y-%m-%dT%H:%M:%S%z"
        )

        # Create deck.
        deck = create_deck(data)

        # Test result.
        self.assertEqual(source_date, deck.creation_date)

    def test_report_missing_deck_name_error(self):
        # Define deck data.
        data = {
            "description": "Some deck with no name."
        }

        # Attempt to create deck (which should fail).
        errors = create_deck(data)

        # Test result.
        self.assertIn("name", errors)


class CreateCardTests(TestCase):
    """Tests for the procedure forms.create_card."""
    def test_create_card(self):
        # Define parent deck.
        deck = Deck.objects.create(name="Hiragana")

        # Define card data.
        data = {
            "deck": deck,
            "text": "む",
            "text_b": "mu",
            "creation_date": "2020-01-15T10:00:00+0000"
        }

        # Define card files.
        image_path = os.path.join(
            "mazo", "tests", "data", "card-image.svg"
        )
        sound_path = os.path.join(
            "mazo", "tests", "data", "card-sound.oga"
        )
        files = {
            "image": create_file_upload(image_path),
            "sound": create_file_upload(sound_path)
        }

        # Test results.
        self.assertIsInstance(
            create_card(data, files),
            Card
        )

    def test_record_creation_date_from_data_when_provided(self):
        """Newly created cards are assigned a creation date automatically, but it must be overwritten if provided in the data dictionary (e.g. when importing a deck)."""
        # Define deck.
        deck = Deck.objects.create(name="Spanish/English Vocab")

        # Define card data.
        creation_date = "2020-01-15T10:00:00+0000"
        data = {
            "deck": deck,
            "text": "armario",
            "text_b": "wardrobe",
            "creation_date": creation_date
        }

        # Expected date.
        source_date = datetime.strptime(
            creation_date,
            "%Y-%m-%dT%H:%M:%S%z"
        )

        # Create card.
        card = create_card(data)

        # Test result.
        self.assertEqual(source_date, card.creation_date)

    def test_report_missing_deck_field_error(self):
        # Define card data with no deck field.
        data = {
            "text": "cat",
            "text_b": "gato"
        }

        # Attempt to create card (which should fail).
        errors = create_card(data)

        # Test result.
        self.assertIn("deck", errors)

    def test_report_not_enough_values_for_studying_error(self):
        """Creating a card requires at least two fields to be filled.

        The two fields must be any of the following:

        + Card.text
        + Card.text_b
        + Card.image
        + Card.sound

        """
        # Define parent deck.
        deck = Deck.objects.create(name="Spanish Nouns")

        # Define card data.
        data = {
            "deck": deck,
            "text": "mantel"
        }

        # Attempt to create card (which should fail).
        errors = create_card(data)

        # Test result.
        self.assertIn("__all__", errors)


class CreateFileUploadTests(TestCase):
    """Tests for the procedure forms.create_file_upload."""
    def test_create_file_upload(self):
        # Path to test file.
        path = os.path.join(
            "mazo", "tests", "data", "icon.svg"
        )

        # Test results.
        self.assertIsInstance(
            create_file_upload(path),
            SimpleUploadedFile
        )

    def test_raise_os_error_if_file_does_not_exist(self):
        # Non-existent path to file.
        path = os.path.join(
            "mazo", "tests", "data", "end.png"
        )

        # Test results.
        with self.assertRaises(FileNotFoundError):
            create_file_upload(path)
