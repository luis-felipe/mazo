from datetime import datetime
import os

from django.test import TestCase

from mazo.ie import import_deck
from mazo.models import Deck


# IMPORT TESTS
# ============

class ImportDeckTests(TestCase):
    """Tests for the procedure ie.import_deck."""
    def test_import_a_deck_with_no_cards(self):
        # Import deck.
        test_archive = os.path.join(
            "mazo", "tests", "data", "empty-deck.zip"
        )
        result = import_deck(test_archive)

        # Test result.
        self.assertIsInstance(result, Deck)

    def test_record_creation_date_from_data_when_provided(self):
        """Newly created decks are assigned a datetime automatically, which must be overwritten on import with the actual date of the imported deck, if provided."""
        # Import deck.
        test_archive = os.path.join(
            "mazo", "tests", "data", "empty-deck.zip"
        )
        result = import_deck(test_archive)

        # Creation date from test archive.
        source_date = datetime.strptime(
            "2022-04-19T13:30:00+0000",
            "%Y-%m-%dT%H:%M:%S%z"
        )

        # Test result.
        self.assertEqual(source_date, result.creation_date)

    def test_credits_field_is_imported(self):
        # Import deck.
        test_archive = os.path.join(
            "mazo", "tests", "data", "deck-with-credits.zip"
        )
        result = import_deck(test_archive)

        # Expected credits.
        credits = "Author: John Doe · Graphics: Jane Roe"

        # Test result.
        self.assertEqual(result.credits, credits)

    def test_report_invalid_zip_error(self):
        # Try importing an invalid ZIP.
        test_archive = os.path.join(
            "mazo", "tests", "data", "invalid.zip"
        )
        result = import_deck(test_archive)

        # Test result.
        self.assertIn("InvalidZipError", result)

    def test_report_no_json_file_in_deck_archive_error(self):
        # Try importing an invalid ZIP.
        test_archive = os.path.join(
            "mazo", "tests", "data", "no-json.zip"
        )
        result = import_deck(test_archive)

        # Test result.
        self.assertIn("NoJsonFileInDeckArchiveError", result)

    def test_report_missing_format_version_error(self):
        # The deck.json in this ZIP does not have a version field.
        test_archive = os.path.join(
            "mazo", "tests", "data", "no-format-version.zip"
        )

        # Attempt to import the deck (which should fail).
        errors = import_deck(test_archive)

        # Test result.
        self.assertIn("NoFormatVersionError", errors)

    def test_report_missing_deck_name_error(self):
        # The deck.json in this ZIP does not have a name field.
        test_archive = os.path.join(
            "mazo", "tests", "data", "no-deck-name.zip"
        )

        # Attempt to import the deck (which should fail).
        errors = import_deck(test_archive)

        # Test result.
        self.assertIn("name", errors)
