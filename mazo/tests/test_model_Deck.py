from django.db.models import Sum
from django.test import TestCase
from django.utils import timezone

from mazo.constants import ASPECT_POINTS_FOR_MEMORIZATION
from mazo.models import Aspect, Card, Deck, Review


# MODEL TESTS
# ===========

class CountLearnedCardsTests(TestCase):
    """Tests for the method Deck.count_learned_cards."""
    def test_return_the_number_of_learned_cards(self):
        # Create a deck and some cards.
        deck = Deck.objects.create(name="Charts")
        card_a = Card.objects.create(
            deck=deck,
            image="icicle.png",
            text="Icicle plot"
        )
        card_b = Card.objects.create(
            deck=deck,
            image="bubble.png",
            text="Bubble chart"
        )
        Card.objects.create(
            deck=deck,
            image="histogram.png",
            text="Histogram"
        )

        # Mark all aspects of cards A and B as learned so that these
        # cards are considered learned.
        for aspect in card_a.aspects.all():
            aspect.set_is_learned(True)

        for aspect in card_b.aspects.all():
            aspect.set_is_learned(True)

        # Test result.
        self.assertEqual(
            deck.count_learned_cards(),
            2
        )

    def test_return_zero_when_no_card_is_learned(self):
        # Create a deck and some cards.
        deck = Deck.objects.create(name="Flower parts")
        Card.objects.create(
            deck=deck,
            image="pedicel.png",
            text="Pedicel"
        )
        Card.objects.create(
            deck=deck,
            image="ovary.png",
            text="Ovary"
        )
        Card.objects.create(
            deck=deck,
            image="stigma.png",
            text="Stigma"
        )

        # Test result.
        self.assertEqual(
            deck.count_learned_cards(),
            0
        )


class GetPartialProgressTests(TestCase):
    """Tests for the method Deck.get_partial_progress."""
    def test_return_one_when_all_aspects_reach_the_points_goal(self):
        # Create a deck and some cards.
        deck = Deck.objects.create(name="Fruits")
        Card.objects.create(
            deck=deck,
            image="pineapple.png",
            text="Pineapple"
        )
        Card.objects.create(
            deck=deck,
            image="watermelon.png",
            text="Watermelon"
        )
        Card.objects.create(
            deck=deck,
            image="cantaloupe.png",
            text="Cantaloupe"
        )

        # NOTE: When these cards were created, two aspects were created
        # automatically for each of them: a VISUAL aspect and a TEXTUAL
        # aspect. Also, one review was scheduled for every aspect.

        # Respond all default reviews correctly.
        for review in Review.objects.all():
            review.respond_correctly()

        # Respond enough reviews correctly for every aspect, so that
        # every aspect reaches the goal of retention points to be
        # considered memorized. The goal is indicated by
        # mazo.constants.ASPECT_POINTS_FOR_MEMORIZATION.
        for aspect in Aspect.objects.all():
            while (aspect.retention_points < ASPECT_POINTS_FOR_MEMORIZATION):
                review = aspect.schedule_review(timezone.now())
                review.respond_correctly()

        # Test result.
        self.assertEqual(
            deck.get_partial_progress(),
            1.0
        )

    def test_return_zero_when_all_aspects_have_zero_retention_points(self):
        deck = Deck.objects.create(name="Fruits")
        Card.objects.create(
            deck=deck,
            image="apple.png",
            text="Apple"
        )
        Card.objects.create(
            deck=deck,
            image="banana.png",
            text="Banana"
        )
        Card.objects.create(
            deck=deck,
            image="kiwi.png",
            text="Kiwi"
        )

        # NOTE: When these cards were created, two aspects were created
        # automatically for each of them: a VISUAL aspect and a TEXTUAL
        # aspect. Also, one review was scheduled for every aspect.

        # Don't answer any review, so that all aspects have zero
        # retention points.

        # Test result.
        self.assertEqual(
            deck.get_partial_progress(),
            0.0
        )

    def test_return_zero_when_the_deck_has_no_cards(self):
        deck = Deck.objects.create(name="Dark matter components")

        # Test result.
        self.assertEqual(
            deck.get_partial_progress(),
            0.0
        )

    def test_return_a_fraction_when_there_is_some_progress(self):
        # Create a deck and some cards.
        deck = Deck.objects.create(name="Fruits")
        Card.objects.create(
            deck=deck,
            image="mago.png",
            text="Mango"
        )
        Card.objects.create(
            deck=deck,
            image="papaya.png",
            text="Papaya"
        )
        Card.objects.create(
            deck=deck,
            image="lemon.png",
            text="Lemon"
        )

        # NOTE: When these cards were created, two aspects were created
        # automatically for each of them: a VISUAL aspect and a TEXTUAL
        # aspect. Also, one review was scheduled for every aspect.

        # Respond all visual reviews correctly.
        for review in Review.objects.filter(aspect__kind="VISUAL"):
            review.respond_correctly()

        # Get currect retention points and total retention points.
        aspects = Aspect.objects.all()
        total_points = aspects.count() * ASPECT_POINTS_FOR_MEMORIZATION
        current_points = aspects.aggregate(
            Sum("retention_points")
        )["retention_points__sum"]

        # Test result.
        self.assertEqual(
            deck.get_partial_progress(),
            current_points/total_points
        )


class GetPreviousProgressTests(TestCase):
    """Tests for the method Deck.get_previous_progress."""
    def test_return_progress_of_the_penultimate_finished_study_session(self):
        # Create a deck and some cards.
        deck = Deck.objects.create(name="Spanish adjectives")
        Card.objects.create(
            deck=deck,
            image="amargo.png",
            text="amargo"
        )
        Card.objects.create(
            deck=deck,
            image="sabroso.png",
            text="sabroso"
        )
        Card.objects.create(
            deck=deck,
            image="aburridor.png",
            text="aburridor"
        )

        # Start a study session and finish it.
        session_kind = "VISUAL"
        study_session_1 = deck.start_session(session_kind)

        for review in study_session_1.reviews.all():
            review.respond_correctly()

        study_session_1.finish()

        # Start another study session and finish it.
        session_kind = "TEXTUAL"
        study_session_2 = deck.start_session(session_kind)

        for review in study_session_2.reviews.all():
            review.respond_correctly()

        study_session_2.finish()

        # Start a final study session (don't finish it).
        session_kind = "TEXTUAL"
        deck.start_session(session_kind)

        # Test result.
        self.assertEqual(
            deck.get_previous_progress(),
            study_session_1.progress
        )

    def test_return_zero_when_there_are_no_study_sessions_yet(self):
        # Create an empty deck.
        deck = Deck.objects.create(name="Spaceships from Saturn")

        # Test result.
        self.assertEqual(
            deck.get_previous_progress(),
            0
        )

    def test_return_zero_when_there_is_no_penultimate_study_session(self):
        # Create a deck and some cards.
        deck = Deck.objects.create(name="Spanish verbs")
        Card.objects.create(
            deck=deck,
            image="trotar.gif",
            text="trotar"
        )
        Card.objects.create(
            deck=deck,
            image="nadar.gif",
            text="nadar"
        )
        Card.objects.create(
            deck=deck,
            image="volar.gif",
            text="volar"
        )

        # Start a study session (don't finish it).
        deck.start_session("VISUAL")

        # Test result.
        self.assertEqual(
            deck.get_previous_progress(),
            0
        )


class GetProgressTests(TestCase):
    """Tests for the method Deck.get_progress."""
    def test_return_a_fraction_when_aspects_are_not_fully_learned(self):
        # Create a deck and cards with their default reviews.
        deck = Deck.objects.create(name="Charts")
        card_a = Card.objects.create(
            deck=deck,
            image="candlestick.png",
            text="Candlestick chart"
        )
        card_b = Card.objects.create(
            deck=deck,
            image="waterfall.png",
            text="Waterfall chart"
        )
        Card.objects.create(
            deck=deck,
            image="funnel.png",
            text="Funnel"
        )

        # Respond all visual reviews correctly.
        aspect_kind = "VISUAL"
        for review in Review.objects.filter(aspect__kind=aspect_kind):
            review.respond_correctly()

        # Mark visual aspects of cards A and B as learned.
        card_a.aspects.filter(kind=aspect_kind).first().set_is_learned(True)
        card_b.aspects.filter(kind=aspect_kind).first().set_is_learned(True)

        # Compute expected deck progress.
        aspects = Aspect.objects.filter(card__deck=deck)
        total_aspects = aspects.count()
        total_points = total_aspects * (ASPECT_POINTS_FOR_MEMORIZATION + 1)
        sum_of_aspects_points = aspects.aggregate(
            Sum("retention_points")
        )["retention_points__sum"]
        num_aspects_learned = aspects.filter(is_learned=True).count()
        current_points = sum_of_aspects_points + num_aspects_learned

        progress = current_points / total_points

        # Test result.
        self.assertEqual(
            deck.get_progress(),
            progress
        )

    def test_return_zero_when_the_deck_has_no_cards(self):
        # Create a deck.
        deck = Deck.objects.create(name="Clothing")

        # Test result.
        self.assertEqual(
            deck.get_progress(),
            0.0
        )

    def test_return_one_when_all_aspects_are_considered_memorized_and_marked_as_learned_by_the_user(self):
        # Create a deck and cards with their default reviews.
        deck = Deck.objects.create(name="Charts")
        Card.objects.create(
            deck=deck,
            image="candlestick.png",
            text="Candlestick chart"
        )
        Card.objects.create(
            deck=deck,
            image="waterfall.png",
            text="Waterfall chart"
        )
        Card.objects.create(
            deck=deck,
            image="funnel.png",
            text="Funnel"
        )

        # Respond all default reviews correctly.
        for review in Review.objects.all():
            review.respond_correctly()

        # Schedule additional reviews for all aspects and respond them
        # correctly so that they are considered memorized according to
        # mazo.constants.ASPECT_POINTS_FOR_MEMORIZATION.
        for aspect in Aspect.objects.all():
            for point in range(ASPECT_POINTS_FOR_MEMORIZATION - 1):
                # Minus one, because the default review is complete now.
                review = aspect.schedule_review(timezone.now())
                review.respond_correctly()

        # Mark all aspects as learned.
        aspects = Aspect.objects.filter(card__deck=deck)
        for aspect in aspects:
            aspect.set_is_learned(True)

        # Test result.
        self.assertEqual(
            deck.get_progress(),
            1.0
        )


class HasAuralCards(TestCase):
    """Tests for method Deck.has_aural_cards."""
    def test_know_when_there_are_aural_cards(self):
        # Create deck and cards.
        deck = Deck.objects.create(name="Animals")
        Card.objects.create(
            deck=deck,
            sound="Wolf.opus",
            text="Wolf"
        )

        # Test result.
        self.assertTrue(
            deck.has_aural_cards(),
            "The deck has an aural card, but it says it doesn't."
        )

    def test_know_when_there_are_no_aural_cards(self):
        # Create deck and cards.
        deck = Deck.objects.create(name="Animals")
        Card.objects.create(
            deck=deck,
            image="Owl.png",
            text="Owl"
        )

        # Test result.
        self.assertFalse(
            deck.has_aural_cards(),
            "The deck has no aural cards, but it says it does."
        )


class HasTextualCards(TestCase):
    """Tests for method Deck.has_textual_cards."""
    def test_know_when_there_are_textual_cards(self):
        # Create deck and cards.
        deck = Deck.objects.create(name="Animals")
        Card.objects.create(
            deck=deck,
            sound="Human.opus",
            text="Human"
        )

        # Test result.
        self.assertTrue(
            deck.has_textual_cards(),
            "The deck has a textual card, but it says it doesn't."
        )

    def test_know_when_there_are_no_textual_cards(self):
        # Create deck and cards.
        deck = Deck.objects.create(name="Animals")
        Card.objects.create(
            deck=deck,
            image="dolphin.png",
            sound="dolphin.opus"
        )

        # Test result.
        self.assertFalse(
            deck.has_textual_cards(),
            "The deck has no textual cards, but it says it does."
        )


class HasVisualCards(TestCase):
    """Tests for method Deck.has_visual_cards."""
    def test_know_when_there_are_visual_cards(self):
        # Create deck and cards.
        deck = Deck.objects.create(name="Elements")
        Card.objects.create(
            deck=deck,
            image="Helium.png",
            text="Helium"
        )

        # Test result.
        self.assertTrue(
            deck.has_visual_cards(),
            "The deck has a visual card, but it says it doesn't."
        )

    def test_know_when_there_are_no_visual_cards(self):
        # Create deck and cards.
        deck = Deck.objects.create(name="Elements")
        Card.objects.create(
            deck=deck,
            sound="Gallium.opus",
            text="Gallium"
        )

        # Test result.
        self.assertFalse(
            deck.has_visual_cards(),
            "The deck has no visual cards, but it says it does."
        )


class isAspectLearnedTests(TestCase):
    """Tests for the method Deck.is_aspect_learned."""
    def test_return_true_when_all_visual_aspects_have_been_learned(self):
        # Create deck and cards.
        deck = Deck.objects.create(name="Planets")
        Card.objects.create(
            deck=deck,
            image="earth.png",
            text="Earth"
        )
        Card.objects.create(
            deck=deck,
            image="neptune.png",
            text="Neptune"
        )
        Card.objects.create(
            deck=deck,
            image="saturn.png",
            text="Saturn"
        )

        # Mark all visual aspects sneakily as learned without having
        # studied any of the cards.
        visual_aspects = Aspect.objects.filter(card__deck=deck, kind="VISUAL")
        for aspect in visual_aspects:
            aspect.set_is_learned(True)

        # Test result.
        self.assertTrue(
            deck.is_aspect_learned("VISUAL"),
            msg="The deck says its visual aspect is not learned, but it should be."
        )

    def test_return_false_when_any_visual_aspect_is_not_learned(self):
        # Create deck and cards.
        deck = Deck.objects.create(name="Planets")
        Card.objects.create(
            deck=deck,
            image="mars.png",
            text="Mars"
        )
        Card.objects.create(
            deck=deck,
            image="venus.png",
            text="Venus"
        )
        Card.objects.create(
            deck=deck,
            image="jupiter.png",
            text="Jupiter"
        )

        # Mark some visual aspects sneakily as learned without having
        # studied any of the cards.
        visual_aspects = Aspect.objects.filter(card__deck=deck, kind="VISUAL")
        for aspect in visual_aspects[:2]:
            aspect.set_is_learned(True)

        # Test result.
        self.assertFalse(
            deck.is_aspect_learned("VISUAL"),
            msg="The deck says its visual aspect is learned, but it shouln't be."
        )

    def test_return_true_when_all_aural_aspects_have_been_learned(self):
        # Create deck and cards.
        deck = Deck.objects.create(name="Horns")
        Card.objects.create(
            deck=deck,
            sound="lur.opus",
            text="Lur"
        )
        Card.objects.create(
            deck=deck,
            sound="tuba.opus",
            text="Tuba"
        )
        Card.objects.create(
            deck=deck,
            sound="serpent.opus",
            text="Serpent"
        )

        # Mark all aural aspects sneakily as learned without having
        # studied any of the cards.
        aspect_kind = "AURAL"
        aural_aspects = Aspect.objects.filter(
            card__deck=deck,
            kind=aspect_kind
        )
        for aspect in aural_aspects:
            aspect.set_is_learned(True)

        # Test result.
        self.assertTrue(
            deck.is_aspect_learned(aspect_kind),
            msg="The deck says its aural aspect is not learned, but it should be."
        )

    def test_return_false_when_any_aural_aspect_is_not_learned(self):
        # Create deck and cards.
        deck = Deck.objects.create(name="Planets")
        Card.objects.create(
            deck=deck,
            image="mars.png",
            text="Mars"
        )
        Card.objects.create(
            deck=deck,
            image="venus.png",
            text="Venus"
        )
        Card.objects.create(
            deck=deck,
            image="jupiter.png",
            text="Jupiter"
        )

        # Mark some aural aspects sneakily as learned without having
        # studied any of the cards.
        aspect_kind = "AURAL"
        aural_aspects = Aspect.objects.filter(
            card__deck=deck,
            kind=aspect_kind
        )
        for aspect in aural_aspects[:2]:
            aspect.set_is_learned(True)

        # Test result.
        self.assertFalse(
            deck.is_aspect_learned(aspect_kind),
            msg="The deck says its aural aspect is learned, but it shouln't be."
        )

    def test_return_false_when_the_deck_is_empty(self):
        # Create a deck.
        deck = Deck.objects.create(name="Alien species")

        # Test result.
        self.assertFalse(
            deck.is_aspect_learned("VISUAL"),
            msg="The deck says its visual aspect is learned, but the deck is empty."
        )


class StartRoundTests(TestCase):
    """Tests for the method Deck.start_round."""
    def test_create_a_new_study_round(self):
        # Create a deck and its default round.
        deck = Deck.objects.create(name="X Equipment")

        # Finish the default round.
        deck.rounds.last().finish()

        # Start a new round.
        new_round = deck.start_round()

        # Test result.
        self.assertEqual(
            deck.rounds.last(),
            new_round
        )

    def test_return_false_if_there_are_ongoing_rounds(self):
        # Create a deck and its default round.
        deck = Deck.objects.create(name="Whales")

        # Let the default round unfinished.

        # Test result.
        self.assertFalse(deck.start_round())


class StartSessionTests(TestCase):
    """Tests for the method Deck.start_session."""
    def test_create_a_new_session_for_visual_study(self):
        # Create a deck and its default Round.
        deck = Deck.objects.create(name="Clothing")

        # Start a visual study session.
        session = deck.start_session("VISUAL")

        # Test result.
        round = deck.rounds.first()
        ongoing_session = round.sessions.first()
        self.assertEqual(session, ongoing_session)

    def test_close_ongoing_session_before_starting_a_new_one(self):
        """Users can abandon study sessions at any time. These sessions
        can't be continued because their related cards might have been
        removed in the meantime. So starting a new session should close
        any ongoing session and start a new one instead."""
        # Create a deck and its default Round.
        deck = Deck.objects.create(name="Colors")

        # Start a study session, don't finish it, and start another
        # one, but finish it.
        session_kind = "VISUAL"
        deck.start_session(session_kind)           # Session 1
        deck.start_session(session_kind).finish()  # Session 2

        # Get ongoing study sessions (there shouldn't be any, though).
        current_round = deck.rounds.order_by("number").last()
        ongoing_sessions = current_round.sessions.filter(
            kind=session_kind,
            finish_date__isnull=True
        )

        # Test result.
        self.assertFalse(
            ongoing_sessions.exists(),
            "There are ongoing sessions, but there shouldn't be any."
        )


class UnlearnCardsTests(TestCase):
    """Tests for the method Deck.unlearn_cards."""
    def test_unlearn_cards(self):
        # Create deck and cards.
        deck = Deck.objects.create(name="Charts")
        Card.objects.create(
            deck=deck,
            image="candlestick.png",
            text="Candlestick chart"
        )
        Card.objects.create(
            deck=deck,
            image="waterfall.png",
            text="Waterfall chart"
        )
        Card.objects.create(
            deck=deck,
            image="funnel.png",
            text="Funnel"
        )

        # Mark all card aspects as learned.
        cards = deck.cards.all()
        for card in cards:
            for aspect in card.aspects.all():
                aspect.set_is_learned(True)

        # Unlearn cards.
        deck.unlearn_cards()

        # List the value of Aspect.is_learned for all aspects.
        aspects_learned = []  # List of Boolean.
        for card in cards:
            for aspect in card.aspects.all():
                aspects_learned.append(aspect.is_learned)

        # Test result.
        self.assertFalse(
            any(aspects_learned),
            msg="There are learned card aspects when there shouldn't be any."
        )


# SIGNAL HANDLERS TESTS
# =====================

class OnDeckCreatedTests(TestCase):
    """Tests for the signal handler on_deck_created
    (see mazo.signals)."""
    def test_create_a_default_study_round_automatically(self):
        # Create a deck and its default Round.
        deck = Deck.objects.create(name="Viruses")

        # Test result.
        round_count = deck.rounds.all().count()
        self.assertEqual(round_count, 1)
