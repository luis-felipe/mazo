from django.test import TestCase

from mazo.models import Card, Deck, Review, StudySession


# MODEL TESTS
# ===========

class FinishTests(TestCase):
    """Tests for the method StudySession.finish."""
    def test_set_finish_date_to_the_current_datetime(self):
        # Create a deck and its default Round.
        deck = Deck.objects.create(name="Viruses")
        round = deck.rounds.last()

        # Start a study session and finish it.
        session = StudySession.objects.create(
            round=round,
            kind="VISUAL"
        )
        finish_date = session.finish()

        # Test result.
        self.assertEqual(session.finish_date, finish_date)

    def test_do_not_alter_the_finish_date_of_a_finished_session(self):
        # Create a deck and its default round.
        deck = Deck.objects.create(name="Islands")
        round = deck.rounds.last()

        # Start a study session and finish it.
        session = StudySession.objects.create(
            round=round,
            kind="VISUAL"
        )
        finish_date = session.finish()

        # Attempt to finish the same session again.
        session.finish()

        # Test result.
        self.assertEqual(session.finish_date, finish_date)

    def test_set_session_progress_to_current_deck_progress(self):
        # Create a deck and some cards.
        deck = Deck.objects.create(name="Spanish nouns")
        Card.objects.create(
            deck=deck,
            image="loma.png",
            text="loma"
        )
        Card.objects.create(
            deck=deck,
            image="rama.png",
            text="rama"
        )
        Card.objects.create(
            deck=deck,
            image="cascada.png",
            text="cascada"
        )

        # Start a study session and finish it.
        session_kind = "VISUAL"
        study_session = deck.start_session(session_kind)

        for review in study_session.reviews.all():
            review.respond_correctly()

        study_session.finish()

        # At this point memorization progress has increased with
        # every correct answer and this progress has been recorded
        # at the moment the session was finished.

        # Test result.
        self.assertGreater(
            study_session.progress,
            0,
            "Progress is not greater than zero, but it should be."
        )


class NextReviewTests(TestCase):
    """Tests for the method StudySession.next_review."""
    def test_raise_index_error_when_the_session_has_no_reviews(self):
        # Create a deck and its default Round.
        deck = Deck.objects.create(name="Gods")
        round = deck.rounds.last()

        # Start a study session.
        session_kind = "VISUAL"
        session = StudySession.objects.create(
            round=round,
            kind=session_kind
        )

        # Test result.
        self.assertRaises(
            IndexError,
            session.next_review
        )

    def test_return_a_review_when_the_session_has_pending_reviews(self):
        # Create a deck and its default Round.
        deck = Deck.objects.create(name="Animals")
        round = deck.rounds.last()

        # Add some cards to the deck.
        Card.objects.create(
            deck=deck,
            image="grison.png",
            text="Grison"
        )
        Card.objects.create(
            deck=deck,
            image="Dingo.png",
            text="Dingo"
        )

        # Start a study session.
        session_kind = "VISUAL"
        session = StudySession.objects.create(
            round=round,
            kind=session_kind
        )

        # Test result.
        self.assertIsInstance(
            session.next_review(),
            Review
        )


# SIGNAL HANDLERS TESTS
# =====================

class OnStudySessionCreatedTests(TestCase):
    """Tests for the signal handler on_study_session_created
    (see mazo.signals)."""
    def test_tell_visual_session_to_claim_visual_reviews(self):
        # Create a deck and its default Round.
        deck = Deck.objects.create(name="Fruits")
        round = deck.rounds.last()

        # Add some cards to the deck.
        Card.objects.create(
            deck=deck,
            image="mango.png",
            text="Mango"
        )
        Card.objects.create(
            deck=deck,
            image="lulo.png",
            text="Lulo"
        )

        # At this point, VISUAL and TEXTUAL reviews were scheduled
        # automatically for each card.

        # Start a visual study session.
        session_kind = "VISUAL"
        visual_session = StudySession.objects.create(
            round=round,
            kind=session_kind
        )

        # At this point, pending visual reviews were associated with the
        # visual session started above. In this case, the session should
        # have two visual reviews corresponding to the two visual cards
        # created.
        reviews = visual_session.reviews.all()

        # Test result.
        self.assertEqual(
            reviews.count(),
            2
        )
        self.assertEqual(
            reviews.first().aspect.kind,
            session_kind
        )
        self.assertEqual(
            reviews.last().aspect.kind,
            session_kind
        )
