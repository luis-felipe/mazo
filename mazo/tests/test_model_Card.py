from django.test import TestCase

from mazo.models import Card, Deck


# MODEL TESTS
# ===========

class StrTests(TestCase):
    """Tests for method Card.__str__."""
    def test_str_representation_of_a_multimedia_card_is_well_formatted(self):
        # Create card.
        deck = Deck.objects.create(name="Musical Instruments")
        card_image = "transverse-flute.png"
        card_sound = "transverse-flute.opus"
        card_text = "Transverse flute"

        card = Card.objects.create(
            deck=deck,
            image=card_image,
            sound=card_sound,
            text=card_text
        )

        # Test result.
        self.assertEqual(
            str(card),
            "image:{} | sound:{} | text:{}".format(
                card_image, card_sound, card_text
            )
        )


class CreateAspectsTests(TestCase):
    """Tests for Card.create_aspects method."""
    def test_create_enough_aspects_according_to_card_properties(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Vehicles")
        card = Card.objects.create(
            deck=deck,
            image="car.png",
            sound="car.opus",
            text="Car"
        )

        # Create aspects.
        #
        # The aspects of a card are created automatically when the card
        # is created, so there is no need to call card.create_aspects()
        # directly.

        # Test result.
        self.assertEqual(
            card.aspects.count(),
            # Three aspects because the card is VISUAL (has an image),
            # AURAL (has sound), and TEXTUAL (has text).
            3
        )

    def test_prevent_duplicate_aspects(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Vehicles")
        card = Card.objects.create(
            deck=deck,
            image="motorcycle.png",
            sound="motorcycle.opus",
            text="Motorcycle"
        )

        # Create aspects.
        #
        # card.create_aspects() is called automatically when the card is
        # created (see mazo.signals.on_card_created), so calling it
        # again should not create more aspects.
        card.create_aspects()

        # Test result.
        self.assertEqual(
            card.aspects.count(),
            # Three aspects because the card is VISUAL (has an image),
            # AURAL (has sound), and TEXTUAL (has text).
            3
        )

    def test_create_a_visual_aspect_if_the_card_has_an_image(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Planets")
        card = Card.objects.create(
            deck=deck,
            image="saturn.png",
            text="Saturn"
        )

        # Create aspects.
        card.create_aspects()

        # Test result.
        self.assertTrue(
            card.aspects.filter(kind="VISUAL").exists(),
            msg="There is no VISUAL aspect, but it should exist"
        )

    def test_create_an_aural_aspect_if_the_card_has_sound(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Musical instruments")
        card = Card.objects.create(
            deck=deck,
            sound="ocarina.opus",
            text="Ocarina"
        )

        # Create aspects.
        card.create_aspects()

        # Test result.
        self.assertTrue(
            card.aspects.filter(kind="AURAL").exists(),
            msg="There is no AURAL aspect, but it should exist"
        )

    def test_create_a_textual_aspect_if_the_card_has_text(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Spanish vocabulary")
        card = Card.objects.create(
            deck=deck,
            text="metralla",
            text_b="shrapnel"
        )

        # Create aspects.
        card.create_aspects()

        # Test result.
        self.assertTrue(
            card.aspects.filter(kind="TEXTUAL").exists(),
            msg="There is no TEXTUAL aspect, but it should exist"
        )

    def test_dont_create_aural_aspect_if_the_card_is_not_aural(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Musical instruments")
        card = Card.objects.create(
            deck=deck,
            image="trumpet.png",
            text="Trumpet"
        )

        # Create aspects.
        #
        # The aspects of a card are created automatically when the card
        # is created, so there is no need to call card.create_aspects()
        # directly.

        # Test result.
        self.assertFalse(
            card.aspects.filter(kind="AURAL").exists(),
            msg="There is an AURAL aspect, but there shouldn't"
        )

    def test_dont_create_textual_aspect_if_the_card_is_not_textual(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Musical instruments")
        card = Card.objects.create(
            deck=deck,
            image="viola.png",
            sound="viola.opus"
        )

        # Create aspects.
        #
        # The aspects of a card are created automatically when the card
        # is created, so there is no need to call card.create_aspects()
        # directly.

        # Test result.
        self.assertFalse(
            card.aspects.filter(kind="TEXTUAL").exists(),
            msg="There is a TEXTUAL aspect, but there shouldn't"
        )

    def test_dont_create_visual_aspect_if_the_card_is_not_visual(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Musical instruments")
        card = Card.objects.create(
            deck=deck,
            sound="cello.opus",
            text="Cello"
        )

        # Create aspects.
        #
        # The aspects of a card are created automatically when the card
        # is created, so there is no need to call card.create_aspects()
        # directly.

        # Test result.
        self.assertFalse(
            card.aspects.filter(kind="VISUAL").exists(),
            msg="There is a VISUAL aspect, but there shouldn't"
        )


class IsAuralTests(TestCase):
    """Tests for method Card.is_aural."""
    def test_aural_card_knows_it_is_aural(self):
        # Create card.
        deck = Deck.objects.create(name="Birds")
        card = Card.objects.create(
            deck=deck,
            sound="tanager.opus",
            text="Tanager"
        )

        # Test result.
        self.assertTrue(
            card.is_aural(),
            "The card is aural, but it says it is not."
        )

    def test_non_aural_card_knows_it_is_not_aural(self):
        # Create card.
        deck = Deck.objects.create(name="Trees")
        card = Card.objects.create(
            deck=deck,
            image="sequoia.png",
            text="Sequoia"
        )

        # Test result.
        self.assertFalse(
            card.is_aural(),
            "The card is not aural, but it says it is."
        )


class IsTextualTests(TestCase):
    """Tests for method Card.is_textual."""
    def test_textual_card_knows_it_is_textual(self):
        # Create card.
        deck = Deck.objects.create(name="Minerals")
        card = Card.objects.create(
            deck=deck,
            image="pyrite.png",
            text="Pyrite"
        )

        # Test result.
        self.assertTrue(
            card.is_textual(),
            "The card is textual, but it says it is not."
        )

    def test_non_textual_card_knows_it_is_not_textual(self):
        # Create card.
        deck = Deck.objects.create(name="Musical Instruments")
        card = Card.objects.create(
            deck=deck,
            image="clarinet.png",
            sound="clarinet.opus"
        )

        # Test result.
        self.assertFalse(
            card.is_textual(),
            "The card is not textual, but it says it is."
        )


class IsVisualTests(TestCase):
    """Tests for method Card.is_visual."""
    def test_visual_card_knows_it_is_visual(self):
        # Create card.
        deck = Deck.objects.create(name="Flowers")
        card = Card.objects.create(
            deck=deck,
            image="dandelion.png",
            text="Dandelion"
        )

        # Test result.
        self.assertTrue(
            card.is_visual(),
            "The card is visual, but it says it is not."
        )

    def test_non_visual_card_knows_it_is_not_visual(self):
        # Create card.
        deck = Deck.objects.create(name="Birds")
        card = Card.objects.create(
            deck=deck,
            sound="canary.opus",
            text="Canary"
        )

        # Test result.
        self.assertFalse(
            card.is_visual(),
            "The card is not visual, but it says it is."
        )


class IsLearnedTests(TestCase):
    """Tests for the method Card.is_learned."""
    def test_return_true_if_all_aspects_of_the_card_are_learned(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Musical instruments")
        card = Card.objects.create(
            deck=deck,
            image="xylophone.png",
            sound="xylophone.opus",
            text="Xylophone"
        )

        # Mark all card aspects as learned.
        for aspect in card.aspects.all():
            aspect.set_is_learned(True)

        # Test result.
        self.assertTrue(
            card.is_learned(),
            "The card says it is not learned, but it should be."
        )

    def test_return_false_if_any_aspect_of_the_card_is_not_learned(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Musical instruments")
        card = Card.objects.create(
            deck=deck,
            image="trumpet.png",
            sound="trumpet.opus",
            text="Trumpet"
        )

        # Test result.
        self.assertFalse(
            card.is_learned(),
            "The card says it is learned, but it shouldn't be."
        )


class RemoveAspectsTests(TestCase):
    """Tests for the method Card.remove_aspects."""
    def test_remove_visual_aspect_if_card_image_was_removed(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Aves")
        card = Card.objects.create(
            deck=deck,
            image="sirirí.png",
            sound="sirirí.opus",
            text="Sirirí"
        )

        # NOTE: When this card was created, three aspects were created
        # automatically: a VISUAL aspect, an AURAL aspect and a TEXTUAL
        # aspect.

        # Remove card image.
        card.image = None
        card.save()

        # Remove missing aspects.
        card.remove_aspects()

        # Test result.
        self.assertFalse(
            card.aspects.filter(kind="VISUAL").exists(),
            "The card has a visual aspect, but it shouldn't have one."
        )
        self.assertTrue(
            card.aspects.filter(kind="AURAL").exists(),
            "The card has no aural aspect, but it should have one."
        )
        self.assertTrue(
            card.aspects.filter(kind="TEXTUAL").exists(),
            "The card has no textual aspect, but it should have one."
        )

    def test_remove_aural_aspect_if_card_sound_was_removed(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Aves")
        card = Card.objects.create(
            deck=deck,
            image="guacharaca.png",
            sound="guacharaca.opus",
            text="Guacharaca"
        )

        # NOTE: When this card was created, three aspects were created
        # automatically: a VISUAL aspect, an AURAL aspect and a TEXTUAL
        # aspect.

        # Remove card sound.
        card.sound = None
        card.save()

        # Remove missing aspects.
        card.remove_aspects()

        # Test result.
        self.assertFalse(
            card.aspects.filter(kind="AURAL").exists(),
            "The card has an aural aspect, but it shouldn't have one."
        )
        self.assertTrue(
            card.aspects.filter(kind="VISUAL").exists(),
            "The card has no visual aspect, but it should have one."
        )
        self.assertTrue(
            card.aspects.filter(kind="TEXTUAL").exists(),
            "The card has no textual aspect, but it should have one."
        )

    def test_remove_textual_aspect_if_card_text_was_removed(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Aves")
        card = Card.objects.create(
            deck=deck,
            image="ruiseñor.png",
            sound="ruiseñor.opus",
            text="Ruiseñor"
        )

        # NOTE: When this card was created, three aspects were created
        # automatically: a VISUAL aspect, an AURAL aspect and a TEXTUAL
        # aspect.

        # Remove card text.
        card.text = ""
        card.save()

        # Remove missing aspects.
        card.remove_aspects()

        # Test result.
        self.assertFalse(
            card.aspects.filter(kind="TEXTUAL").exists(),
            "The card has a textual aspect, but it shouldn't have one."
        )
        self.assertTrue(
            card.aspects.filter(kind="VISUAL").exists(),
            "The card has no visual aspect, but it should have one."
        )
        self.assertTrue(
            card.aspects.filter(kind="AURAL").exists(),
            "The card has no aural aspect, but it should have one."
        )


# SIGNAL HANDLERS TESTS
# =====================

class OnCardCreatedTests(TestCase):
    """Tests for the signal handler on_card_created
    (see mazo.signals)."""
    def test_tell_card_to_create_its_aspects(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Aves")
        card = Card.objects.create(
            deck=deck,
            image="turpial.png",
            sound="turpial.opus",
            text="Turpial"
        )

        # Test result.
        self.assertEqual(
            card.aspects.filter(kind="VISUAL").count(),
            1
        )
        self.assertEqual(
            card.aspects.filter(kind="AURAL").count(),
            1
        )
        self.assertEqual(
            card.aspects.filter(kind="TEXTUAL").count(),
            1
        )

    def test_tell_card_to_schedule_reviews_for_its_aspects(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Birds")
        card = Card.objects.create(
            deck=deck,
            image="cockatoo.png",
            sound="cockatoo.opus",
            text="Cockatoo"
        )

        # NOTE: When this card was created, three aspects were created
        # automatically: a VISUAL aspect, an AURAL aspect and a TEXTUAL
        # aspect.

        # Get card aspects.
        visual_aspect = card.aspects.filter(kind="VISUAL").first()
        aural_aspect = card.aspects.filter(kind="AURAL").first()
        textual_aspect = card.aspects.filter(kind="AURAL").first()

        # Test result.
        # (There must be three reviews: visual, aural and textual)
        self.assertEqual(
            visual_aspect.reviews.all().count(),
            1
        )
        self.assertEqual(
            aural_aspect.reviews.all().count(),
            1
        )
        self.assertEqual(
            textual_aspect.reviews.all().count(),
            1
        )


class OnCardChangedTests(TestCase):
    """Tests for the signal handler on_card_changed
    (see mazo.signals)."""
    def test_tell_card_to_remove_visual_aspect_when_its_image_is_removed(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Aves")
        card = Card.objects.create(
            deck=deck,
            image="sirirí.png",
            sound="sirirí.opus",
            text="Sirirí"
        )

        # NOTE: When this card was created, three aspects were created
        # automatically: a VISUAL aspect, an AURAL aspect and a TEXTUAL
        # aspect.

        # Remove card image.
        card.image = None
        card.save()

        # Test result.
        self.assertFalse(
            card.aspects.filter(kind="VISUAL").exists(),
            "The card has a visual aspect, but it shouldn't have one."
        )
        self.assertTrue(
            card.aspects.filter(kind="AURAL").exists(),
            "The card has no aural aspect, but it should have one."
        )
        self.assertTrue(
            card.aspects.filter(kind="TEXTUAL").exists(),
            "The card has no textual aspect, but it should have one."
        )

    def test_tell_card_to_remove_aural_aspect_when_its_sound_is_removed(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Aves")
        card = Card.objects.create(
            deck=deck,
            image="guacharaca.png",
            sound="guacharaca.opus",
            text="Guacharaca"
        )

        # NOTE: When this card was created, three aspects were created
        # automatically: a VISUAL aspect, an AURAL aspect and a TEXTUAL
        # aspect.

        # Remove card sound.
        card.sound = None
        card.save()

        # Test result.
        self.assertFalse(
            card.aspects.filter(kind="AURAL").exists(),
            "The card has an aural aspect, but it shouldn't have one."
        )
        self.assertTrue(
            card.aspects.filter(kind="VISUAL").exists(),
            "The card has no visual aspect, but it should have one."
        )
        self.assertTrue(
            card.aspects.filter(kind="TEXTUAL").exists(),
            "The card has no textual aspect, but it should have one."
        )

    def test_tell_card_to_remove_textual_aspect_when_its_text_is_removed(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Aves")
        card = Card.objects.create(
            deck=deck,
            image="ruiseñor.png",
            sound="ruiseñor.opus",
            text="Ruiseñor"
        )

        # NOTE: When this card was created, three aspects were created
        # automatically: a VISUAL aspect, an AURAL aspect and a TEXTUAL
        # aspect.

        # Remove card text.
        card.text = ""
        card.save()

        # Test result.
        self.assertFalse(
            card.aspects.filter(kind="TEXTUAL").exists(),
            "The card has a textual aspect, but it shouldn't have one."
        )
        self.assertTrue(
            card.aspects.filter(kind="VISUAL").exists(),
            "The card has no visual aspect, but it should have one."
        )
        self.assertTrue(
            card.aspects.filter(kind="AURAL").exists(),
            "The card has no aural aspect, but it should have one."
        )

    def test_tell_card_to_add_a_visual_aspect_when_an_image_is_added(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Musical Instruments")
        card = Card.objects.create(
            deck=deck,
            sound="horn.opus",
            text="Horn"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: an AURAL aspect and a TEXTUAL aspect.

        # Add card image.
        card.image = "horn.png"
        card.save()

        # Test result.
        self.assertTrue(
            card.aspects.filter(kind="VISUAL").exists(),
            "The card has no visual aspect, but it should have one."
        )
        self.assertTrue(
            card.aspects.filter(kind="AURAL").exists(),
            "The card has no aural aspect, but it should have one."
        )
        self.assertTrue(
            card.aspects.filter(kind="TEXTUAL").exists(),
            "The card has no textual aspect, but it should have one."
        )

    def test_tell_card_to_add_an_aural_aspect_when_a_sound_is_added(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Musical Instruments")
        card = Card.objects.create(
            deck=deck,
            image="bassoon.png",
            text="bassoon"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: a VISUAL aspect, and a TEXTUAL aspect.

        # Add card sound.
        card.sound = "bassoon.opus"
        card.save()

        # Test result.
        self.assertTrue(
            card.aspects.filter(kind="AURAL").exists(),
            "The card has no aural aspect, but it should have one."
        )
        self.assertTrue(
            card.aspects.filter(kind="VISUAL").exists(),
            "The card has no visual aspect, but it should have one."
        )
        self.assertTrue(
            card.aspects.filter(kind="TEXTUAL").exists(),
            "The card has no textual aspect, but it should have one."
        )

    def test_tell_card_to_add_a_textual_aspect_when_text_is_added(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Musical Instruments")
        card = Card.objects.create(
            deck=deck,
            image="harp.png",
            sound="harp.opus"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: a VISUAL aspect, and an AURAL aspect.

        # Add card text.
        card.text = "Harp"
        card.save()

        # Test result.
        self.assertTrue(
            card.aspects.filter(kind="TEXTUAL").exists(),
            "The card has no textual aspect, but it should have one."
        )
        self.assertTrue(
            card.aspects.filter(kind="VISUAL").exists(),
            "The card has no visual aspect, but it should have one."
        )
        self.assertTrue(
            card.aspects.filter(kind="AURAL").exists(),
            "The card has no aural aspect, but it should have one."
        )
