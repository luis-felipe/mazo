import os
import unittest

from django.test import TestCase

from mazo.ie import restore_data


# RESTORING TESTS
# ===============

class RestoreDataTests(TestCase):
    """Tests for the procedure ``ie.restore_data``."""
    # FIXME: Enable test when it doesn't modify user data.
    #
    # Currently, mazo.ie.restore_data modifies user data located in the
    # directory indicated by mazo.constants.USER_DATA_DIR, which points
    # to $HOME/.local/share/mazo.
    #
    # This test will be omitted until mazo.constants.USER_DATA_DIR
    # becomes configurable, so that a safe test user folder can be used
    # during tests like this one.
    @unittest.skip("Test modifies data in mazo.constants.USER_DATA_DIR.")
    def test_user_data_folder_exists_after_restoring(self):
        # Restore data.
        backup_path = os.path.join(
            "mazo", "tests", "data", "mazo-backup.zip"
        )
        restored_data_path = restore_data(backup_path)

        # Test result.
        self.assertTrue(
            os.path.exists(restored_data_path),
            "User data folder does not exist, but it should."
        )

    def test_report_missing_path_error(self):
        # Non-existant backup file.
        backup_path = os.path.join(
            "mazo", "tests", "data", "mazo-backup-imaginary.zip"
        )

        # Attempt to restore data (which should fail).
        errors = restore_data(backup_path)

        # Test result.
        self.assertIn("BackupNotFoundError", errors)

    def test_report_invalid_format_error(self):
        # Invalid backup file.
        backup_path = os.path.join(
            "mazo", "tests", "data", "mazo-backup-not-zip.tar.xz"
        )

        # Attempt to restore data (which should fail).
        errors = restore_data(backup_path)

        # Test result.
        self.assertIn("BackupFormatError", errors)

    def test_report_missing_database_file(self):
        # Invalid backup copy.
        backup_path = os.path.join(
            "mazo", "tests", "data", "mazo-backup-no-database.zip"
        )

        # Attempt to restore data (which should fail).
        errors = restore_data(backup_path)

        # Test result.
        self.assertIn("DatabaseNotFoundError", errors)
