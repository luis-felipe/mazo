from datetime import datetime
import os
import shutil
import tempfile

from django.test import TestCase

from mazo.constants import IMEXPORT_FORMAT_VERSION
from mazo.ie import card_to_dict, deck_to_dict, export_deck
from mazo.models import Card, Deck


# EXPORT TESTS
# ============

class CardToDictTests(TestCase):
    """Tests for the procedure ie.card_to_dict."""
    maxDiff = None

    def test_convert_card_to_dict(self):
        # Create deck.
        deck = Deck.objects.create(name="Birds")

        # Create card.
        card = Card.objects.create(
            deck=deck,
            text="budgie, parakeet",
            text_b="Melopsittacus undulatus",
            image="budgie.png",
            sound="budgie.oga"
        )

        # Expected dictionary.
        card_dict = {
            "creation_date": card.creation_date.strftime(
                "%Y-%m-%dT%H:%M:%S%z"
            ),
            "text": card.text,
            "text_b": card.text_b,
            "image": card.image.name,
            "sound": card.sound.name
        }

        # Test results.
        self.assertEqual(card_to_dict(card), card_dict)

    def test_omit_text_field_when_empty(self):
        # Create deck.
        deck = Deck.objects.create(name="Birds")

        # Create card.
        card = Card.objects.create(
            deck=deck,
            text_b="Pica hudsonia",
            image="pica-hudsonia.png",
            sound="pica-hudsonia.oga"
        )

        # Convert card.
        card_dict = card_to_dict(card)

        # Test results.
        with self.assertRaises(KeyError):
            card_dict["text"]

    def test_omit_text_b_field_when_empty(self):
        # Create deck.
        deck = Deck.objects.create(name="Birds")

        # Create card.
        card = Card.objects.create(
            deck=deck,
            text="House sparrow",
            image="house-sparrow.png",
            sound="house-sparrow.oga"
        )

        # Convert card.
        card_dict = card_to_dict(card)

        # Test results.
        with self.assertRaises(KeyError):
            card_dict["text_b"]

    def test_omit_image_field_when_empty(self):
        # Create deck.
        deck = Deck.objects.create(name="Birds")

        # Create card.
        card = Card.objects.create(
            deck=deck,
            text="Song Thrush",
            text_b="Turdus philomelos",
            sound="turdus-philomelos.oga"
        )

        # Convert card.
        card_dict = card_to_dict(card)

        # Test results.
        with self.assertRaises(KeyError):
            card_dict["image"]

    def test_omit_sound_field_when_empty(self):
        # Create deck.
        deck = Deck.objects.create(name="Birds")

        # Create card.
        card = Card.objects.create(
            deck=deck,
            text="Atlantic canary",
            text_b="Serinus canaria",
            image="serinus-canaria.png"
        )

        # Convert card.
        card_dict = card_to_dict(card)

        # Test results.
        with self.assertRaises(KeyError):
            card_dict["sound"]


class DeckToDictTests(TestCase):
    """Tests for the procedure ie.deck_to_dict."""
    maxDiff = None

    def test_version_field_is_included(self):
        # Create a deck.
        deck = Deck.objects.create(name="Fish")

        # Result.
        deck_dict = deck_to_dict(deck)

        # Test results.
        self.assertEqual(deck_dict["version"], IMEXPORT_FORMAT_VERSION)

    def test_creation_date_field_is_included(self):
        # Create a deck.
        deck = Deck.objects.create(name="English Verbs")

        # Expected date.
        deck_dict = deck_to_dict(deck)
        expected_date = deck.creation_date.strftime(
            "%Y-%m-%dT%H:%M:%S%z"
        )

        # Test results.
        self.assertEqual(deck_dict["creation_date"], expected_date)

    def test_name_field_is_included(self):
        # Create a deck.
        deck_name = "Teams"
        deck = Deck.objects.create(name=deck_name)

        # Result.
        deck_dict = deck_to_dict(deck)

        # Test results.
        self.assertEqual(deck_dict["name"], deck_name)

    def test_description_field_is_included(self):
        # Create a deck with description.
        deck_description = "Insects of the valley."
        deck = Deck.objects.create(
            name="Bugs",
            description=deck_description
        )

        # Result.
        deck_dict = deck_to_dict(deck)

        # Test results.
        self.assertEqual(deck_dict["description"], deck_description)

    def test_credits_field_is_included(self):
        # Create a deck with credits.
        deck_credits = "John Doe, Jane Roe"
        deck = Deck.objects.create(
            name="Bikes",
            credits=deck_credits
        )

        # Result.
        deck_dict = deck_to_dict(deck)

        # Test results.
        self.assertEqual(deck_dict["credits"], deck_credits)

    def test_icon_field_is_included(self):
        # Create a deck with icon.
        deck_icon = "icon.png"
        deck = Deck.objects.create(
            name="Boats",
            icon=deck_icon
        )

        # Result.
        deck_dict = deck_to_dict(deck)

        # Test results.
        self.assertEqual(deck_dict["icon"], deck_icon)

    def test_cards_field_is_included(self):
        # Create a deck and some cards.
        deck = Deck.objects.create(name="Charts")
        Card.objects.create(
            deck=deck,
            image="icicle.png",
            text="Icicle plot"
        )
        Card.objects.create(
            deck=deck,
            image="bubble.png",
            text="Bubble chart"
        )

        # Expected dictionary.
        deck_dict = deck_to_dict(deck)
        card_dicts = deck_dict["cards"]

        # Test results.
        self.assertEqual(len(card_dicts), 2)

    def test_omit_description_field_when_empty(self):
        # Create a deck with no description.
        deck = Deck.objects.create(name="birds")

        # Convert deck.
        deck_dict = deck_to_dict(deck)

        # Test results.
        with self.assertRaises(KeyError):
            deck_dict["description"]

    def test_omit_credits_field_when_empty(self):
        # Create a deck with no credits.
        deck = Deck.objects.create(name="Dolls")

        # Convert deck.
        deck_dict = deck_to_dict(deck)

        # Test results.
        with self.assertRaises(KeyError):
            deck_dict["credits"]

    def test_omit_icon_field_when_deck_has_no_icon(self):
        # Create a deck without icon.
        deck = Deck.objects.create(name="Trains")

        # Convert deck to dictionary.
        deck_dict = deck_to_dict(deck)

        # Test results.
        with self.assertRaises(KeyError):
            deck_dict["icon"]

    def test_omit_cards_field_when_deck_has_no_cards(self):
        # Create a deck without cards.
        deck = Deck.objects.create(name="Colors")

        # Convert deck to dictionary.
        deck_dict = deck_to_dict(deck)

        # Test results.
        with self.assertRaises(KeyError):
            deck_dict["cards"]


class ExportDeckTests(TestCase):
    """Tests for the procedure ie.export_deck."""
    def test_the_deck_archive_is_created(self):
        # Create a deck.
        deck = Deck.objects.create(name="Coins")

        # Define export destination.
        destination = tempfile.gettempdir()

        # Test result.
        self.assertTrue(
            os.path.exists(export_deck(deck, destination)),
            "The zipped deck archive does not exist, but it should."
        )

    def test_the_archive_has_a_base_directory(self):
        """Unzipping must not behave like a tarbomb."""
        # Create a deck.
        deck = Deck.objects.create(name="Butterflies")

        # Export deck.
        destination = tempfile.gettempdir()
        archive_path = export_deck(deck, destination)

        # Extract exported archive.
        timestamp = datetime.now().strftime("%Y-%m-%dT%H-%M-%S-%f")
        extraction_dir_path = os.path.join(
            destination,
            "mazo-export-test-{}".format(timestamp)
        )
        os.mkdir(extraction_dir_path)
        shutil.unpack_archive(archive_path, extraction_dir_path, "zip")

        # Expected root dir path.
        archive_name = os.path.basename(archive_path)
        root_dir_name = archive_name.replace(".zip", "")
        root_dir_path = os.path.join(extraction_dir_path, root_dir_name)

        # Test result.
        self.assertTrue(
            os.path.exists(root_dir_path),
            "The zipped deck archive does not have a base directory, but it should."
        )
