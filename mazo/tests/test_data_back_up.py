from datetime import datetime
import os
import shutil
import tempfile

from django.test import TestCase

from mazo.constants import APP_NAME
from mazo.ie import back_up_data


# BACK UP TESTS
# =============

class BackUpDataTests(TestCase):
    """Tests for the procedure ``ie.back_up_data``."""
    def test_the_backup_archive_is_created(self):
        # Define destination folder.
        destination = tempfile.gettempdir()

        # Test result.
        self.assertTrue(
            os.path.exists(back_up_data(destination)),
            "The backup copy does not exist, but it should."
        )

    def test_the_backup_archive_has_a_base_directory(self):
        """Unzipping must not behave like a tarbomb."""
        # Back up data.
        destination = tempfile.gettempdir()
        archive_path = back_up_data(destination)

        # Extract exported archive.
        timestamp = datetime.now().strftime("%Y-%m-%dT%H-%M-%S-%f")
        extraction_dir_path = os.path.join(
            destination,
            "mazo-backup-test-{}".format(timestamp)
        )
        os.mkdir(extraction_dir_path)
        shutil.unpack_archive(archive_path, extraction_dir_path, "zip")

        # Expected base directory.
        base_dir_path = os.path.join(extraction_dir_path, APP_NAME)

        # Test result.
        self.assertTrue(
            os.path.exists(base_dir_path),
            "The zipped backup does not have a base directory, but it should."
        )
