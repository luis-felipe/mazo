import os

from django.test import TestCase

from mazo.ie import import_card
from mazo.models import Card, Deck


# IMPORT TESTS
# ============

class ImportCardTests(TestCase):
    """Tests for the procedure ie.import_card."""
    def test_import_a_textual_card(self):
        # Create a deck.
        deck = Deck.objects.create(name="Aquatic animals")

        # Define card data to import.
        card_dict = {
            "creation_date": "2022-04-19T13:38:00-0500",
            "text": "Beluga or beluga whale",
            "text_b": "Delphinapterus leucas"
        }
        media_path = os.path.join("mazo", "tests", "data")

        # Import card (result must be a Card object).
        result = import_card(card_dict, deck, media_path)

        # Test result.
        self.assertIsInstance(result, Card)

    def test_import_a_visual_card(self):
        # Create a deck.
        deck = Deck.objects.create(name="Aquatic animals")

        # Define card data to import.
        card_dict = {
            "creation_date": "2022-04-19T13:38:00-0500",
            "text": "Beluga or beluga whale",
            "text_b": "Delphinapterus leucas",
            "image": "card-image.svg"
        }
        media_path = os.path.join("mazo", "tests", "data")

        # Import card (result must be a Card object).
        result = import_card(card_dict, deck, media_path)

        # Test result.
        self.assertIsInstance(result, Card)

    def test_import_an_aural_card(self):
        # Create a deck.
        deck = Deck.objects.create(name="Aquatic animals")

        # Define card data to import.
        card_dict = {
            "creation_date": "2022-04-19T13:38:00-0500",
            "text": "Beluga or beluga whale",
            "text_b": "Delphinapterus leucas",
            "image": "card-image.svg",
            "sound": "card-sound.oga"
        }
        media_path = os.path.join("mazo", "tests", "data")

        # Import card (result must be a Card object).
        result = import_card(card_dict, deck, media_path)

        # Test result.
        self.assertIsInstance(result, Card)
