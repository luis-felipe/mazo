from django.db.models import Sum
from django.test import TestCase

from mazo.models import Aspect, Card, Deck, Review, Round


# MODEL TESTS
# ===========

class FinishTests(TestCase):
    """Tests for the method Round.finish."""
    def test_finishing_a_round_sets_its_finish_date_to_now(self):
        # Create a deck and its default round.
        deck = Deck.objects.create(name="Gods")

        # Finish default round.
        round = deck.rounds.last()
        finish_date = round.finish()

        # Test result.
        self.assertEqual(round.finish_date, finish_date)

    def test_do_not_alter_finish_date_if_round_is_finished(self):
        # Create a deck and its default round.
        deck = Deck.objects.create(name="Islands")

        # Finish default round.
        round = deck.rounds.last()
        finish_date = round.finish()

        # Attemp to finish the same round again.
        round.finish()

        # Test result.
        self.assertEqual(round.finish_date, finish_date)

    def test_update_round_progress_to_complete(self):
        # Create a deck and its default round.
        deck = Deck.objects.create(name="Traffic signs")

        # Finish default round.
        default_round = deck.rounds.last()
        default_round.finish()

        # Test result.
        self.assertEqual(
            default_round.progress,
            1.0
        )


class GetScheduledReviewsTests(TestCase):
    """Tests for the method Round.get_scheduled_reviews."""
    def test_return_pending_visual_reviews(self):
        # Create deck and cards with default reviews.
        deck = Deck.objects.create(name="Fruits")
        Card.objects.create(  # Not a visual card.
            deck=deck,
            sound="lulo.opus",
            text="Lulo"
        )
        Card.objects.create(
            deck=deck,
            image="squash.png",
            text="Squash"
        )
        Card.objects.create(
            deck=deck,
            image="damson.png",
            text="Damson"
        )

        # Get schedule visual reviews.
        current_round = deck.rounds.order_by("number").last()
        visual_reviews = current_round.get_scheduled_reviews("VISUAL")

        # Test result.
        self.assertEqual(
            visual_reviews.count(),
            2
        )

    def test_return_pending_aural_reviews(self):
        # Create deck and cards with default reviews.
        deck = Deck.objects.create(name="Spanish")
        Card.objects.create(  # Not an aural card.
            deck=deck,
            image="silla.png",
            text="Silla"
        )
        Card.objects.create(
            deck=deck,
            sound="matera.png",
            text="Matera"
        )
        Card.objects.create(
            deck=deck,
            sound="pollito.png",
            text="Pollito"
        )

        # Get schedule aural reviews.
        current_round = deck.rounds.order_by("number").last()
        visual_reviews = current_round.get_scheduled_reviews("AURAL")

        # Test result.
        self.assertEqual(
            visual_reviews.count(),
            2
        )

    def test_return_pending_textual_reviews(self):
        # Create deck and cards with default reviews.
        deck = Deck.objects.create(name="Eugnil")
        Card.objects.create(  # Not a textual card.
            deck=deck,
            image="mare.png",
            sound="mare.opus"
        )
        Card.objects.create(
            deck=deck,
            text="azula",
            text_b="blue"
        )
        Card.objects.create(
            deck=deck,
            text="kirie",
            text_b="book"
        )

        # Get schedule textual reviews.
        current_round = deck.rounds.order_by("number").last()
        visual_reviews = current_round.get_scheduled_reviews("TEXTUAL")

        # Test result.
        self.assertEqual(
            visual_reviews.count(),
            2
        )


# SIGNAL HANDLERS TESTS
# =====================

class OnRoundCreatedTests(TestCase):
    """Tests for the signal handler on_round_created
    (see mazo.signals)."""
    def test_tell_deck_to_increase_its_round_count(self):
        # Create a deck and its default Round.
        deck = Deck.objects.create(name="Fruits")

        # Test result.
        self.assertEqual(deck.round_count, 1)

    def test_tell_round_to_number_itself(self):
        # Create a deck and its default Round.
        deck = Deck.objects.create(name="Viruses")

        # Create another round
        # (don't care about finishing the first round).
        new_round = Round.objects.create(deck=deck)

        # Test result.
        self.assertEqual(new_round.number, deck.round_count)

    def test_tell_aspects_to_schedule_reviews_for_themselves(self):
        # Create a deck with its default study Round.
        deck = Deck.objects.create(name="Tools")

        # Create one card.
        card = Card.objects.create(
            deck=deck,
            image="drill.png",
            sound="drill.opus",
            text="Drill"
        )

        # NOTE: When this card was created, three aspects were created
        # automatically: a VISUAL aspect, an AURAL aspect and a TEXTUAL
        # aspect. Also, a review was scheduled automatically for every
        # aspect.

        # Answer correctly every review and finish the 1st study round.
        aspects = card.aspects.all()

        for aspect in aspects:
            for review in aspect.reviews.all():
                review.respond_correctly()

        deck.rounds.last().finish()

        # Start a 2nd study round.
        deck.start_round()

        # At this point, there should be 3 pending reviews; one for each
        # aspect of the one and only card in the deck.

        # Test result.
        pending_reviews = Review.objects.filter(answer__isnull=True)

        self.assertEqual(
            pending_reviews.count(),
            3
        )
        self.assertEqual(
            pending_reviews.filter(aspect__kind="VISUAL").count(),
            1
        )
        self.assertEqual(
            pending_reviews.filter(aspect__kind="AURAL").count(),
            1
        )
        self.assertEqual(
            pending_reviews.filter(aspect__kind="TEXTUAL").count(),
            1
        )

    def test_tell_aspects_to_reset_retention_points_back_to_zero(self):
        # Create a deck and its default round.
        deck = Deck.objects.create(name="Spanish")

        # Create some cards.
        Card.objects.create(
            deck=deck,
            image="grifo.png",
            text="Grifo"
        )
        Card.objects.create(
            deck=deck,
            image="ducha.png",
            text="Ducha"
        )
        Card.objects.create(
            deck=deck,
            image="sanitario.png",
            text="Sanitario"
        )

        # NOTE: When these cards were created, two aspects were created
        # automatically for all of them: a VISUAL aspect and a TEXTUAL
        # aspect. Also, a default review was scheduled for every aspect,
        # but they have no answer yet.

        # Add one point to all aspects sneakily, ignoring the review
        # process.
        aspects = Aspect.objects.filter(card__deck=deck)
        for aspect in aspects:
            aspect.add_point()

        # Finish default round.
        round = deck.rounds.last()
        round.finish()

        # Start a new round.
        deck.start_round()

        # Test result.
        self.assertEqual(
            aspects.aggregate(
                Sum("retention_points")
            )["retention_points__sum"],
            0
        )
