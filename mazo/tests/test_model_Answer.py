from django.test import TestCase
from django.utils import timezone

from mazo.models import Card, Deck, StudySession


# SIGNAL HANDLERS TESTS
# =====================

class OnAnswerCreatedTests(TestCase):
    """Tests for signal handler mazo.signals.on_answer_created."""
    def test_tell_aspect_to_add_one_retention_point_if_answer_is_correct(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Electronic components")
        card = Card.objects.create(
            deck=deck,
            image="inductor.png",
            text="Inductor"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: a VISUAL aspect and a TEXTUAL aspect. Also,
        # one review was scheduled for each aspect.

        # Get visual aspect's review and answer it correctly.
        aspect = card.aspects.filter(kind="VISUAL").first()
        review = aspect.reviews.last()
        review.respond_correctly()

        # NOTE: Responding correctly creates a correct answer, and
        # its creation is expected to tell the related aspect to
        # add one point to its accumulator of retention points.

        # Test result.
        self.assertEqual(
            aspect.retention_points,
            1
        )

    def test_tell_aspect_to_reset_retention_points_back_to_zero_if_answer_is_incorrect(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Electronic components")
        card = Card.objects.create(
            deck=deck,
            image="fuse.png",
            text="Fuse"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: a VISUAL aspect and a TEXTUAL aspect. Also,
        # one review was scheduled for each aspect.

        # Get visual aspect's review and answer it correctly.
        aspect = card.aspects.filter(kind="VISUAL").first()
        review = aspect.reviews.last()
        review.respond_correctly()

        # At this point, the aspect's retention points have been
        # increased by one.

        # Schedule a new review and answer it incorrectly.
        new_review = aspect.schedule_review(timezone.now())
        new_review.respond_incorrectly()

        # NOTE: Responding incorrectly creates an incorrect answer, and
        # its creation is expected to tell the related aspect to reset
        # its retention points to zero.

        # Test result.
        self.assertEqual(
            aspect.retention_points,
            0
        )

    def test_tell_aspect_to_add_one_retention_point_if_answer_is_memorized(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Electronic components")
        card = Card.objects.create(
            deck=deck,
            image="transducer.png",
            text="transducer"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: a VISUAL aspect and a TEXTUAL aspect. Also,
        # one review was scheduled for each aspect.

        # Let's cheat and answer aspect's default review as memorized
        # even though the user has not accumulated enough points for
        # memorization.
        aspect = card.aspects.filter(kind="VISUAL").first()
        review = aspect.reviews.last()
        review.respond_memorized()

        # NOTE: Responding as memorized creates an answer of the
        # MEMORIZED kind, and its creation is expected to make the
        # related aspect to add one point to its accumulator of
        # retention points.

        # Test result.
        self.assertEqual(
            aspect.retention_points,
            1
        )

    def test_tell_aspect_to_set_itself_as_learned_if_answer_is_memorized(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Electronic components")
        card = Card.objects.create(
            deck=deck,
            image="memristor.png",
            text="memristor"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: a VISUAL aspect and a TEXTUAL aspect. Also,
        # one review was scheduled for each aspect.

        # Let's cheat and answer aspect's default review as memorized
        # even though the user has not accumulated enough points for
        # memorization.
        aspect = card.aspects.filter(kind="VISUAL").first()
        review = aspect.reviews.last()
        review.respond_memorized()

        # NOTE: Responding as memorized creates an answer of the
        # MEMORIZED kind, and its creation is expected to make the
        # related aspect to mark itself as learned.

        # Test result.
        self.assertTrue(
            aspect.is_learned,
            "The aspect related to the answer must be set to learned, but it isn't."
        )

    def test_tell_round_to_update_its_progress(self):
        # Create a deck and its default round.
        deck = Deck.objects.create(name="Musical Instruments")

        # Add a card.
        Card.objects.create(
            deck=deck,
            image="maracas.png",
            text="Maracas"
        )

        # When this card was created, two aspects were created
        # automatically: a VISUAL aspect and a TEXTUAL aspect. Also,
        # one review was scheduled for each aspect.

        # Finish a VISUAL study session to answer initial reviews.
        ongoing_round = deck.rounds.last()
        session = StudySession.objects.create(
            round=ongoing_round,
            kind="VISUAL"
        )

        for review in session.reviews.all():
            review.respond_correctly()

        session.finish()

        # At this point, answer objects were created and their creation
        # should have triggered the update of the current round's
        # progress to the deck's memorization progress at the time.

        # Test result.
        self.assertEqual(
            deck.rounds.last().progress,
            deck.get_progress()
        )
