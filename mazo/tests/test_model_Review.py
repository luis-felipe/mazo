from datetime import timedelta

from django.test import TestCase
from django.utils import timezone

from mazo.models import Card, Deck, Review


class StringRepresentationOfReviewTests(TestCase):
    """Tests for the method Review.__str__."""
    def test_the_string_is_human_readable(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Fish")
        card = Card.objects.create(
            deck=deck,
            image="stonefish.png",
            text="Stonefish"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: a VISUAL aspect and a TEXTUAL aspect.

        # Get visual aspect.
        aspect = card.aspects.filter(kind="VISUAL").first()

        # Get visual aspect's default review.
        default_review = aspect.reviews.last()

        # Test result.
        self.assertEqual(
            str(default_review),
            "{} {} PENDING".format(aspect.kind, default_review.date)
        )

    def test_indicate_when_the_review_is_done(self):
        # Create deck and a card.
        deck = Deck.objects.create(name="Fish")
        card = Card.objects.create(
            deck=deck,
            image="salmon.png",
            text="Salmon"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: a VISUAL aspect and a TEXTUAL aspect.

        # Get visual aspect.
        aspect = card.aspects.filter(kind="VISUAL").first()

        # Get aspect's default review and answer it.
        default_review = aspect.reviews.last()
        default_review.respond_correctly()

        # Test result.
        self.assertEqual(
            str(default_review),
            "{} {} DONE".format(aspect.kind, default_review.date)
        )

    def test_indicate_when_the_review_is_not_done(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Fish")
        card = Card.objects.create(
            deck=deck,
            image="clownfish.png",
            text="Clownfish"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: a VISUAL aspect and a TEXTUAL aspect.

        # Get visual aspect.
        aspect = card.aspects.filter(kind="VISUAL").first()

        # Get aspect's default review.
        default_review = aspect.reviews.last()

        # Test result.
        self.assertEqual(
            str(default_review),
            "{} {} PENDING".format(aspect.kind, default_review.date)
        )


class IsCompleteTests(TestCase):
    """Tests for the method Review.is_complete."""
    def test_return_true_when_the_review_has_an_answer(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Fish")
        card = Card.objects.create(
            deck=deck,
            image="sailfish.png",
            text="Sailfish"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: a VISUAL aspect and a TEXTUAL aspect.

        # Get visual aspect.
        aspect = card.aspects.filter(kind="VISUAL").first()

        # Get aspects's default review and answer it.
        default_review = aspect.reviews.last()
        default_review.respond_correctly()

        # Test result.
        self.assertTrue(
            default_review.is_complete(),
            "The review is complete, but it says it isn't."
        )

    def test_return_false_when_the_review_has_no_answer(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Fish")
        card = Card.objects.create(
            deck=deck,
            image="pygmy-seahorse.png",
            text="Pygmy Seahorse"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: a VISUAL aspect and a TEXTUAL aspect.

        # Get visual aspect.
        aspect = card.aspects.filter(kind="VISUAL").first()

        # Get aspect's default review (unanswered by default).
        default_review = aspect.reviews.last()

        # Test result.
        self.assertFalse(
            default_review.is_complete(),
            "The review is not complete, but it says it is."
        )


class IsTodayTests(TestCase):
    """Tests for the method Review.is_today."""
    def test_tell_when_the_review_is_today(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Seeds")
        card = Card.objects.create(
            deck=deck,
            image="radish.png",
            text="Radish"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: a VISUAL aspect and a TEXTUAL aspect.

        # Get the VISUAL aspect of the card and schedule a review
        # for today.
        aspect = card.aspects.filter(kind="VISUAL").first()
        review = Review.objects.create(
            aspect=aspect,
            date=timezone.now()
        )

        # Test result.
        self.assertTrue(
            review.is_today(),
            "The review is today, but it says it is not."
        )

    def test_tell_when_the_review_is_not_today(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Seeds")
        card = Card.objects.create(
            deck=deck,
            image="rice.png",
            text="Rice"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: a VISUAL aspect and a TEXTUAL aspect.

        # Get the VISUAL aspect of the card and schedule a review
        # for six days in the future.
        aspect = card.aspects.filter(kind="VISUAL").first()
        review = Review.objects.create(
            aspect=aspect,
            date=timezone.now() + timedelta(days=6)
        )

        # Test result.
        self.assertFalse(
            review.is_today(),
            "The review is not today, but it says it is."
        )


class RespondCorrectlyTests(TestCase):
    """Tests for the method Review.respond_correctly."""
    def test_record_answer(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Spanish")
        card = Card.objects.create(
            deck=deck,
            sound="emplumar.opus",
            text="Emplumar"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: an AURAL aspect and a TEXTUAL aspect.

        # Get aural aspect.
        aspect = card.aspects.filter(kind="AURAL").first()

        # Answer aspect's default review.
        aural_review = aspect.reviews.last()
        aural_review.respond_correctly()

        # Test result.
        self.assertTrue(
            aural_review.is_complete(),
            "The review has an answer, but it says it doesn't."
        )

    def test_record_correct_answer(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Spanish")
        card = Card.objects.create(
            deck=deck,
            sound="coercitivo.opus",
            text="Coercitivo"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: an AURAL aspect and a TEXTUAL aspect.

        # Get aural aspect.
        aspect = card.aspects.filter(kind="AURAL").first()

        # Answer aspect's default review.
        aural_review = aspect.reviews.last()
        answer = aural_review.respond_correctly()

        # Test result.
        self.assertEqual(
            answer.kind,
            "CORRECT"
        )


class RespondIncorrectlyTests(TestCase):
    """Tests for the method Review.respond_incorrectly."""
    def test_record_answer(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Spanish")
        card = Card.objects.create(
            deck=deck,
            sound="frambuesa.opus",
            text="Frambuesa"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: an AURAL aspect and a TEXTUAL aspect.

        # Get aural aspect.
        aspect = card.aspects.filter(kind="AURAL").first()

        # Answer aspect's default review.
        aural_review = aspect.reviews.last()
        aural_review.respond_incorrectly()

        # Test result.
        self.assertTrue(
            aural_review.is_complete(),
            "The review has an answer, but it says it doesn't."
        )

    def test_record_incorrect_answer(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Spanish")
        card = Card.objects.create(
            deck=deck,
            sound="granadilla.opus",
            text="Granadilla"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: an AURAL aspect and a TEXTUAL aspect.

        # Get aural aspect.
        aspect = card.aspects.filter(kind="AURAL").first()

        # Answer aspect's default review.
        aural_review = aspect.reviews.last()
        answer = aural_review.respond_incorrectly()

        # Test result.
        self.assertEqual(
            answer.kind,
            "INCORRECT"
        )


class RespondMemorizedTests(TestCase):
    """Tests for the method Review.respond_memorized."""
    def test_record_answer(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Spanish")
        card = Card.objects.create(
            deck=deck,
            sound="guardabarros.opus",
            text="guardabarros"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: an AURAL aspect and a TEXTUAL aspect.

        # Get aural aspect for review.
        aspect = card.aspects.filter(kind="AURAL").first()

        # Let's cheat and answer aspect's default review as memorized
        # even though the user has not accumulated enough points for
        # memorization.
        aural_review = aspect.reviews.last()
        aural_review.respond_memorized()

        # Test result.
        self.assertTrue(
            aural_review.is_complete(),
            "The review must have an answer, but it says it doesn't."
        )

    def test_record_memorized_answer(self):
        # Create a deck and a card.
        deck = Deck.objects.create(name="Spanish")
        card = Card.objects.create(
            deck=deck,
            sound="altruista.opus",
            text="altruista"
        )

        # NOTE: When this card was created, two aspects were created
        # automatically: an AURAL aspect and a TEXTUAL aspect.

        # Get aural aspect for review.
        aspect = card.aspects.filter(kind="AURAL").first()

        # Let's cheat and answer aspect's default review as memorized
        # even though the user has not accumulated enough points for
        # memorization.
        aural_review = aspect.reviews.last()
        answer = aural_review.respond_memorized()

        # Test result.
        self.assertEqual(
            answer.kind,
            "MEMORIZED"
        )
