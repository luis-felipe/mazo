""" Django configuration for running tests.

This configuration is currently used to override Django's MEDIA_ROOT
setting when running the test suite to avoid polluting a user media
directory when the suite is not run in an isolated environment.

This way of overriding settings is not recommended in Django's manual,
though:

https://docs.djangoproject.com/en/4.1/topics/testing/tools/#overriding-settings

However, using the recommended way didn't actually work. So, for now,
the following is used instead.

"""

import os
import tempfile

# XXX: Use override_settings instead in tests that need it.
from django.conf import settings


TEST_MEDIA_ROOT = os.path.join(
    tempfile.gettempdir(),
    "mazo_test-media"
)
settings.MEDIA_ROOT = TEST_MEDIA_ROOT
