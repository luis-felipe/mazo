""" Django ORM settings for mazo project. """

import os

import django
from django.conf import settings
from django.core.management import execute_from_command_line

from mazo.constants import DB_DIR, DB_PATH, MEDIA_DIR


def init():
    """Initialize Django ORM and Mazo's database."""
    # Configure Django settings.
    settings.configure(
        # This key seems to be mandatory to be able to use manage.py commands,
        # although it is not needed by Mazo.
        SECRET_KEY='nonsense',
        INSTALLED_APPS=[
            'mazo.apps.MazoConfig',
            'django_cleanup.apps.CleanupConfig'  # Must be at the bottom.
        ],
        DATABASES={
            'default': {
                'ENGINE': 'django.db.backends.sqlite3',
                'NAME': DB_PATH,
            }
        },
        DEFAULT_AUTO_FIELD='django.db.models.BigAutoField',
        # Media storage.
        MEDIA_ROOT=os.path.join(MEDIA_DIR, ""),
        # Internationalization.
        # TODO: Check which of these are actually necessary.
        LANGUAGE_CODE='en-us',
        TIME_ZONE='UTC',
        USE_I18N=True,
        USE_L10N=True,
        USE_TZ=True
    )

    # Do set Django up.
    django.setup()

    # Initialize database.
    if not os.path.exists(DB_PATH):
        print("INFO: No database found. Creating it.")
        os.makedirs(DB_DIR)
        execute_from_command_line(["manage", "migrate"])
