.. _building-documentation:

Building the documentation
==========================

The following instructions show you how to build the user manual and
the development documentation to HTML for publishing in Mazo's website.

::

   cd mazo

   # Build the manual.
   mkdir -p /tmp/mazo-manual
   yelp-build html -o /tmp/mazo-manual help/C/mazo

   # Build the development documentation.
   source .envrc
   sphinx-apidoc -o doc/api/ -fe --tocfile index mazo *migrations *tests
   sphinx-build -t development doc /tmp/mazo-dev-doc
