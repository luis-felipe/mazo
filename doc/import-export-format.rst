Import/Export Format
====================

:Format version: 0.7.0 (independet from Mazo's)

Mazo allows users to import/export decks as compressed archives in ZIP
format. This section describes what's in a deck archive.


Archive's Content
-----------------

An archived deck has the following directories and files::

  aquatic-animals-2022-05-17T16-13-58
  ├── deck.json
  ├── icon.png
  ├── images
  └── sounds

``deck.json`` (mandatory)
  This file must be present. It contains deck and cards information.
  See :ref:`json-format` for details.
``icon.png`` (optional)
  Deck icon. The name and image format varies from deck to deck.
``images`` (optional)
  Contains image files associated with cards.
``sounds`` (optional)
  Contains audio files associated with cards.


.. _json-format:

JSON Format
-----------

Example:

.. code:: javascript

   {
       "version": "0.7.0",  // Format version
       "creation_date": "2022-04-19T13:30:00-0500",
       "name": "Aquatic Animals",
       "description": "Animals that live in water most of their lifetime.",
       "credits": "Author: John Doe · Graphics: Jane Roe",
       "icon": "fish.png",

       "cards": [
           {
               "creation_date": "2022-04-19T13:38:00-0500",
               "text": "Beluga or beluga whale",
               "text_b": "Delphinapterus leucas",
               "image": "images/beluga.png",
               "sound": "sounds/beluga.oga"
           },
           // More cards could follow.
       ]
   }


.. note:: Optional fields don't have empty values; instead, they are not
          written to ``deck.json``.


.. _deck-format:

Deck format
~~~~~~~~~~~

``version`` (mandatory)
  ``string``. The version of the import/export format used to save the data.
  This version number is independent from Mazo's version number.

``creation_date`` (optional)
  ``string``. `ISO 8601`_ formatted string indicating date and time as follows::

    YYYY-MM-DDTHH:MM:SS±HH:SS

  For example::

    2022-05-24T14:06:00-05:00

  The above date corresponds to May 24, 2022 at 14:06 UTC-5.

``name`` (mandatory)
  ``string``. The name of the deck.

``description`` (optional)
  ``string``. A short description of the deck (max. 250 characters).

``credits`` (optional)
  ``string``. A text listing people who contributed directly or
  indirectly to the development of the deck (max. 1000 characters).

``icon`` (optional)
  ``string``. Path to an image in the base folder of the import/export
  archive.

  Image formats supported: PNG, JPG, GIF, SVG.

``cards`` (optional)
  ``array``. The list of cards (see :ref:`card-format`).



.. _card-format:

Card format
~~~~~~~~~~~

``creation_date`` (optional)
  ``string``. `ISO 8601`_ formatted string indicating date and time. See
  ``creation_date`` in :ref:`deck-format` for an example.

``text`` (optional)
  ``string``. Arbitrary text (max. 250 characters).

``text_b`` (optional)
  ``string``. Arbitrary text (max. 250 characters).

``image`` (optional)
  ``string``. Path to an image in the base folder of the import/export
  archive.

  Image formats supported: PNG, JPG, GIF, SVG.

``sound`` (optional)
  ``string``. Path to an audio file in the base folder of the
  import/export archive.

  Audio formats supported: mp4, mpeg, oppus, vorbis, wav.



.. Links
.. _ISO 8601: https://en.wikipedia.org/wiki/ISO_8601
