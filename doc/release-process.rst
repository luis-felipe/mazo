.. _release-process:

Release process
===============

#. Make sure all tests pass (See :ref:`testing-process`).
#. Release version ``X.Y.Z``:

   #. Update the release date of version ``X.Y.Z`` in
      :ref:`version-history`.
   #. Update website.
   #. Update online manuals (for all supported languages).
   #. Tag the latest revision as version ``vX.Y.Z``. For example::

        git tag -a v1.5.2 -m "Version 1.5.2"
        git push origin --tags

   #. Release version in local PMS.
   #. Package new version for the `GNU Guix`_ distribution.
   #. Announce the release to the world.



.. LINKS
.. _GNU Guix: https://guix.gnu.org/
