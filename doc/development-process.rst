.. _development-process:

Development process
===================

To start working on a new version of the application (say ``X.Y.Z``):

#. Add the new version to the :abbr:`PMS (Project Management System)`
   (which is private for now).
#. Report to the issue tracker the work to be done for the new
   version, such as new features, bugs, proposals, etc.
#. Increase version number to ``X.Y.Z`` in the source files.
#. For each issue reported to the tracker:

   #. Make the necessary changes to the source files.
   #. Test the changes.
   #. Document the changes.
   #. Mention important changes in :ref:`version-history`.
#. Release version ``X.Y.Z`` (see :ref:`release-process`).
