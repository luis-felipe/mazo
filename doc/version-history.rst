.. _version-history:

Version History
===============

This section lists all the versions of *Mazo* that have been
released so far, and important changes introduced in each of them
(this project uses `Semantic Versioning`_).

Version 1.2.1, 2024-08-10 UTC-05
  + Partially fix long-standing `memory/space leak issue`_.

Version 1.2.0, 2023-08-25 UTC-05
  + Support animated GIF.
  + Provide a couple of `sample decks`_ for download.

Version 1.1.0, 2023-06-14 UTC-05
  + Migrate user interface to GTK 4 components.

Version 1.0.0, 2022-09-16 UTC-05
  + Initial public release.

Versions 0.1.0 to 0.11.0
  Closed-door development versions with no public releases.



.. LINKS
.. _sample decks: https://luis-felipe.gitlab.io/mazo/
.. _Semantic Versioning: https://semver.org/
.. _memory/space leak issue: https://codeberg.org/luis-felipe/mazo/issues/1
