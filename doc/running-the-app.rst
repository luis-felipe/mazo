.. _running-the-app:

Running the application
=======================

The following instructions show you how to get the source files of the
application and how to run it in an isolated development environment.


1. Get the source files::

     git clone https://codeberg.org/luis-felipe/mazo.git

2. Start a development environment in an isolated container::

     cd mazo
     guix shell -L . -C -m manifest.scm \
     -E "^DISPLAY$" -E "^XAUTHORITY$" --expose="$XAUTHORITY" \
     --expose=/tmp/.X11-unix/ --expose=$HOME/.Xauthority \
     --expose=/etc/machine-id --expose=/dev/dri \
     --expose=/run/user/1001/at-spi/bus_0 --expose=$HOME/path/to/directory

   Make sure to replace ``$HOME/path/to/directory`` with a
   directory containing the media files you would like to use
   for creating decks and cards during development.

3. Export required environment variables::

     source ./.envrc

4. Run the application::

     python3 mazo.py
