Feature plan
============

This section lists functionality *Mazo* is expected to provide.

| ☑ means the feature is already implemented.
| ☐ means the feature is not implemented yet.


Service
-------

| ☐ Mazo provides a web catalog of ready-made decks in different languages.


Decks
-----

| ☑ User can manage decks (create, view, update, delete).
| ☑ User can manage cards (create, view, update, delete).
| ☐ User can duplicate decks.
| ☐ User can duplicate decks with inverted text.


Cards
-----

| ☑ User can learn VISUAL aspects of concepts.
| ☑ User can learn TEXTUAL aspects of concepts.
| ☑ User can learn AURAL aspects of concepts.
| ☐ User can learn ORAL aspects.
| ☐ User can learn WRITTEN aspects.
| ☐ User can record audio from Mazo.
| ☐ User can add several images to a card.
| ☐ User can add video to a card.


Data
----

| ☑ User can export decks (without personal progress).
| ☑ User can import decks.
| ☑ User can back up Mazo's database and media from Mazo.
| ☑ User can restore Mazo's database and media from Mazo, using a previous backup.


Settings
--------

| ☐ User can set ``ASPECT_POINTS_FOR_MEMORIZATION`` per deck.


Studying
--------

| ☑ User can review decks at any time (no rigid schedulling).
| ☑ User can leave a session and continue later.
| ☑ Mazo suggest when an aspect of a card seems memorized.
| ☑ User can confirm when an aspect of a card is memorized.
| ☑ User can study memorized decks again as if they where new as many times as they like.
| ☐ User can see VISUAL memorization progress of a deck.
| ☐ User can see AURAL memorization progress of a deck.
| ☐ User can see TEXTUAL memorization progress of a deck.
| ☐ Mazo tells local calendaring system to schedule future reviews


User interface
--------------

| ☑ GUI components are GTK 4.
| ☐ GUI adapts well to different screen sizes.
| ☐ Playing audio displays a visualization.


Localization
------------

| ☐ Application and website are available in Spanish.
| ☐ Application and website are available in Japanese.
