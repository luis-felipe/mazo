.. _testing-process:

Testing process
===============

.. warning:: All tests should be run or performed in an isolated
             development environment to avoid altering your regular
             operating system user data. See :ref:`running-the-app` for
             more information.

This section describes the process to control the quality of the
application.

*Mazo* comes with a test suite that can be run automatically, but
there are additional tests, mainly user interface tests, that need to
be performed manually for now.

All automatic tests are run continuously during the development
cycle. All manual tests are performed before releasing a new version
of the software (they take a lot of time).


.. _automatic-tests:

Automatic tests
---------------

To run the complete suite of automatic tests, do the following::

  cd path/to/mazo
  python3 manage.py test


.. _manual-tests:

Manual tests
------------

The following are manual tests that I don't know how to automate or
whether they can be automated.

Make sure the following two points are met:

1. The task can be finished.
2. The time to accomplish it is reasonable.

If any of the above fails, file a bug report.


| ☐ User can create a deck
| ☐ User can edit a deck
| ☐ User can delete a deck
|
| ☐ User can add a single card
| ☐ User can add several cards
| ☐ User can browse cards
| ☐ User can edit a card
| ☐ User can delete a card
|
| ☐ User can start a study session
| ☐ User can review a single card (as part of a study session)
| ☐ User can leave a study session
| ☐ User can complete a study session (review all pending cards)
| ☐ User can start a new round
|
| ☐ User can import a deck
| ☐ User can export a deck
|
| ☐ User can back up their data
| ☐ User can restore their data from a backup copy
