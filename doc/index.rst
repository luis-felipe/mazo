.. Mazo documentation master file, created by
   sphinx-quickstart on Thu Jan 27 10:37:33 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Mazo's Development Documentation
================================

*Mazo* is a learning application that helps you memorize simple
concepts using multimedia flash cards and spaced reviews. This
documentation provides information about its development and
maintenance.

`→ Mazo's website <https://luis-felipe.gitlab.io/mazo/>`_


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   running-the-app
   building-documentation
   development-process
   testing-process
   release-process
   import-export-format
   feature-plan
   api/index
   version-history



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
