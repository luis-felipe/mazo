#!/usr/bin/env python3

import sys

from mazo import orm

# Initialize ORM.
orm.init()

# Run the app.
if __name__ == "__main__":
    from mazo.views.app import App
    app = App()
    exit_status = app.run(sys.argv)
    sys.exit(exit_status)
