====
Mazo
====

*Mazo* is a learning application that helps you memorize simple
concepts using multimedia flash cards and spaced reviews.

+ `Website`_
+ `User manual`_
+ `Development documentation`_


Copying
=======

Public domain 2020 `Luis Felipe López Acevedo`_. All rights waived.

This work has been released into the public domain by its author. This
applies universewide.





.. LINKS
.. _Development documentation: https://luis-felipe.gitlab.io/mazo/doc/
.. _Luis Felipe López Acevedo: https://luis-felipe.gitlab.io/
.. _User manual: https://luis-felipe.gitlab.io/mazo/manual/
.. _Website: https://luis-felipe.gitlab.io/mazo/
